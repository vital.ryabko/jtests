package tests;

import appmanager.AppManager;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.remote.MobilePlatform;
import model.UserData;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.net.MalformedURLException;

public class TestBase {

  protected static final AppManager app = new AppManager(MobilePlatform.ANDROID);

  @Before
  public void startDriver() throws MalformedURLException {
    app.init();
  }
  @After
  public void stopDriver() {
    app.stop();
  }

}
