package tests;

import io.appium.java_client.AppiumDriver;
import model.UserData;
import org.junit.Test;

import java.io.IOException;

public class Login extends TestBase {

  public AppiumDriver driver;
  @Test
  public void testPartialLogin () throws IOException, InterruptedException {
    UserData user1 = new UserData("49151222333", "111111", "autotest123", "autotest123");
    //user1 дошел регистрацией до ввода штата
    app.shortLogin(user1);
  }

  @Test
  public void testLoginContinue () throws IOException, InterruptedException {
    UserData user1 = new UserData("49151222333", "111111", "autotest123", "autotest123");
    //user1 дошел регистрацией до ввода штата
    app.shortLogin(user1);
    app.navigation().continueRegistration();
  }

  @Test
  public void testLoginStatesList () throws IOException, InterruptedException {
    UserData user1 = new UserData("49151222333", "111111", "autotest123", "autotest123");
    //user1 дошел регистрацией до ввода штата
    app.shortLogin(user1);
    app.navigation().continueRegistration();
    app.form().clickStatesMenu();
    app.form().searchAndSelectInList("Bremen");
    app.form().clickStatesMenu();
    app.form().searchAndSelectInList("Lower Saxony");
    app.form().clickStatesMenu();
    app.form().searchAndSelectInList("Thuringia");

  }
  /**
    Thread.sleep(2000);
    app.getContentHelper().makeScreenshot("Main.png");
  }
*/
}
