package tests;

import model.UserData;
import org.junit.Test;

import java.io.IOException;

public class Registering extends TestBase {

  @Test
  public void testRegistration () throws IOException, InterruptedException {
    UserData user2 = new UserData("49151222332", "111111", "autotest123", "autotest123");
    app.login(user2);
    app.navigation().clickToContinue();
    app.form().chooseSalutation();
    app.form().enterName("Auto");
    app.form().enterLastName("Test");
    app.navigation().swipeUp("de.aureum.card.dev:id/lastName", "de.aureum.card.dev:id/salutation_mr");
    app.form().enterEmail("test123@test.ru");
    app.navigation().clickToContinue();
    app.form().enterPostalCode("10115");
    app.form().clickStatesMenu();
    app.form().searchAndSelectInList("Bremen");
    app.form().enterStreet("Hardenbergstraße");
    app.form().enterStreetNum("21");
    app.navigation().swipeUp("de.aureum.card.dev:id/streetNo", "de.aureum.card.dev:id/postalCode");
    app.navigation().clickToContinue();
  }

  /**


   Thread.sleep(2000);
   app.getContentHelper().makeScreenshot("Main.png");
   }*/
}
