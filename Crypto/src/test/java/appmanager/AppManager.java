package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobilePlatform;
import model.UserData;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AppManager {

  private LocatorsBase locators;
  private NavHelper navigation;
  private FormHelper form;
  public AppiumDriver driver;
  private String agent;

  public AppManager(String agent) {
    this.agent = agent;
  }

  public void init() throws MalformedURLException {
    DesiredCapabilities capabilities = new DesiredCapabilities();
    if (agent.equals(MobilePlatform.ANDROID)) {
      capabilities.setCapability("platformName", "Android");
      capabilities.setCapability("deviceName", "041604a1e6563902");
      capabilities.setCapability("platformVersion", "7.0");
      capabilities.setCapability("app", "C:\\Testing\\apk\\AureumCard-dev-release-0.7.12.apk");
      capabilities.setCapability("autoGrantPermissions", true);
      driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    } else if (agent.equals(MobilePlatform.IOS)) {
      capabilities.setCapability("platformName", "iOS");
      capabilities.setCapability("deviceName", "iPhone Simulator");
      capabilities.setCapability("platformVersion", "11.2");
      capabilities.setCapability("app", "C:\\Testing\\apk\\AureumCard-dev-release-0.7.12.apk");
      capabilities.setCapability("autoGrantPermissions", true);
      capabilities.setCapability("automationName", "XCUITest");
      driver = new IOSDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    }
    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    form = new FormHelper(driver);
    navigation = new NavHelper(driver);
    locators = new LocatorsBase(driver);
  }

  public void stop() {
    driver.quit();
  }

  public void shortLogin(UserData userData) {
    navigation.goToPhoneScreen();
    proceedPhone(userData.getNumber());
    form.codeInput(userData.getCode());
    form.enterPassowrd(userData.getPass());
    locators.loginButton.click();
    navigation.clickToConfirm();
    form.pinInput("1111");
    form.pinInput("1111");
  }

  public void login(UserData userData) {
    navigation.goToPhoneScreen();
    proceedPhone(userData.getNumber());
    form.codeInput(userData.getCode());
    form.enterPassowrd(userData.getPass());
    form.enterConfirmPassowrd(userData.getPass_confirm());
    locators.confirmButton.click();
    form.pinInput("1111");
  }

  public void proceedPhone(String number){
    form.enterPhone(number);
    locators.loginButton.click();
    //form.clickElement(By.id("de.aureum.card.dev:id/login"));
  }

  public void checkText(By locator, String text) {
    WebElement title = driver.findElement(locator);
    String titleText = title.getText();
    Assert.assertEquals(titleText, text);
  }

  public void makeScreenshot(String pathname) throws IOException, InterruptedException {
    Thread.sleep(2000);
    File srcFile = driver.getScreenshotAs(OutputType.FILE);
    FileUtils.copyFile(srcFile, new File(pathname));
  }

  public void verifyScreen(String screen_name) {
    boolean is_found = false; // validation parameter
    if (screen_name.equals("Onboarding Name")) {
      checkText(By.id("de.aureum.preference.dev:id/description"),"Please enter your name");
      is_found = true;
    }
    else if (screen_name.equals("Phone input")) {
      checkText(By.className("android.widget.TextView"), "Use your phone number to login or create a new account");
      is_found = true;
    }
    else if (screen_name.equals("Code 1")) {
      checkText(By.id("de.aureum.preference.dev:id/title"), "Setup your password");
      is_found = true;
    }
    else if (screen_name.equals("Code 2")) {
      checkText(By.id("de.aureum.preference.dev:id/title"), "Confirm your password");
      is_found = true;
    }
    else if (screen_name.equals("Code login")) {
      checkText(By.id("de.aureum.preference.dev:id/title"), "Enter your password");
      is_found = true;
    }
    else if (screen_name.equals("Enable camera")) {
      checkText(By.id("de.aureum.preference.dev:id/description"), "First we need to know how you look, so we can recognise you anywhere and anytime you want");
      is_found = true;
    }
    else if (screen_name.equals("Photo screen")) {
      checkText(By.className("android.widget.TextView"), "First we need to know how you look, so we can recognise you anywhere and anytime you want");
      is_found = true;
    }
    Assert.assertEquals(is_found,true);
  }

  public FormHelper form() {
    return form;
  }

  public NavHelper navigation() {
    return navigation;
  }

  public LocatorsBase locators() {
    return locators;
  }

  /*  public void keyboardIsOpen(){
    Assert.assertEquals(driver.isKeyboardShown(),true);
  }
  public void keyboardIsClosed(){
    Assert.assertEquals(driver.isKeyboardShown(),false);
  }
*/
}
