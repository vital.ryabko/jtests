package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.By;

public class NavHelper extends BaseHelpers{

  public NavHelper(AppiumDriver driver) {
    super(driver);
  }

  public void goToPhoneScreen(){
    //clickElement(By.id("de.aureum.card.dev:id/login"));
    loginButton.click();
  }

  public void clickToContinue(){
    //clickElement(By.id("de.aureum.card.dev:id/continueBtn"));
    continueButton.click();
  }

  public void clickToConfirm(){
    //clickElement(By.id("de.aureum.card.dev:id/confirmBtn"));
    confirmButton.click();
  }
  public void continueRegistration(){
    clickElement(By.id("de.aureum.card.dev:id/registrationCard"));
  }

  public void swipeUp(String id_bottom, String id_up) {
    MobileElement element1 = (MobileElement) driver.findElement(By.id(id_bottom));
    MobileElement element2 = (MobileElement) driver.findElement(By.id(id_up));
    new TouchAction(driver).longPress(element1).moveTo(element2).release().perform();
  }
}
