package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class LocatorsBase {

  public LocatorsBase(AppiumDriver driver) {
    PageFactory.initElements(new AppiumFieldDecorator(driver, 15, TimeUnit.SECONDS), this);
  }

  @AndroidFindBy(id = "de.aureum.card.dev:id/login")
  @iOSFindBy()
  public MobileElement loginButton;

  @AndroidFindBy(id = "de.aureum.card.dev:id/confirmBtn")
  @iOSFindBy()
  public MobileElement confirmButton;

  @AndroidFindBy(id = "de.aureum.card.dev:id/continueBtn")
  @iOSFindBy()
  public MobileElement continueButton;

  @AndroidFindBy(id = "de.aureum.card.dev:id/number_1")
  @iOSFindBy()
  public MobileElement codeField;

  @AndroidFindBy(id = "de.aureum.card.dev:id/bottomText")
  @iOSFindBy()
  public MobileElement phoneField;

  @AndroidFindBy(id = "de.aureum.card.dev:id/password")
  @iOSFindBy()
  public MobileElement passwordField;

  @AndroidFindBy(id = "de.aureum.card.dev:id/confirmPassword")
  @iOSFindBy()
  public MobileElement confirmPasswordField;

  @AndroidFindBy(id = "de.aureum.card.dev:id/salutation_mr")
  @iOSFindBy()
  public MobileElement salutation;

  @AndroidFindBy(id = "de.aureum.card.dev:id/name")
  @iOSFindBy()
  public MobileElement nameField;

  @AndroidFindBy(id = "de.aureum.card.dev:id/lastName")
  @iOSFindBy()
  public MobileElement lastNameField;

  @AndroidFindBy(id = "de.aureum.card.dev:id/email")
  @iOSFindBy()
  public MobileElement emailField;

  @AndroidFindBy(id = "de.aureum.card.dev:id/postalCode")
  @iOSFindBy()
  public MobileElement postalCodeField;

  @AndroidFindBy(id = "de.aureum.card.dev:id/street")
  @iOSFindBy()
  public MobileElement streetField;

  @AndroidFindBy(id = "de.aureum.card.dev:id/streetNo")
  @iOSFindBy()
  public MobileElement streetNoField;

  @AndroidFindBy(id = "de.aureum.card.dev:id/states")
  @iOSFindBy()
  public MobileElement states;

  @AndroidFindBy(id = "de.aureum.card.dev:id/design_bottom_sheet")
  @iOSFindBy()
  public MobileElement statesList;
}
