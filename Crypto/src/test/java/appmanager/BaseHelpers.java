package appmanager;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class BaseHelpers extends LocatorsBase{
  protected AppiumDriver driver;

  public BaseHelpers(AppiumDriver driver) {
    super(driver);
    this.driver = driver;
  }

  protected void clickElement(By locator) {
    WebElement element = driver.findElement(locator);
    element.click();
  }
}
