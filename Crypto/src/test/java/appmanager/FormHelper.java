package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.By;

import java.util.List;

public class FormHelper extends BaseHelpers
{

  public FormHelper(AppiumDriver driver) {
    super(driver);
  }

  public void searchAndSelectInList(String value) {
    boolean element_is_found = false; //данный параметр будет индикатором, что мы нашли пункт с необходимым значением
    String current_value;
    int correct_num = 100; // Параметр для номер нашего корректного элемента. Изначально значение явно неверное.
    //Начинаем цикл по поиску нужного элемента
    while (!element_is_found) {
      //Находим родительский элемент списка
      //MobileElement parent_list = (MobileElement) driver.findElement(By.id("de.aureum.card.dev:id/design_bottom_sheet"));
      //Формируем список, состоящий их каждого элемента, видимого на экране
      List<MobileElement> el3 = statesList.findElementsByClassName("android.widget.TextView");
      int size = el3.size(); //Смотрим количество элементов в списке
      //Формируем цикл с перебором каждого элемента
      for (int i = 0; i < size; i++) {
        MobileElement el_with_name = el3.get(i);
        current_value = el_with_name.getText(); //Находим имя элемента с номером i
        if (current_value.equals(value)) {
          element_is_found = true; //Если имя совпадает с искомым, говорим, что нашли имя, чтобы выйти из внешнего цикла
          correct_num = i; //Фиксиуем номер правильного элемента
          i = size; //Завершаем поиск
        }
      }
      if (element_is_found) {
        el3.get(correct_num).click(); //Нажимаем правильный элемент
      } else if (size <= 7) //Если элемент на первой странице, свайпаем одни элементы, если уже свайпали - другие
        new TouchAction(driver).longPress(el3.get(5)).moveTo(el3.get(0)).release().perform();
      else new TouchAction(driver).longPress(el3.get(9)).moveTo(el3.get(3)).release().perform();
    }
  }

/**  public void selectByValue (String value){
    new Select(driver.findElement(By.id("de.aureum.card.dev:id/design_bottom_sheet"))).selectByVisibleText(value);
  }*/

  public void codeInput(String digit) {
    //fillInTheField(By.id("de.aureum.card.dev:id/number_1"), digit);
    codeField.sendKeys(digit);
  }

  public void pinInput(String digit) {
    char[] num = digit.toCharArray();
    for (int i=0; i<=3; i++) {
      clickElement(By.id("de.aureum.card.dev:id/btn_"+num[i]));
    }
  }

  public void enterPhone(String number) {
    //fillInTheField(By.id("de.aureum.card.dev:id/bottomText"),number);
    phoneField.sendKeys(number);
  }

  public void enterPassowrd(String pass){
    //fillInTheField(By.id("de.aureum.card.dev:id/password"), pass);
    passwordField.sendKeys(pass);
  }

  public void enterConfirmPassowrd(String pass){
    //fillInTheField(By.id("de.aureum.card.dev:id/confirmPassword"), pass);
    confirmPasswordField.sendKeys(pass);
  }

  public void chooseSalutation(){
    //clickElement(By.id("de.aureum.card.dev:id/salutation_mr"));
    salutation.click();
  }

  public void enterName(String name){
    //fillInTheField(By.id("de.aureum.card.dev:id/name"), name);
    nameField.sendKeys(name);
  }

  public void enterLastName(String name){
    //fillInTheField(By.id("de.aureum.card.dev:id/lastName"), name);
    lastNameField.sendKeys(name);
  }

  public void enterEmail(String email){
    //fillInTheField(By.id("de.aureum.card.dev:id/email"), email);
    emailField.sendKeys(email);
  }

  public void enterPostalCode(String code){
    //fillInTheField(By.id("de.aureum.card.dev:id/postalCode"),code);
    postalCodeField.sendKeys(code);
  }

  public void enterStreet(String street){
    //fillInTheField(By.id("de.aureum.card.dev:id/street"),street);
    streetField.sendKeys(street);
  }

  public void enterStreetNum(String street_num){
    //fillInTheField(By.id("de.aureum.card.dev:id/streetNo"),street_num);
    streetNoField.sendKeys(street_num);
  }

  public void clickStatesMenu(){
    //clickElement(By.id("de.aureum.card.dev:id/states"));
    states.click();
  }

}
