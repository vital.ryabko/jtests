package course;

public class Point {
  public double x;
  public double y;
  public double x1;
  public double y1;

  public Point(double x, double y, double x1, double y1) {
    this.x = x;
    this.y = y;
    this.x1 = x1;
    this.y1 = y1;
  }

  public Point(double x, double y) {
    this(0,0, x, y);
  }

  public double Point() {
    double a = this.x - this.x1;
    double b = this.y - this.y1;
    double c = Math.sqrt(a*a+b*b);
    return c;
  }
}
