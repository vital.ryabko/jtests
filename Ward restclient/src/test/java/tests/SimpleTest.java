package tests;

import org.junit.Test;
import restclient.AdminClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class SimpleTest {
  private String s = null;

  @Test
  public void test1(){
    try {
      assertThat(s,is(notNullValue()));
    } catch (AssertionError e){
      s = " ";
      assertThat(s,is(notNullValue()));
      System.out.println("String is not null!");
    }
  }

  @Test
  public void createGameTest() throws ParseException, IOException {
    AdminClient admin = new AdminClient();
    String payload = admin.getLastPayloadIdForGame("1572f75b-f3c4-11e8-8b43-de9936a8174c");
    System.out.println(payload);

    Map<String, String> prepick = admin.getPrepickValuesMapFromPayload("1572f75b-f3c4-11e8-8b43-de9936a8174c", payload);
    prepick.keySet().stream().forEach(key -> System.out.println(key));
    prepick.keySet().stream().forEach(key -> System.out.println(prepick.get(key)));

  }

  @Test
  public void testId() throws IOException {
    AdminClient admin = new AdminClient();
    System.out.println(admin.getHeroIdByTitle("LeBlanc"));
  }

  private String getPID(int port){
    try {
      String line;
      String command = String.format("lsof -nP -i4TCP:%d",port);
      Process p = Runtime.getRuntime().exec(command);
      BufferedReader input =
              new BufferedReader
                      (new InputStreamReader(p.getInputStream()));
      while ((line = input.readLine()) != null) {
        String result = line.replaceAll("( )+", " ").split(" ")[1];
        if (!result.equals("PID"))
          return result;
      }
      input.close();
    }
    catch (Exception err) {
      err.printStackTrace();
    }
    return null;
  }

  private void killAppium(String pid) throws IOException {
    String command = String.format("kill -9 %s",pid);
    Runtime.getRuntime().exec(command);
  }
}
