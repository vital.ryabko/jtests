package restclient;

import com.google.gson.Gson;
import restclient.model.Payload;
import restclient.model.PayloadData;
import restclient.model.Ward;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

public class MonitorClient {
  private static final String STAGE = "staging";
  private static final String ADMIN_URI = "https://api-" + STAGE + ".ward.world/games/v2";
  private static final String MANIFEST_URI = "https://manifest.ward.world/" + STAGE + "/m.lol.v1.2.json";
  private static final String API_TOKEN = "secret";
  private String GAMES_LIST_PATH = "types/lol/active";
  private String CURRENT_GAME_ID;
  private String CURRENT_PAYLOAD_ID;
  private int MATCH_ID = 0;
  private String GAME_PAYLOAD_PATH = CURRENT_GAME_ID + "/payloads/" + CURRENT_PAYLOAD_ID;
  private String GAME_PAYLOAD_UPDATE_PATH = CURRENT_GAME_ID + "/match/" + MATCH_ID + "/payloads"; //POST here to update

  private Client client = ClientBuilder.newClient();

  private WebTarget webTarget = client.target(ADMIN_URI);

  public Response getGamePayload(String gameID, String payloadID) {
    this.CURRENT_GAME_ID = gameID;
    this.CURRENT_PAYLOAD_ID = payloadID;
    this.GAME_PAYLOAD_PATH = CURRENT_GAME_ID + "/payloads/" + CURRENT_PAYLOAD_ID;
    return webTarget
            .path(GAME_PAYLOAD_PATH)
            .request(MediaType.APPLICATION_JSON)
            .header("Authorization", API_TOKEN)
            .get();
  }

  public Payload getGamePayloadByIDs(String gameID, String payloadID) {
    Gson gson = new Gson();
    Response response = getGamePayload(gameID, payloadID);
    System.out.println(response.getStatus());
    return gson.fromJson(response.readEntity(String.class), Payload.class);
  }

  public void getAnyDataFromPayload(String gameID, String payloadID) {
    Payload payload = getGamePayloadByIDs(gameID, payloadID);
    for (Map.Entry<String, PayloadData> n : payload.getData().entrySet()) { //get data map
      PayloadData data = n.getValue(); //get PayloadData for each data element (for each match??)
      for (Map.Entry<String, Ward> w : data.getWards().entrySet()) {
        if (w.getKey().equals("prepick")) //get prepick ward and it's data
          System.out.println(w.getValue().getCreated_at());
      }
    }
  }

}
