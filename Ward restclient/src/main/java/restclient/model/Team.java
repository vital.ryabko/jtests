package restclient.model;

import java.util.ArrayList;

public class Team {
  private String id;
  private String title;
  private String acronym;
  private String color;
  private String font_color;
  private ArrayList<Player> players;

  public String getAcronim() {
    return acronym;
  }

  public String getTitle() {
    return title;
  }

  public ArrayList<Player> getPlayers() {
    return players;
  }

  public String getId() {
    return id;
  }
}
