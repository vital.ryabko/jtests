package restclient.model;

import java.util.ArrayList;
import java.util.HashMap;

public class Timer {
  private Long started_at;
  private String ended_at;
  private ArrayList<HashMap<String, Long>> pauses;


  public Timer withStarted_at(Long started_at) {
    this.started_at = started_at;
    return this;
  }

  public Timer withEnded_at(String ended_at) {
    this.ended_at = ended_at;
    return this;
  }

  public Timer withPauses(ArrayList<HashMap<String, Long>> pauses) {
    if (pauses == null)
      this.pauses = new ArrayList<>();
    else
      this.pauses = pauses;
    return this;
  }

  public Long getStarted_at() {
    return started_at;
  }
}
/* "timer":{
      "started_at":1540539108701,
      "ended_at":null,
      "pauses":[
          {
          "started_at":1540544009166,
          "ended_at":1540544010565
          },
          {
          "started_at":1540544011939,
          "ended_at":1540544013038
          }
      ]
},*/
