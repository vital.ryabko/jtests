package restclient.model;

import java.util.List;

public class Match {
  private String id;
  private long starts_at;
  private String state;
  private boolean is_closed;
  private boolean is_new_match;
  private boolean is_modified;
  private String prepick;
  private List<String> wards;

  private enum prepickType {v1,v2,streamer,empty};

  public String getId() {
    return id;
  }

  public Match withId(String id) {
    this.id = id;
    return this;
  }

  public long getStarts_at() {
    return starts_at;
  }

  public Match withStarts_at(long starts_at) {
    this.starts_at = starts_at;
    return this;
  }

  public String getState() {
    return state;
  }

  public Match withState(String state) {
    this.state = state;
    return this;
  }

  public boolean isIs_closed() {
    return is_closed;
  }

  public Match withIs_closed(boolean is_closed) {
    this.is_closed = is_closed;
    return this;
  }

  public List<String> getWards() {
    return wards;
  }

  public Match withWards(List<String> wards) {
    this.wards = wards;
    return this;
  }

  public boolean isIs_new_match() {
    return is_new_match;
  }

  public Match withIs_new_match(boolean is_new_match) {
    this.is_new_match = is_new_match;
    return this;
  }

  public boolean isIs_modified() {
    return is_modified;
  }

  public Match withIs_modified(boolean is_modified) {
    this.is_modified = is_modified;
    return this;
  }

  public Match withPrepick(String prepick) {
    switch (prepick) {
      case "v1":
        this.prepick = prepickType.v1.name();
        break;
      case "v2":
        this.prepick = prepickType.v2.name();
        break;
      case "streamer":
        this.prepick = prepickType.streamer.name();
        break;
      case "empty":
        this.prepick = prepickType.empty.name();
        break;
    }
    return this;
  }

  public String getPrepick(){
    return this.prepick;
  }

  /* Example
 {
    "id": "0",
    "starts_at": 1539989650019,
    "state": "new",
    "is_closed": false,
    "prepick": "v1",
    "wards": [
      "prepick",
      "first_blood",
      "tower",
      "dragon",
      "herald",
      "baron",
      "elder",
      "ace",
      "inhibitor",
      "nexus"
      ]
    "isNewMatch":true,
    "isModified":false
  }*/

}
