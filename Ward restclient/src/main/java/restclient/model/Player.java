package restclient.model;

public class Player {
  private String id;
  private String title;
  private int regular;
  private String position;

  public String getTitle() {
    return title;
  }

  public String getPosition() {
    if (position.equals("support"))
      return "sup";
    else
      return position;
  }

  public boolean isRegular() {
    if (regular == 1)
      return true;
    else
      return false;
  }

  public String getId() {
    return id;
  }

  public String getFullPosition() {
    return position;
  }
}
