package restclient.model;

import net.bytebuddy.build.ToStringPlugin;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Game {
  private String id;
  private String game_id;
  private String type;
  private String title;
  private String state;
  private List<String> teams;
  private long starts_at;
  private ArrayList<String> tags;
  private ArrayList<String> ads;
  private ArrayList<Event> events;
  private boolean is_closed;
  private String championship;
  private String stage;
  @ToStringPlugin.Exclude
  private int issued_tickets_count;
  @ToStringPlugin.Exclude
  private int tickets_count;
  private ArrayList<Match> matches;


  public String getId() {
    return id;
  }

  public Game withId(String id) {
    if (id == null)
      this.id = "";
    else
      this.id = id;
    return this;
  }

  public String getGame_id() {
    return game_id;
  }

  public Game withGame_id(String game_id) {
    this.game_id = game_id;
    return this;
  }

  public String getType() {
    return type;
  }

  public Game withType(String type) {
    this.type = type;
    return this;
  }

  public String getTitle() {
    return title;
  }

  public Game withTitle(String title) {
    if (title == null)
      this.title = "";
    else
      this.title = title;
    return this;
  }

  public String getState() {
    return state;
  }

  public Game withState(String state) {
    this.state = state;
    return this;
  }

  public List<String> getTeams() {
    return teams;
  }

  public Game withTeams(List<String> teams) {
    this.teams = teams;
    return this;
  }

  public long getStarts_at() {
    return starts_at;
  }

  public Game withStarts_at(long started_at) {
    this.starts_at = started_at;
    return this;
  }

  public List<String> getTags() {
    return tags;
  }

  public Game withTags(ArrayList<String> tags) {
    if (tags == null)
      this.tags = new ArrayList<>();
    else
      this.tags = tags;
    return this;
  }

  public ArrayList<String> getAds() {
    return ads;
  }

  public Game withAds(ArrayList<String> ads) {
    if (ads == null)
      this.ads = new ArrayList<>();
    else
      this.ads = ads;
    return this;
  }

  public ArrayList<Event> getEvents() {
    return events;
  }

  public Game withEvents(ArrayList<Event> events) {
    this.events = events;
    return this;
  }

  public boolean isIs_closed() {
    return is_closed;
  }

  public Game withIs_closed(boolean is_closed) {
    this.is_closed = is_closed;
    return this;
  }

  public String getChampionship() {
    return championship;
  }

  public Game withChampionship(String championship) {
    this.championship = championship;
    return this;
  }

  public String getStage() {
    return stage;
  }

  public Game withStage(String stage) {
    if (stage == null)
      this.stage = "";
    else
      this.stage = stage;
    return this;
  }

  public int getIssued_tickets_count() {
    return issued_tickets_count;
  }

  public Game withIssued_tickets_count(int issued_tickets_count) {
    this.issued_tickets_count = issued_tickets_count;
    return this;
  }

  public int getTickets_count() {
    return tickets_count;
  }

  public Game withTickets_count(int tickets_count) {
    this.tickets_count = tickets_count;
    return this;
  }

  public ArrayList<Match> getMatches() {
    return matches;
  }

  public Game withMatches(ArrayList<Match> matches) {
    this.matches = matches;
    return this;
  }

  public String getFullTime() { // format 18:19
    Timestamp ts = new Timestamp(this.starts_at);
    DateTimeFormatter sdf = DateTimeFormatter.ofPattern("H:mm").withLocale(Locale.US);
    return ts.toLocalDateTime().format(sdf);
  }

  public String getTime() { // format 6:19 PM
    Timestamp ts = new Timestamp(this.starts_at);
    DateTimeFormatter sdf = DateTimeFormatter.ofPattern("h:mm a").withLocale(Locale.US);
    return ts.toLocalDateTime().format(sdf);
  }

  public String getDate() { // format "dd.MM.yyyy"
    Timestamp ts = new Timestamp(this.starts_at);
    DateTimeFormatter sdf = DateTimeFormatter.ofPattern("dd.MM.yyyy").withLocale(Locale.US);
    return ts.toLocalDateTime().format(sdf);
  }
/*  {
    "id":"4ffa6e02-d3f2-11e8-8a92-0a5864600692",
    "game_id":"auto-38482347232",
    "type":"lol",
    "title":"",
    "state":"new",
    "teams":[
      "lpl_909",
      "cdl_938"
      ],
    "starts_at":1539993240000,
    "tags":[
      "test"
      ],
    "ads":null,
    "events":[
      {
      "type":"match_created",
      "id":"5015a278-d3f2-11e8-8a92-0a5864600692",
      "data":{
        "match_id":"0"
        }
      },
      {
      "type":"payload",
      "id":"4ffa6e14-d3f2-11e8-8a92-0a5864600692",
      "data":{
        "id":"4ffa6e14-d3f2-11e8-8a92-0a5864600692"
        }
      }
    ],
    "is_closed":false,
    "championship":"",
    "stage":"",
    "issued_tickets_count":10,
    "tickets_count":10,
    "matches":[
      {
      "id":"0",
      "starts_at":1539989650019,
      "state":"new",
      "is_closed":false,
      "wards":[
        "prepick",
        "first_blood",
        "tower",
        "dragon",
        "herald",
        "baron",
        "elder",
        "ace",
        "inhibitor",
        "nexus"
        ]
      }
    ]
  }*/
}
