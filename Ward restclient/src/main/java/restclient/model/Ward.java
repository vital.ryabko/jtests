package restclient.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ward {
  private String id;
  private Integer duration;
  private Map<String, Question> questions;
  private Long starts_at;
  private List<String> order;

  //Special keys for prepick
  private Long created_at;
  private Integer prepick_reward;
  private Map<String, String> values;

  public Ward withCreated_at(long created_at) {
    this.created_at = created_at;
    return this;
  }

  public Ward withPrepick_reward(int prepick_reward) {
    this.prepick_reward = prepick_reward;
    return this;
  }

  public Ward withValues(Map<String, String> values) {
    this.values = values;
    return this;
  }

  public Ward withId(String id) {
    this.id = id;
    return this;
  }

  public Ward withDuration(int duration) {
    this.duration = duration;
    return this;
  }

  public Ward withQuestions(Map<String, Question> questions) {
    if (questions == null)
      this.questions = new HashMap<>();
    else
      this.questions = questions;
    return this;
  }

  public Ward withStarts_at(long starts_at) {
    this.starts_at = starts_at;
    return this;
  }

  public List<String> getOrder() {
    return order;
  }

  public Ward withOrder(List<String> order) {
    this.order = order;
    return this;
  }

  public String getId() {
    return id;
  }

  public long getCreated_at() {
    return created_at;
  }

  public Map<String, String> getValues() {
    return values;
  }

  public Integer getDuration() {
    return duration;
  }

  public Map<String, Question> getQuestions() {
    return questions;
  }

  public Long getStarts_at() {
    return starts_at;
  }

}
/*          "baron": {
                    "duration": 1300,
                    "id": "baron",
                    "questions": {
                        "team": {
                            "decreasing": true,
                            "description": "manifest_question_baron_desc",
                            "notification": {
                                "default": "manifest_question_baron_notify_default"
                            },
                            "pick_type": "team",
                            "reward": 60000,
                            "subtitle": "WARD 5",
                            "threshold": 1000,
                            "title": "manifest_question_baron_title"
                        }
                    },
                    "starts_at": 900
                }
 ---
 "prepick": {
            "created_at": 1540408104656,
            "id": "prepick",
            "prepick_reward": 10000,
            "questions": {},
            "starts_at": -21600,
            "values": {
                "blinky": "110",
                "crazy": "27",
                "frae": "18",
                "ghost": "412",
                "ignar": "43",
                "jayke": "114",
                "only": "37",
                "pabu": "24",
                "tempt": "7",
                "trick": "223"
            }
        },

*/
