package restclient.model;

import java.util.ArrayList;

public class Hero {
  private String id;
  private String key;
  private String name;
  private String title;
  private String image;
  private ArrayList<String> tags;

  public String getTitle() {
    return title;
  }
/*
   {
      "id": 412,
      "key": "Thresh",
      "name": "Thresh",
      "title": "the Chain Warden",
      "tags": [
        "Support",
        "Fighter"
      ],
      "image": "https://manifest.ward.world/staging/images/lol_champions/Thresh.jpg"
    }
   */
}
