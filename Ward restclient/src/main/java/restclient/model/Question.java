package restclient.model;

import java.util.Map;

public class Question {
  private Boolean decreasing;
  private String description;
  private Map<String, String> notification;
  private String pick_type;
  private Integer reward;
  private String subtitle;
  private Integer threshold;
  private String title;

  public Question() {
  }

  public Boolean getDecreasing() {
    return decreasing;
  }

  public String getDescription() {
    return description;
  }

  public Map<String, String> getNotification() {
    return notification;
  }

  public String getPick_type() {
    return pick_type;
  }

  public Integer getReward() {
    return reward;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public Integer getThreshold() {
    return threshold;
  }

  public String getTitle() {
    return title;
  }
}
/* "team": {
                            "decreasing": true,
                            "description": "manifest_question_dragon_desc",
                            "notification": {
                                "default": "manifest_question_dragon_notify_default"
                            },
                            "pick_type": "team",
                            "reward": 30000,
                            "subtitle": "WARD 3",
                            "threshold": 1000,
                            "title": "manifest_question_dragon_title"
                        }
*/
