package restclient.model;

import java.util.Map;

public class Event {
  private String type;
  private String id;
  private Map<String, String> data;

  public String getType() {
    return type;
  }

  public Event withType(String type) {
    this.type = type;
    return this;
  }

  public String getId() {
    return id;
  }

  public Event withId(String id) {
    this.id = id;
    return this;
  }

  public Map<String, String> getData() {
    return data;
  }

  public Event withData(Map<String, String> data) {
    this.data = data;
    return this;
  }

/* Example
 {
    "type": "payload",
    "id": "99afd6a5-d3f3-11e8-992c-0a58646007ad",
    "data": {
       "id": "99afd6a5-d3f3-11e8-992c-0a58646007ad",
       "match_id": "0"
       }
  }*/
}
