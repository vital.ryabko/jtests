package restclient.model;

import net.bytebuddy.build.ToStringPlugin;

import java.util.HashMap;
import java.util.Map;

public class Payload {
  @ToStringPlugin.Exclude
  private String id;
  @ToStringPlugin.Exclude
  private String game_id;
  @ToStringPlugin.Exclude
  private Map<String, PayloadData> data;
  //For POST request
  private Map<String, String> substitutions;
  private Timer timer;
  private Map<String, Ward> wards;

  public String getId() {
    return id;
  }

  public Map<String, PayloadData> getData() {
    return data;
  }

  public Map<String, Ward> getWards() {
    return wards;
  }

  public Payload withSubstitutions(Map<String, String> substitutions) {
    if (substitutions == null)
      this.substitutions = new HashMap<>();
    else
      this.substitutions = substitutions;
    return this;
  }

  public Payload withTimer(Timer timer) {
    this.timer = timer;
    return this;
  }

  public Payload withEmptyTimer() {
    this.timer = new Timer()
            .withStarted_at(null)
            .withEnded_at(null)
            .withPauses(null);
    return this;
  }

  public Payload withWards(Map<String, Ward> wards) {
    this.wards = wards;
    return this;
  }

  public Map<String, String> getSubstitutions() {
    return substitutions;
  }

  public Timer getTimer() {
    return timer;
  }
}
/*{
    "id": "15f2d6f9-d7a0-11e8-baa3-0a586460027d",
    "game_id": "15f2d6e6-d7a0-11e8-baa3-0a586460027d",
    "data": {
        "0": {
            "substitutions": {
                "huni": "lost"
            },
            "id": "0",
            "wards": {
                "ace": {
                    "duration": 2000,
                    "id": "ace",
                    "order": [
                        "bool",
                        "team"
                    ],
                    "questions": {
                        "bool": {
                            "decreasing": true,
                            "description": "manifest_question_ace_desc",
                            "notification": {
                                "default": "manifest_question_ace_notify_default",
                                "no": "manifest_question_ace_notify_no"
                            },
                            "pick_type": "bool",
                            "reward": 20000,
                            "subtitle": "WARD 7",
                            "threshold": 1000,
                            "title": "manifest_question_ace_title"
                        },
                        "team": {
                            "decreasing": false,
                            "description": "manifest_question_team_ace_desc",
                            "notification": {
                                "default": "manifest_question_team_ace_notify_default"
                            },
                            "pick_type": "team",
                            "reward": 30000,
                            "subtitle": "WARD 7",
                            "threshold": 1000,
                            "title": "manifest_question_bonus_title"
                        }
                    },
                    "starts_at": 1000
                },
                "baron": {
                    "duration": 1300,
                    "id": "baron",
                    "questions": {
                        "team": {
                            "decreasing": true,
                            "description": "manifest_question_baron_desc",
                            "notification": {
                                "default": "manifest_question_baron_notify_default"
                            },
                            "pick_type": "team",
                            "reward": 60000,
                            "subtitle": "WARD 5",
                            "threshold": 1000,
                            "title": "manifest_question_baron_title"
                        }
                    },
                    "starts_at": 900
                },
                "dragon": {
                    "duration": 430,
                    "id": "dragon",
                    "order": [
                        "team",
                        "next_dragon"
                    ],
                    "questions": {
                        "next_dragon": {
                            "decreasing": false,
                            "description": "manifest_question_next_dragon_desc",
                            "notification": {
                                "default": "manifest_question_next_dragon_notify_default"
                            },
                            "pick_type": "next_dragon",
                            "reward": 10000,
                            "subtitle": "WARD 3",
                            "threshold": 1000,
                            "title": "manifest_question_bonus_title"
                        },
                        "team": {
                            "decreasing": true,
                            "description": "manifest_question_dragon_desc",
                            "notification": {
                                "default": "manifest_question_dragon_notify_default"
                            },
                            "pick_type": "team",
                            "reward": 30000,
                            "subtitle": "WARD 3",
                            "threshold": 1000,
                            "title": "manifest_question_dragon_title"
                        }
                    },
                    "starts_at": 170
                },
                "elder": {
                    "duration": 2000,
                    "id": "elder",
                    "order": [
                        "bool",
                        "team_with_neither"
                    ],
                    "questions": {
                        "bool": {
                            "decreasing": true,
                            "description": "manifest_question_elder_desc",
                            "notification": {
                                "default": "manifest_question_elder_notify_default",
                                "no": "manifest_question_elder_notify_no"
                            },
                            "pick_type": "bool",
                            "reward": 20000,
                            "subtitle": "WARD 6",
                            "threshold": 1000,
                            "title": "manifest_question_elder_title"
                        },
                        "team_with_neither": {
                            "decreasing": false,
                            "description": "manifest_question_team_elder_desc",
                            "notification": {
                                "default": "manifest_question_team_elder_notify_default"
                            },
                            "pick_type": "team_with_neither",
                            "reward": 40000,
                            "subtitle": "WARD 6",
                            "threshold": 1000,
                            "title": "manifest_question_bonus_title"
                        }
                    },
                    "starts_at": 960
                },
                "first_blood": {
                    "duration": 200,
                    "id": "first_blood",
                    "order": [
                        "player",
                        "lines_with_jungle"
                    ],
                    "questions": {
                        "lines_with_jungle": {
                            "decreasing": false,
                            "description": "manifest_question_lines_with_jungle_desc",
                            "notification": {
                                "default": "manifest_question_lines_with_jungle_notify_default"
                            },
                            "pick_type": "lines_with_jungle",
                            "reward": 10000,
                            "subtitle": "WARD 1",
                            "threshold": 1000,
                            "title": "manifest_question_bonus_title"
                        },
                        "player": {
                            "decreasing": true,
                            "description": "manifest_question_first_blood_desc",
                            "notification": {
                                "default": "manifest_question_first_blood_notify_default"
                            },
                            "pick_type": "player",
                            "reward": 50000,
                            "subtitle": "WARD 1",
                            "threshold": 1000,
                            "title": "manifest_question_first_blood_title"
                        }
                    },
                    "starts_at": 0
                },
                "herald": {
                    "duration": 780,
                    "id": "herald",
                    "order": [
                        "bool",
                        "team"
                    ],
                    "questions": {
                        "bool": {
                            "decreasing": true,
                            "description": "manifest_question_herald_desc",
                            "notification": {
                                "default": "manifest_question_herald_notify_default",
                                "no": "manifest_question_herald_notify_no"
                            },
                            "pick_type": "bool",
                            "reward": 20000,
                            "subtitle": "WARD 4",
                            "threshold": 1000,
                            "title": "manifest_question_herald_title"
                        },
                        "team": {
                            "decreasing": false,
                            "description": "manifest_question_team_herald_desc",
                            "notification": {
                                "default": "manifest_question_team_herald_notify_default"
                            },
                            "pick_type": "team",
                            "reward": 40000,
                            "subtitle": "WARD 4",
                            "threshold": 1000,
                            "title": "manifest_question_bonus_title"
                        }
                    },
                    "starts_at": 480
                },
                "inhibitor": {
                    "duration": 1500,
                    "id": "inhibitor",
                    "order": [
                        "team",
                        "lines"
                    ],
                    "questions": {
                        "lines": {
                            "decreasing": false,
                            "description": "manifest_question_lines_desc",
                            "notification": {
                                "default": "manifest_question_lines_notify_default"
                            },
                            "pick_type": "lines",
                            "reward": 20000,
                            "subtitle": "WARD 8",
                            "threshold": 1000,
                            "title": "manifest_question_bonus_title"
                        },
                        "team": {
                            "decreasing": true,
                            "description": "manifest_question_inhibitor_desc",
                            "notification": {
                                "default": "manifest_question_inhibitor_notify_default"
                            },
                            "pick_type": "team",
                            "reward": 40000,
                            "subtitle": "WARD 8",
                            "threshold": 1000,
                            "title": "manifest_question_inhibitor_title"
                        }
                    },
                    "starts_at": 1050
                },
                "nexus": {
                    "duration": 1800,
                    "id": "nexus",
                    "questions": {
                        "team": {
                            "decreasing": true,
                            "description": "manifest_question_nexus_desc",
                            "notification": {
                                "default": "manifest_question_nexus_notify_default"
                            },
                            "pick_type": "team",
                            "reward": 50000,
                            "subtitle": "WARD 9",
                            "threshold": 1000,
                            "title": "manifest_question_nexus_title"
                        }
                    },
                    "starts_at": 1200
                },
                "prepick": {
                    "id": "prepick",
                    "prepick_reward": 10000,
                    "questions": null,
                    "starts_at": -21600
                },
                "tower": {
                    "duration": 420,
                    "id": "tower",
                    "order": [
                        "team",
                        "lines"
                    ],
                    "questions": {
                        "lines": {
                            "decreasing": false,
                            "description": "manifest_question_lines_desc",
                            "notification": {
                                "default": "manifest_question_lines_notify_default"
                            },
                            "pick_type": "lines",
                            "reward": 10000,
                            "subtitle": "WARD 2",
                            "threshold": 1000,
                            "title": "manifest_question_bonus_title"
                        },
                        "team": {
                            "decreasing": true,
                            "description": "manifest_question_tower_desc",
                            "notification": {
                                "default": "manifest_question_tower_notify_default"
                            },
                            "pick_type": "team",
                            "reward": 50000,
                            "subtitle": "WARD 2",
                            "threshold": 1000,
                            "title": "manifest_question_tower_title"
                        }
                    },
                    "starts_at": 130
                }
            }
        }
    }
}*/
