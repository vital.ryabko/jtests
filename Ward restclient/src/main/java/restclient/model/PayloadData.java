package restclient.model;

import java.util.Map;

public class PayloadData {
  private String id;
  private Map<String, Ward> wards;
  private Map<String, String> substitutions;
  private Timer timer;

  public Map<String, Ward> getWards() {
    return wards;
  }

  public Timer getTimer() {
    return timer;
  }
}
