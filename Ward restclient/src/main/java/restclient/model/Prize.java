package restclient.model;

public class Prize {
  private String id; //for PUT only
  private String state; //for PUT only
  private String game_id;
  private String type;
  private Integer place;
  private String entity;
  private String redeem_msg;
  private String redeem_msg_ru;
  private String user_id;
  private String user_claim;
  private String match_id;

  public Prize withId(String id) {
    this.id = id;
    return this;
  }

  public Prize withState(String state) {
    this.state = state;
    return this;
  }

  public String getGame_id() {
    return game_id;
  }

  public Prize withGame_id(String game_id) {
    this.game_id = game_id;
    return this;
  }

  public String getType() {
    return type;
  }

  public Prize withType(String type) {
    this.type = type;
    return this;
  }

  public Integer getPlace() {
    return place;
  }

  public Prize withPlace(Integer place) {
    this.place = place;
    return this;
  }

  public String getEntity() {
    return entity;
  }

  public Prize withEntity(String entity) {
    this.entity = entity;
    return this;
  }

  public String getRedeem_msg() {
    return redeem_msg;
  }

  public Prize withRedeem_msg(String redeem_msg) {
    this.redeem_msg = redeem_msg;
    return this;
  }

  public String getRedeem_msg_ru() {
    return redeem_msg_ru;
  }

  public Prize withRedeem_msg_ru(String redeem_msg_ru) {
    this.redeem_msg_ru = redeem_msg_ru;
    return this;
  }

  public String getMatch_id() {
    return match_id;
  }

  public Prize withMatch_id(String match_id) {
    this.match_id = match_id;
    return this;
  }
}

/*
{
        "game_id": "d78697af-ef39-11e8-b1b5-3e87fd280030",
        "type": "match",
        "place": 1,
        "entity": "tripleprize",
        "redeem_msg": "EN sms",
        "redeem_msg_ru": "RU ama",
        "user_id": "",
        "user_claim": "",
        "match_id": "0"
    }
 */

/* To PUT changes
{
  "id":"match:1:0",
  "game_id":"",
  "entity":"tripleprize",
  "type":"match",
  "place":1,
  "redeem_msg":"EN sms",
  "redeem_msg_ru":"RU sms",
  "user_id":"",
  "state":"new",
  "match_id":"0"}
 */
