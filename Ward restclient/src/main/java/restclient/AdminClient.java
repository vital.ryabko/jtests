package restclient;

import com.google.gson.*;
import net.bytebuddy.build.ToStringPlugin;
import restclient.model.*;
import restclient.model.Timer;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class AdminClient {
  //private Timestamp timestamp = new Timestamp(System.currentTimeMillis());
  private static final String STAGE = "staging";
  private static final String API_PATH = "https://api-" + STAGE + ".ward.world";
  private static final String ADMIN_URI = API_PATH + "/games/v2";
  private static final String MANIFEST_URI = "https://manifest.ward.world/" + STAGE + "/m.lol.v1.2.json";
  private static final String API_TOKEN = "secret";
  private String REFERER_PATH = "https://monitor-staging.wardapp.xyz/game/";
  private String ADMIN_REFERER_PATH = "https://admin-staging.wardapp.xyz/game/new"; //https://admin-staging.wardapp.xyz/game/new
  private String ADMIN_ORIGIN_PATH = "https://admin-staging.ward.world";
  private String GAME_PRIZES_PATH = "prizes/v2/admin/games"; //https://api-staging.ward.world/prizes/v2/admin/games/d78697af-ef39-11e8-b1b5-3e87fd280030
  private String CURRENT_GAME_ID;
  private String CURRENT_PAYLOAD_ID;
  private int MATCH_ID = 0;
  private String GAME_CREATE_PATH = "";
  private String GAMES_LIST_PATH = "types/lol/active";
  private String GAME_PAYLOAD_PATH;// = CURRENT_GAME_ID + "/payloads/" + CURRENT_PAYLOAD_ID;
  private String GAME_EVENT_UPDATE_PATH;// = CURRENT_GAME_ID + "/match/" + MATCH_ID + "/events";
  private String GAME_PAYLOAD_UPDATE_PATH;// = CURRENT_GAME_ID + "/match/" + MATCH_ID + "/payloads"; //POST here to update

  private enum GAME_TYPE {
    lol, dota
  }

  private enum WARDS {
    prepick, first_blood, tower, dragon, herald, baron, elder, ace, inhibitor, nexus
  }

  private enum EVENT_TYPE {
    closed, match_started
  }

  private Client client = ClientBuilder.newClient();

  private WebTarget webTarget = client.target(ADMIN_URI);

  public Response getLOLActiveGames() {
    return webTarget
            .path(GAMES_LIST_PATH)
            .request(MediaType.APPLICATION_JSON)
            .header("Authorization", API_TOKEN)
            .get();
  }

  public Response createNewGame(String gameId, Timestamp startTimestamp, List<String> teams) {
    Gson gson = new GsonBuilder().setExclusionStrategies(new AnnotationExclusionStrategy()).create();
    ArrayList<Match> matches = new ArrayList<Match>();
    Match defaultMatches = new Match().withId("0")
            .withStarts_at(startTimestamp.getTime())
            .withState("new")
            .withWards(defaultWards())
            .withIs_new_match(true)
            .withIs_modified(false);
    matches.add(defaultMatches);

    Game newGame = new Game()
            .withId(null)
            .withGame_id(gameId)
            .withType(GAME_TYPE.lol.name())
            .withTitle(null)
            .withState("new")
            .withTeams(convertedTeamsFromNames(teams))
            .withStarts_at(startTimestamp.getTime())
            .withTags(null)
            .withAds(null)
            .withStage(null)
            .withMatches(matches);
    System.out.println(gson.toJson(newGame, Game.class));

    return webTarget
            .path(GAME_CREATE_PATH)
            .request(MediaType.APPLICATION_JSON)
            .header("Origin", ADMIN_ORIGIN_PATH)
            .header("Authorization", API_TOKEN)
            .header("Referer", ADMIN_REFERER_PATH)
            .post(Entity.entity(newGame, MediaType.APPLICATION_JSON));
  }

  public Payload getCustomGamePayload(String gameID, String payloadID) {
    Gson gson = new Gson();
    this.CURRENT_GAME_ID = gameID;
    this.CURRENT_PAYLOAD_ID = payloadID;
    this.GAME_PAYLOAD_PATH = CURRENT_GAME_ID + "/payloads/" + CURRENT_PAYLOAD_ID;
    Response response = webTarget
            .path(GAME_PAYLOAD_PATH)
            .request(MediaType.APPLICATION_JSON)
            .header("Authorization", API_TOKEN)
            .get();
    if (response.getStatus() != 200) {
      System.out.println(String.format("ERROR with request. Body of response is: %s", response.readEntity(String.class)));
      return null;
    }
    return gson.fromJson(response.readEntity(String.class), Payload.class);
  }

  public Payload postPrepicksToGame(Game game, Ward prepick) {
    return postPrepicksToGame(game.getId(), prepick);
  }

  public Payload postPrepicksToGame(String gameID, Ward prepick) {
    Gson gson = new GsonBuilder().setExclusionStrategies(new AnnotationExclusionStrategy()).serializeNulls().create();
    Gson rgson = new Gson();
    Payload newPayload = new Payload()
            .withWards(Map.of("prepick", prepick))
            .withSubstitutions(null);
    String payload_final = gson.toJson(newPayload, Payload.class);
    System.out.println(String.format("Json to POST is: %s", payload_final));

    this.CURRENT_GAME_ID = gameID;
    this.GAME_PAYLOAD_UPDATE_PATH = CURRENT_GAME_ID + "/match/" + MATCH_ID + "/payloads";
    Response response = webTarget
            .path(GAME_PAYLOAD_UPDATE_PATH)
            .request(MediaType.APPLICATION_JSON)
            .header("Authorization", API_TOKEN)
            .post(Entity.entity(payload_final, MediaType.APPLICATION_JSON));

    return rgson.fromJson(response.readEntity(String.class), Payload.class);
  }

  public Payload postStartGame(Game game, Map<String, Ward> all_wards) {
    return postStartGame(game.getId(), all_wards);
  }

  public Payload postStartGame(String gameID, Map<String, Ward> all_wards) {
    Gson tgson = new GsonBuilder().setExclusionStrategies(new AnnotationExclusionStrategy()).serializeNulls().create();
    Gson gson = new Gson();
    Date date = new Date();
    Timestamp timestamp = new Timestamp(date.getTime());
    long current_time = timestamp.getTime();

    Timer timer = new Timer()
            .withStarted_at(current_time)
            .withEnded_at(null)
            .withPauses(null);

    Payload newPayload = new Payload()
            .withTimer(timer)
            .withSubstitutions(null)
            .withWards(all_wards);

    this.CURRENT_GAME_ID = gameID;
    this.GAME_PAYLOAD_UPDATE_PATH = CURRENT_GAME_ID + "/match/" + MATCH_ID + "/payloads";
    String timer_payload = tgson.toJson(newPayload, Payload.class);
    Response payload_timer = webTarget
            .path(GAME_PAYLOAD_UPDATE_PATH)
            .request(MediaType.APPLICATION_JSON)
            .header("Authorization", API_TOKEN)
            .post(Entity.entity(timer_payload, MediaType.APPLICATION_JSON));
    newPayload = gson.fromJson(payload_timer.readEntity(String.class), Payload.class);
    //System.out.println(String.format("Uploading timer Payload successfull - %b", payload_timer.getStatus() == 200));
    //System.out.println(String.format("Response json from uploading: %s", payload_timer.readEntity(String.class)));
    this.GAME_EVENT_UPDATE_PATH = CURRENT_GAME_ID + "/match/" + MATCH_ID + "/events";
    Map<String, String> event_type = Map.of("type", EVENT_TYPE.match_started.name());
    webTarget
            .path(GAME_EVENT_UPDATE_PATH)
            .request(MediaType.APPLICATION_JSON)
            .header("Authorization", API_TOKEN)
            .post(Entity.entity(event_type, MediaType.APPLICATION_JSON));
    return newPayload;
  }

  //TODO Need to test
  public Payload postWardsUpdate(String gameID, Payload current_payload, Map<String, Ward> new_ward) {
    Gson tgson = new GsonBuilder().setExclusionStrategies(new AnnotationExclusionStrategy()).serializeNulls().create();
    Gson gson = new Gson();

    //current_wards - actual wards from last payload
    //new_ward - updated ward data
    Map<String, Ward> current_wards = current_payload.getWards();
    for (String w : new_ward.keySet()) {
      current_wards.put(w, new_ward.get(w));
    }
    //Update last payload with new ward data to POST updated payload
    current_payload.withWards(current_wards);

    String updated_payload = tgson.toJson(current_payload, Payload.class);

    this.CURRENT_GAME_ID = gameID;
    this.GAME_PAYLOAD_UPDATE_PATH = CURRENT_GAME_ID + "/match/" + MATCH_ID + "/payloads";
    Response payload_timer = webTarget
            .path(GAME_PAYLOAD_UPDATE_PATH)
            .request(MediaType.APPLICATION_JSON)
            .header("Authorization", API_TOKEN)
            .post(Entity.entity(updated_payload, MediaType.APPLICATION_JSON));
    return gson.fromJson(payload_timer.readEntity(String.class), Payload.class);
  }

  public Response postEndGameSeries(String gameID) {
    this.CURRENT_GAME_ID = gameID;
    this.GAME_EVENT_UPDATE_PATH = CURRENT_GAME_ID + "/events";
    Map<String, String> event_type = Map.of("type", EVENT_TYPE.closed.name());
    Gson gson = new Gson();
    String event = gson.toJson(event_type, Map.class);
    return webTarget
            .path(GAME_EVENT_UPDATE_PATH)
            .request(MediaType.APPLICATION_JSON)
            .header("Authorization", API_TOKEN)
            .post(Entity.entity(event, MediaType.APPLICATION_JSON));
  }

  public Response postEndGameSeries(Game game) {
    return postEndGameSeries(game.getId());
  }

  public List<Prize> getPrizesForCustomGame(String gameID) {
    List<Prize> prizeList = new ArrayList<>();
    Gson gson = new Gson();
    CURRENT_GAME_ID = gameID;
    String updatePrizePath = GAME_PRIZES_PATH + "/" + CURRENT_GAME_ID;

    System.out.println("Path to GET from: " + API_PATH + updatePrizePath);
    webTarget = client.target(API_PATH);
    Response response = webTarget
            .path(updatePrizePath)
            .request(MediaType.APPLICATION_JSON)
            .header("Authorization", API_TOKEN)
            .get();

    if (response.getStatus() != 200) {
      System.out.println(String.format("ERROR with request. Body of response is: %s", response.readEntity(String.class)));
      return null;
    }

    JsonArray prizes = gson.fromJson(response.readEntity(String.class), JsonArray.class);
    System.out.println("Full array of prizes: " + prizes.toString());
    for (JsonElement p: prizes){
      System.out.println("Found prize is: " + p.toString());
      prizeList.add(gson.fromJson(p.toString(),Prize.class));
    }
    return prizeList;
  }

  public Game getGameInfo(String gameID) {
    Gson gson = new GsonBuilder().setExclusionStrategies(new AnnotationExclusionStrategy()).serializeNulls().create();
    CURRENT_GAME_ID = gameID;
    Response getGameData = webTarget
            .path(CURRENT_GAME_ID)
            .request(MediaType.APPLICATION_JSON)
            .header("Authorization", API_TOKEN)
            .get();
    return gson.fromJson(getGameData.readEntity(String.class), Game.class);
  }

  public Ward makePrepickWardForTeams(String firstTeamName, String secondTeamName) throws IOException {
    List<String> firstTeamPlayers = getListOfRegularTeamPlayers(firstTeamName);
    List<String> secondTeamPlayers = getListOfRegularTeamPlayers(secondTeamName);
    List<String> playersList = merge(firstTeamPlayers, secondTeamPlayers);
    List<String> heroesList = Arrays.asList(new String[]{"21", "22", "23", "24", "25", "31", "32", "33", "34", "35"});
    Map<String, String> values = zipToMap(playersList, heroesList);

    Date date = new Date();
    Timestamp timestamp = new Timestamp(date.getTime());
    long created_time = timestamp.getTime();

    return new Ward()
            .withId("prepick")
            .withStarts_at(-21600)
            .withQuestions(null)
            .withValues(values)
            .withCreated_at(created_time)
            .withPrepick_reward(10000);
  }

  public Map<String, Ward> setFirstBloodWard(String gameID, Team team, String player_position, String line_name) {
    Ward fb = getDefaultWardsForGame(gameID).get("first_blood");
    String player_id = team.getPlayers().stream()
            .filter(player -> player.isRegular())
            .filter(player -> player_position.equals(player.getPosition()))
            .map(player -> player.getId())
            .collect(Collectors.joining());
    System.out.println(String.format("Player ID to FB results is: %s", player_id));

    Date date = new Date();
    Timestamp timestamp = new Timestamp(date.getTime());
    long current_time = timestamp.getTime();

    Map<String, String> values = Map.of("player", player_id, "lines_with_jungle", line_name);

    return Map.of("first_blood", fb.withValues(values).withCreated_at(current_time));
  }

  public Map<String, Ward> setFirstBloodWard(Game game, Team team, String player_position, String line_name) {
    return setFirstBloodWard(game.getId(), team, player_position, line_name);
  }

  public Map<String, Ward> setTowerWardTeam(String gameID, Team team) {
    Ward tower = getDefaultWardsForGame(gameID).get("tower");
    String team_id = team.getId();
    System.out.println(String.format("Team ID to Tower results is: %s", team_id));

    Date date = new Date();
    Timestamp timestamp = new Timestamp(date.getTime());
    long current_time = timestamp.getTime();

    Map<String, String> values = Map.of("team", team_id);

    return Map.of("tower", tower.withValues(values).withCreated_at(current_time));
  }

  public Map<String, Ward> setTowerWardTeam(Game game, Team team) {
    return setTowerWardTeam(game.getId(), team);
  }

  public Map<String, Ward> setTowerWardLine(String gameID, String line_name) {
    Ward tower = getDefaultWardsForGame(gameID).get("tower");
    System.out.println(String.format("Line to Tower results is: %s", line_name));

    Date date = new Date();
    Timestamp timestamp = new Timestamp(date.getTime());
    long current_time = timestamp.getTime();

    Map<String, String> values = Map.of("line", line_name);

    return Map.of("tower", tower.withValues(values).withCreated_at(current_time));
  }

  public Map<String, Ward> setTowerWardLine(Game game, String line_name) {
    return setTowerWardLine(game.getId(), line_name);
  }

  public Map<String, Ward> getDefaultWardsForGame(String gameID) {
    Gson gson = new GsonBuilder().setExclusionStrategies(new AnnotationExclusionStrategy()).serializeNulls().create();
    this.CURRENT_GAME_ID = gameID;
    this.REFERER_PATH = REFERER_PATH + CURRENT_GAME_ID;
    Response getGameData = webTarget
            .path(CURRENT_GAME_ID)
            .request(MediaType.APPLICATION_JSON)
            .header("Authorization", API_TOKEN)
            .header("Referer", REFERER_PATH)
            .get();
    Game last_game = gson.fromJson(getGameData.readEntity(String.class), Game.class);

    last_game.getEvents().forEach(e -> {
      if (e.getType().equals("payload"))
        this.CURRENT_PAYLOAD_ID = e.getId();
    });

    Payload initialPayload = getCustomGamePayload(CURRENT_GAME_ID, CURRENT_PAYLOAD_ID);

    return initialPayload.getData().get("0").getWards();
  }

  public Map<String, Ward> getDefaultWardsForGame(Game game) {
    return getDefaultWardsForGame(getGameID(game));
  }

  public String getLastPayloadIdForGame(String gameID) {
    Gson gson = new GsonBuilder().setExclusionStrategies(new AnnotationExclusionStrategy()).serializeNulls().create();
    this.CURRENT_GAME_ID = gameID;
    Response getGameData = webTarget
            .path(CURRENT_GAME_ID)
            .request(MediaType.APPLICATION_JSON)
            .header("Authorization", API_TOKEN)
            .get();
    Game last_game = gson.fromJson(getGameData.readEntity(String.class), Game.class);

    return last_game.getEvents().stream()
            .filter(event -> event.getType().equals("payload"))
            .findFirst()
            .map(event -> event.getId())
            .get();
  }

  public void getAnyDataFromPayload(String gameID, String payloadID) {
    Payload payload = getCustomGamePayload(gameID, payloadID);
    for (Map.Entry<String, PayloadData> n : payload.getData().entrySet()) { //get data map
      PayloadData data = n.getValue(); //get PayloadData for each data element (for each match??)
      for (Map.Entry<String, Ward> w : data.getWards().entrySet()) {
        if (w.getKey().equals("prepick")) //get prepick ward and it's data
          System.out.println(w.getValue().getCreated_at());
      }
    }
  }

  public Map<String, String> getPrepickValuesMapFromPayload(String gameID, String payloadID) {
    Payload payload = getCustomGamePayload(gameID, payloadID);
    for (Map.Entry<String, PayloadData> n : payload.getData().entrySet()) { //get data map
      PayloadData data = n.getValue(); //get PayloadData for each data element (for each match??)
      for (Map.Entry<String, Ward> w : data.getWards().entrySet()) {
        if (w.getKey().equals("prepick")) { //get prepick ward and it's data
          return w.getValue().getValues();
        }
      }
    }
    return null;
  }

  //TODO need to test
  public long getGameStartTimestamp(String gameID, String payloadID) {
    Payload payload = getCustomGamePayload(gameID, payloadID);
    for (Map.Entry<String, PayloadData> n : payload.getData().entrySet()) { //get data map
      PayloadData data = n.getValue(); //get PayloadData for each data element (for each match??)
      return data.getTimer().getStarted_at();
    }
    return -1;
  }

  //TODO need to test
  public long getFBStartTimestamp(String gameID, String payloadID) {
    Payload payload = getCustomGamePayload(gameID, payloadID);
    for (Map.Entry<String, PayloadData> n : payload.getData().entrySet()) { //get data map
      PayloadData data = n.getValue(); //get PayloadData for each data element (for each match??)
      for (Map.Entry<String, Ward> w : data.getWards().entrySet()) {
        if (w.getKey().equals("first_blood")) { //get prepick ward and it's data
          return w.getValue().getCreated_at();
        }
      }
    }
    return -1;
  }

  public Team parseTeamfromManifest(String teamName) throws IOException {
    Team t = new Team();
    JsonObject json = readJsonFromUrl(MANIFEST_URI);
    JsonObject teams = json.get("teams").getAsJsonObject();
    for (String key : teams.keySet()) {
      JsonObject o = teams.get(key).getAsJsonObject();
      if (teamName.equals(o.get("title").getAsString())) {
        t = new Gson().fromJson(o.toString(), Team.class);
      }
    }
    return t;
  }

  public Hero parseHerofromManifest(String heroName) throws IOException {
    Hero t = new Hero();
    JsonObject json = readJsonFromUrl(MANIFEST_URI);
    JsonArray heroes = json.get("heroes").getAsJsonArray();
    for (JsonElement h : heroes) {
      String hero = h.getAsJsonObject().get("name").getAsString();
      if (heroName.equals(hero))
        t = new Gson().fromJson(h.toString(), Hero.class);
    }
    return t;
  }

  public Team parseTeamfromManifestByID(String teamID) throws IOException {
    Team t = new Team();
    JsonObject json = readJsonFromUrl(MANIFEST_URI);
    JsonObject teams = json.get("teams").getAsJsonObject();
    for (String key : teams.keySet()) {
      JsonObject o = teams.get(key).getAsJsonObject();
      if (teamID.equals(o.get("id").getAsString())) {
        t = new Gson().fromJson(o.toString(), Team.class);
      }
    }
    return t;
  }

  public boolean isGameInActiveList(Game game) {
    Gson gson = new Gson();
    Response response = getLOLActiveGames();
    JsonArray games = gson.fromJson(response.readEntity(String.class), JsonArray.class);
    for (JsonElement g : games) {
      if (g.getAsJsonObject().get("starts_at").getAsLong() == game.getStarts_at()) {
        JsonArray teams = g.getAsJsonObject().get("teams").getAsJsonArray();
        if (game.getTeams().get(0).equals(teams.get(0).getAsString()))
          if (game.getTeams().get(1).equals(teams.get(1).getAsString()))
            return true;
      }
    }
    return false;
  }

  public Game isGameInActiveList(String firstTeam, String secondTeam, Timestamp timestamp) {
    Gson gson = new Gson();
    Response response = getLOLActiveGames();
    JsonArray games = gson.fromJson(response.readEntity(String.class), JsonArray.class);
    for (JsonElement g : games) {
      if (g.getAsJsonObject().get("starts_at").getAsLong() == timestamp.getTime()) {
        JsonArray teams = g.getAsJsonObject().get("teams").getAsJsonArray();
        if (firstTeam.equals(teams.get(0).getAsString()))
          if (secondTeam.equals(teams.get(1).getAsString()))
            return gson.fromJson(g, Game.class);
      }
    }
    return null;
  }

  public String getGameID(Game game) {
    Gson gson = new Gson();
    Response response = getLOLActiveGames();
    JsonArray games = gson.fromJson(response.readEntity(String.class), JsonArray.class);
    for (JsonElement g : games) {
      if (g.getAsJsonObject().get("starts_at").getAsLong() == game.getStarts_at()) {
        JsonArray teams = g.getAsJsonObject().get("teams").getAsJsonArray();
        if (game.getTeams().get(0).equals(teams.get(0).getAsString()))
          if (game.getTeams().get(1).equals(teams.get(1).getAsString()))
            return g.getAsJsonObject().get("id").getAsString();
      }
    }
    return null;
  }

  public String getGameID(String firstTeam, String secondTeam, Timestamp timestamp) {
    Gson gson = new Gson();
    Response response = getLOLActiveGames();
    JsonArray games = gson.fromJson(response.readEntity(String.class), JsonArray.class);
    for (JsonElement g : games) {
      if (g.getAsJsonObject().get("starts_at").getAsLong() == timestamp.getTime()) {
        JsonArray teams = g.getAsJsonObject().get("teams").getAsJsonArray();
        if (firstTeam.equals(teams.get(0).getAsString()))
          if (secondTeam.equals(teams.get(1).getAsString()))
            return g.getAsJsonObject().get("id").getAsString();
      }
    }
    return null;
  }

  public List<Event> getEventsOfGame(String gameID){
    Game currentGame = getGameInfo(gameID);
    return currentGame.getEvents();
  }

  //=================
  //Service methods
  //=================

  public Payload convertFullPayloadToPOSTView(Payload full_payload) {
    return new Payload()
            .withTimer(full_payload.getTimer())
            .withSubstitutions(full_payload.getSubstitutions())
            .withWards(full_payload.getWards());
  }

  private List<String> defaultWards() {
    ArrayList<String> wards = new ArrayList<String>();
    wards.add(WARDS.prepick.name());
    wards.add(WARDS.first_blood.name());
    wards.add(WARDS.tower.name());
    wards.add(WARDS.dragon.name());
    wards.add(WARDS.herald.name());
    wards.add(WARDS.baron.name());
    wards.add(WARDS.elder.name());
    wards.add(WARDS.ace.name());
    wards.add(WARDS.inhibitor.name());
    wards.add(WARDS.nexus.name());
    return wards;
  }

  public static List<String> convertedTeamsFromNames(List<String> teamsNames) {
    return teamsNames.stream().map(n -> {
      try {
        n = getTeamIdbyTitle(n);
      } catch (IOException e) {
        e.printStackTrace();
      }
      return n;
    }).collect(Collectors.toList());
  }

  public static String getTeamIdbyTitle(String teamName) throws IOException {
    JsonObject json = readJsonFromUrl(MANIFEST_URI);
    JsonObject teams = json.get("teams").getAsJsonObject();
    for (String key : teams.keySet()) {
      JsonObject o = teams.get(key).getAsJsonObject();
      if (teamName.equals(o.get("title").getAsString())) {
        return o.get("id").getAsString();
      }
    }
    return null;
  }

  public List<String> getListOfRegularTeamPlayers(String teamName) throws IOException {
    List<String> players = new ArrayList<>();
    JsonObject json = readJsonFromUrl(MANIFEST_URI);
    JsonObject teams = json.get("teams").getAsJsonObject();
    for (String key : teams.keySet()) {
      JsonObject o = teams.get(key).getAsJsonObject();
      if (teamName.equals(o.get("title").getAsString())) {
        JsonArray jplayers = o.get("players").getAsJsonArray();
        for (JsonElement p : jplayers) {
          if (p.getAsJsonObject().get("regular").getAsInt() == 1)
            players.add(p.getAsJsonObject().get("id").getAsString());
        }
      }
    }
    return players;
  }

  public String getPlayerIdByTitle(String playerName) throws IOException {
    JsonObject json = readJsonFromUrl(MANIFEST_URI);
    JsonObject teams = json.get("teams").getAsJsonObject();
    for (String key : teams.keySet()) {
      JsonObject o = teams.get(key).getAsJsonObject(); //this object is one team
      JsonArray team_players = o.get("players").getAsJsonArray(); //this is JsonArray of current team
      for (JsonElement player : team_players) { //for every entity in "players" of "team"
        String current_player = player.getAsJsonObject().get("title").getAsString();
        if (playerName.equals(current_player)) //check if the player title is that we're looking for
          return player.getAsJsonObject().get("id").getAsString();
      }
    }
    return null;
  }

  public String getHeroIdByTitle(String heroName) throws IOException {
    JsonObject json = readJsonFromUrl(MANIFEST_URI);
    JsonArray heroes = json.get("heroes").getAsJsonArray();
    for (JsonElement hero : heroes) {
      if (heroName.equals(hero.getAsJsonObject().get("name").getAsString()))
        return hero.getAsJsonObject().get("id").getAsString();
    }
    return null;
  }

  private static JsonObject readJsonFromUrl(String sURL) throws IOException {
    URL url = new URL(sURL);
    URLConnection request = url.openConnection();
    request.connect();
    JsonParser jp = new JsonParser(); //from gson
    JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element
    return root.getAsJsonObject();
  }

  public static <T> List<T> merge(List<T> list1, List<T> list2) {
    return Stream.of(list1, list2)
            .flatMap(x -> x.stream())
            .collect(Collectors.toList());
  }

  private static <K, V> Map<K, V> zipToMap(List<K> keys, List<V> values) {
    Iterator<K> keyIter = keys.iterator();
    Iterator<V> valIter = values.iterator();
    return IntStream.range(0, keys.size()).boxed()
            .collect(Collectors.toMap(_i -> keyIter.next(), _i -> valIter.next()));
  }

  private class AnnotationExclusionStrategy implements ExclusionStrategy {

    @Override
    public boolean shouldSkipField(FieldAttributes f) {
      return f.getAnnotation(ToStringPlugin.Exclude.class) != null;
    }

    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
      return false;
    }
  }
/* {
"id":"",
"game_id":"auto-stage1",
"type":"lol",
"title":"",
"teams":[
    "54",
    "33"
    ]
"starts_at":1540241760000,
"state":"new",
"tags":[
    ],
"ads":[
    ],
"stage":"",
"matches":[
    {
    "id":"0",
    "starts_at":1540155386013,
    "state":"new",
    "wards":[
        "prepick",
        "first_blood",
        "tower",
        "dragon",
        "herald",
        "baron",
        "elder",
        "ace",
        "inhibitor",
        "nexus"
     ],
"isNewMatch":true,
"isModified":false
}
],
}
*/
}
