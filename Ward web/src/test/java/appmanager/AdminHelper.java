package appmanager;

import com.google.gson.Gson;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import restclient.AdminClient;
import restclient.model.*;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class AdminHelper extends BaseHelpers {

  private AdminClient admin = new AdminClient();

  public AdminHelper(WebDriver browser) {
    super(browser);
  }

  public Game createGame(String gameType, Team firstTeam, Team secondTeam, String startHours, String startMinutes) throws ParseException {
    initAdmin();
    //Click "Create" button
    new WebDriverWait(browser, 180)
            .until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Create new']"))).click();
    //Choose game type
    browser.findElement(By.xpath(String.format("//option[@value='%s']", gameType))).click();
    //Choose first team
    browser.findElement(By.xpath("//input[@name='team_1']")).sendKeys(firstTeam.getTitle());
    browser.findElement(By.xpath(String.format("//div[text()='%s']",firstTeam.getTitle()))).click();
    //Choose second team
    browser.findElement(By.xpath("//input[@name='team_2']")).sendKeys(secondTeam.getTitle());
    browser.findElement(By.xpath(String.format("//div[text()='%s']",secondTeam.getTitle()))).click();
    //Time in backend format
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
    String today = java.time.LocalDate.now().toString();
    Date parsedDate = dateFormat.parse(today + " " + startHours + ":" + startMinutes + ":00");
    Timestamp gameTimestamp = new Timestamp(parsedDate.getTime());
    /*Time in web format (Safari only)
    DateTimeFormatter sdf = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withLocale(Locale.US);
    String gameTime = gameTimestamp.toLocalDateTime().format(sdf);*/
    //Set game time on page
    WebElement date = browser.findElement(By.name("starts_at"));
    date.click();
    //date.sendKeys(Keys.chord(Keys.COMMAND, "a")); This one is for Safari where field is different
    //date.sendKeys(gameTime);
    date.sendKeys(Keys.ARROW_RIGHT);
    date.sendKeys(Keys.ARROW_LEFT);
    date.sendKeys(startHours);
    date.sendKeys(Keys.ARROW_RIGHT);
    date.sendKeys(startMinutes);
    //Make gameId
    String gameId = "auto-" + gameTimestamp;
    //Insert unique game ID
    WebElement timeField = browser.findElement(By.name("game_id"));
    timeField.click();
    timeField.sendKeys(Keys.chord(Keys.CONTROL, "a"));
    timeField.sendKeys(gameId);
    //Click Save button
    browser.findElement(By.xpath("//button[@type='submit']")).click();

    //Make an array of teams by their ids
    List<String> teamsByNames = new ArrayList<>();
    teamsByNames.add(firstTeam.getTitle());
    teamsByNames.add(secondTeam.getTitle());
    List<String> teamsByIds = admin.convertedTeamsFromNames(teamsByNames);

    return new Game().withGame_id(gameId).withTeams(teamsByIds).withStarts_at(gameTimestamp.getTime());
  }

  public Game createGame(String gameType, Team firstTeam, Team secondTeam, int shiftingMinutes) throws ParseException {
    //Set shifting time
    Calendar cal = setShiftingMinutes(shiftingMinutes);
    String startHours = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
    String startMinutes = String.valueOf(cal.get(Calendar.MINUTE));

    return createGame(gameType, firstTeam, secondTeam, startHours, startMinutes);
  }

  public boolean doesGameIdExistOnAdminPage(String gameId){
    boolean lastTry = false;
    do {
      try { //when we are in the end of the list, "Create new" button shouldn't be found
        browser.findElement(By.xpath("//button[text()='Create new']"));
      } catch (NoSuchElementException e){
        lastTry = true;
      }
      try {
        browser.findElement(By.xpath(String.format("//a[contains(@href,'%s')]", gameId)));
        return true;
      } catch (NoSuchElementException e){
        System.out.println("Can't find the game... Scrolling...");
        scrollDownToFooter();
      }
    } while (!lastTry);
    System.out.println("Game is not found in the list.");
    return false;
  }

  public void goToGameWithIdOnAdminPage(String gameId){
    initAdmin();
    boolean lastTry = false;
    do {
      try { //when we are in the end of the list, "Create new" button shouldn't be found
        browser.findElement(By.xpath("//button[text()='Create new']"));
      } catch (NoSuchElementException e){
        lastTry = true;
      }
      try {
        browser.findElement(By.xpath(String.format("//a[contains(@href,'%s')]", gameId))).click();
        break;
      } catch (NoSuchElementException e){
        System.out.println("Can't find the game... Scrolling...");
        scrollDownToFooter();
      }
    } while (!lastTry);
    System.out.println("Game is not found in the list.");
  }

  public void setMatchPrizeOnPage(int matchID, int place, String prizeType) {
    boolean lastTry = false;
    do {
      try { //when we are in the end of the list, "Create new" button shouldn't be found
        browser.findElement(By.xpath("//button[text()='Create new']"));
      } catch (NoSuchElementException e) {
        lastTry = true;
      }
      try {
        //Press New button
        browser.findElement(By.xpath("//span[text()='Match Prizes']//following::button[text()='New']")).click();
        //Find MatchID selector
        browser.findElement(By.xpath("//span[text()='Match Prizes']//following::select[@name='match_id']")).sendKeys(String.valueOf(matchID));
        //Find place input
        browser.findElement(By.xpath("//span[text()='Match Prizes']//following::input[@name='place']")).sendKeys(String.valueOf(place));
        //Find prize type selector
        browser.findElement(By.xpath("//span[text()='Match Prizes']//following::select[@name='entity']")).sendKeys(prizeType);
        //Find Redeem EN textfield
        browser.findElement(By.xpath("//span[text()='Match Prizes']//following::textarea[@name='redeem_msg']")).sendKeys("EN sms");
        //Find Redeem RU textfield
        browser.findElement(By.xpath("//span[text()='Match Prizes']//following::textarea[@name='redeem_msg_ru']")).sendKeys("RU sms");
        //Press Add New Prize button
        browser.findElement(By.xpath("//button[text()='Add New Prize']")).click();
      } catch (NoSuchElementException e) {
        System.out.println("Ooops! Can't find Prizes block! Scrolling...");
        scrollDownToFooter();
      }
    } while (!lastTry);
    assertThat(browser.findElement(By.xpath("//div[contains(text(),'Match #')]//following-sibling::div[2]")).getText(), equalTo(prizeType));
    System.out.println(String.format("Prize %s was set to %s place of %s match.", prizeType, place, matchID));
  }

  public void setSeriesPrizeOnPage(int place, String prizeType) {
    boolean lastTry = false;
    do {
      try { //when we are in the end of the list, "Create new" button shouldn't be found
        browser.findElement(By.xpath("//button[text()='Create new']"));
      } catch (NoSuchElementException e) {
        lastTry = true;
      }
      try {
        //Press New button
        browser.findElement(By.xpath("//span[text()='Game Prizes']//following::button[text()='New']")).click();
        //Find place input
        browser.findElement(By.xpath("//span[text()='Game Prizes']//following::input[@name='place']")).sendKeys(String.valueOf(place));
        //Find prize type selector
        browser.findElement(By.xpath("//span[text()='Game Prizes']//following::select[@name='entity']")).sendKeys(prizeType);
        //Find Redeem EN textfield
        browser.findElement(By.xpath("//span[text()='Game Prizes']//following::textarea[@name='redeem_msg']")).sendKeys("EN series sms");
        //Find Redeem RU textfield
        browser.findElement(By.xpath("//span[text()='Game Prizes']//following::textarea[@name='redeem_msg_ru']")).sendKeys("RU series sms");
        //Press Add New Prize button
        browser.findElement(By.xpath("//span[text()='Game Prizes']//following::button[text()='Add New Prize']")).click();
      } catch (NoSuchElementException e) {
        System.out.println("Ooops! Can't find Prizes block! Scrolling...");
        scrollDownToFooter();
      }
    } while (!lastTry);
    assertThat(browser.findElement(By.xpath("//div[contains(text(),'Match #')]//following-sibling::div[2]")).getText(), equalTo(prizeType));
    System.out.println(String.format("Game Prize %s was set to %s place.", prizeType, place));
  }

  public void saveUpdatedGame(){
    boolean lastTry = false;
    do {
      try { //when we are in the end of the list, "Create new" button shouldn't be found
        browser.findElement(By.xpath("//button[text()='Create new']"));
      } catch (NoSuchElementException e) {
        lastTry = true;
      }
      try {
        //Click on Save button
        browser.findElement(By.xpath("//button[text()='Save']")).click();
      } catch (NoSuchElementException e) {
      System.out.println("Ooops! Can't find Prizes block! Scrolling...");
      scrollDownToFooter();
    }
  } while (!lastTry);
    System.out.println(String.format("Game was saved/updated."));
  }

  //========================
  //API methods or service
  //========================

  public boolean isPrizeExistForGame(String gameId, int matchId, int place, String prizeType){
    List<Prize> prizes = admin.getPrizesForCustomGame(gameId);
    for (Prize p: prizes) {
      try {
        assertThat(p.getType(), equalTo("match"));
        assertThat(p.getGame_id(), equalTo(gameId));
        assertThat(p.getEntity(), equalTo(prizeType));
        Assert.assertEquals(p.getMatch_id(),String.valueOf(matchId-1));
        assertThat(p.getPlace(), equalTo(place));
        assertThat(p.getRedeem_msg_ru(), equalTo("RU sms"));
        return true;
      } catch (AssertionError a) {
        System.out.println(a);
        System.out.println("Prize is not found in the list.");
      }
    }
    return false;
  }

  public boolean isSeriesPrizeExistForGame(String gameId, int place, String prizeType){
    List<Prize> prizes = admin.getPrizesForCustomGame(gameId);
    for (Prize p: prizes) {
      try {
        assertThat(p.getType(), equalTo("game"));
        assertThat(p.getGame_id(), equalTo(gameId));
        assertThat(p.getEntity(), equalTo(prizeType));
        assertThat(p.getPlace(), equalTo(place));
        assertThat(p.getRedeem_msg(), equalTo("EN series sms"));
        return true;
      } catch (AssertionError a) {
        System.out.println(a);
        System.out.println("Prize is not found in the list.");
      }
    }
    return false;
  }

  public Team getTeamByName(String teamName) throws IOException {
    return admin.parseTeamfromManifest(teamName);
  }

  public String getGameId(Game game){
    return admin.getGameID(game);
  }

  public Game createGameToday(String firstTeam, String secondTeam, String startHours, String startMinutes) throws ParseException {
    Gson gson = new Gson();
    Response response;

    List<String> teams = new ArrayList<>();
    teams.add(firstTeam);
    teams.add(secondTeam);

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    String today = java.time.LocalDate.now().toString();
    Date parsedDate = dateFormat.parse(today + " " + startHours + ":" + startMinutes + ":00");
    Timestamp timestamp = new Timestamp(parsedDate.getTime());
    Game found_game = admin.isGameInActiveList(firstTeam, secondTeam, timestamp);
    if (found_game == null) {
      response = admin.createNewGame("auto-" + timestamp, timestamp, teams);
      /*if(response.getStatus()!=204)
        System.out.println(response.readEntity(String.class));*/
      return gson.fromJson(response.readEntity(String.class), Game.class);
    } else
      return found_game;
  }

  public Game createGameTodayInXMinutes(String firstTeam, String secondTeam, int shiftingMinutes) throws ParseException {
    Calendar cal = setShiftingMinutes(shiftingMinutes);
    String startHours = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
    String startMinutes = String.valueOf(cal.get(Calendar.MINUTE));
    return createGameToday(firstTeam, secondTeam, startHours, startMinutes);
  }

  public Payload setDefaultPrepick(Game game, String firstTeamName, String secondTeamName) throws InterruptedException, IOException {
    Ward prepick = admin.makePrepickWardForTeams(firstTeamName, secondTeamName);
    return admin.postPrepicksToGame(game, prepick);
  }

  protected Calendar setShiftingMinutes(int time_shifting) {
    Calendar cal = Calendar.getInstance();
    //String day_of_week = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);
    int hours = cal.get(Calendar.HOUR_OF_DAY);
    int minutes = cal.get(Calendar.MINUTE);
    //String date = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
    if (minutes + time_shifting > 60) {
      minutes = minutes + time_shifting - 60;
      hours = hours + 1;
    } else {
      minutes = minutes + time_shifting;
    }
    cal.set(Calendar.HOUR_OF_DAY, hours);
    cal.set(Calendar.MINUTE, minutes);
    return cal;
  }

  public Payload startGame(Game game, Payload current_payload) {
    //TODO Check correctness. Here wards can be null.
    Gson gson = new Gson();
    Ward current_wards = current_payload.getWards().get("prepick");

    Map<String, Ward> all_wards = admin.getDefaultWardsForGame(game);
    all_wards.put("prepick", current_wards);

    System.out.println(String.format("Current wards JSon to POST game start: %s", gson.toJson(all_wards)));
    return admin.postStartGame(game, all_wards);
  }

  public void endGameSeries(Game game) {
    if (admin.isGameInActiveList(game))
      admin.postEndGameSeries(game);
  }

  public Payload setFBWard(Game game, Payload current_payload, Team team, String player_position, String line) {
    Map<String, Ward> new_ward_map = admin.setFirstBloodWard(game, team, player_position, line);
    return admin.postWardsUpdate(game.getId(), current_payload, new_ward_map);
  }

  public Payload setTowerWardTeam(Game game, Payload current_payload, Team team) {
    Map<String, Ward> new_ward_map = admin.setTowerWardTeam(game, team);
    return admin.postWardsUpdate(game.getId(), current_payload, new_ward_map);
  }

  public Payload setTowerWardLine(Game game, Payload current_payload, Team team, String line) {
    Map<String, Ward> new_ward_map = admin.setTowerWardLine(game, line);
    return admin.postWardsUpdate(game.getId(), current_payload, new_ward_map);
  }

}
