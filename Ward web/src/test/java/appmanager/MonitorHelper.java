package appmanager;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import restclient.AdminClient;
import restclient.model.Event;
import restclient.model.Game;
import restclient.model.Team;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MonitorHelper extends BaseHelpers{
  private AdminClient admin = new AdminClient();

  public MonitorHelper(WebDriver browser) {
    super(browser);
  }

  public boolean isDraftClosedInAPI(String gameId) {
    List<Event> events = admin.getEventsOfGame(gameId);
    for (Event e: events){
      if(e.getType().equals("match_draft_closed"))
        return true;
    }
    return false;
  }

  public boolean isDraftOnBackendEqualsToThis(String gameId, Map<String, String> currentDraft) {
    String draft_payload_id = admin.getLastPayloadIdForGame(gameId);
    Map<String, String> backDraft = admin.getPrepickValuesMapFromPayload(gameId, draft_payload_id);
    try {
      for (String k : backDraft.keySet()) {
        if (!currentDraft.get(k).equals(backDraft.get(k)))
          return false;
      }
      for (String y : currentDraft.keySet()) {
        if (!backDraft.containsKey(y))
          return false;
      }
    } catch (NullPointerException np) {
      return false;
    }
    return true;
  }

  public boolean isSubPlayerInDraftOnBack(String gameId, Map<String, String> currentDraft) {
    String draft_payload_id = admin.getLastPayloadIdForGame(gameId);
    Map<String, String> backDraft = admin.getPrepickValuesMapFromPayload(gameId, draft_payload_id);
    try {
      for (String k : currentDraft.keySet()) {
        if (!currentDraft.get(k).equals(backDraft.get(k)))
          return false;
      }
    } catch (NullPointerException np) {
      return false;
    }
    return true;
  }

  public void setChampForPlayer(Map<String, String> draft_list) {
    draft_list.forEach((p, h) -> setChampForPlayer(p, h));
  }

  private void setDraftForTeamOnPage(List<String> heroes, List<String> players) {
    for(int i=0; i<players.size(); i++){
      setChampForPlayer(heroes.get(i),players.get(i));
    }
  }

  public void setChampForPlayer(String player, String hero) {
    try{
      WebElement field = browser.findElement(By.xpath(String.format("//span[text()='%s']//following::div[text()='Select...']//ancestor::div[3]", player)));
      field.click();
      browser.findElement(By.xpath(String.format("//span[text()='%s']//following::input[1]", player))).sendKeys(hero);
      Actions act = new Actions(browser);
      act.sendKeys(Keys.ENTER).perform();
    } catch (Exception e){
      System.out.println(String.format("Can't find player %s or champion %s", player, hero));
    }
  }

  public void subPlayerToPlayerOnPage(String oldPlayer, String newPlayer) throws InterruptedException {
    //click on Substitution button
    browser.findElement(By.xpath("//button[text()='Substitutions']")).click();
    Thread.sleep(1000);
    try {
      WebElement field = browser.findElement(By.xpath(String.format("//option[text()='%s']//following::select[1]", oldPlayer)));
      field.sendKeys(newPlayer);
    } catch (Exception e) {
      System.out.println(String.format("Can't find player %s", oldPlayer));
    }
  }
  
  public void confirmDraftOnPage() throws InterruptedException {
    Thread.sleep(1000);
    browser.findElement(By.xpath("//button[text()='Confirm']")).click();
    Thread.sleep(1000);
  }

  public void reConfirmDraftOnPage() throws InterruptedException {
    Thread.sleep(1000);
    browser.findElement(By.xpath("//button[text()='Reconfirm']")).click();
    Thread.sleep(1000);
  }

  public void confirmPlayerSubOnPage() throws InterruptedException {
    Thread.sleep(1000);
    browser.findElement(By.xpath("//button[text()='Confirm']")).click();
    Thread.sleep(1000);
  }

  public void setFBmainTo(String player) {
    while (true) {
      try {
        browser.findElement(By.xpath("//div[text()='First blood']"));
        browser.findElement(By.xpath(String.format("//div[text()='First blood']//ancestor::div[3]//input[@name='%s']", player)));
        break;
      } catch (NoSuchElementException e) {
        System.out.println("Can't find requested checkbox...");
        scrollDownToFooter();
      }
    }
  }

  public String getTimeOfFBwardOnPage() {
    return browser.findElement(By.className("ward-title__time")).getText();
  }

  public String getTimeOfFBinAPI(String gameID) {
    String last_payload_id = admin.getLastPayloadIdForGame(gameID);
    long fb_time = admin.getFBStartTimestamp(gameID, last_payload_id);
    long game_start_time = admin.getGameStartTimestamp(gameID, last_payload_id);
    if (fb_time != -1 && game_start_time != -1)
      return millisToTime(fb_time - game_start_time);
    return null;
  }

  public String getFBplayerValueInAPI(String gameID) {
    //TODO finish this
    return null;
  }

  private void setNexusWardOnPage(Team team) {
    while (true) {
      try {
        browser.findElement(By.xpath("//div[text()='Nexus']"));
        browser.findElement(By.xpath(String.format("//div[text()='Nexus']//ancestor::div[3]//input[@name='%s']", team.getTitle())));
        break;
      } catch (NoSuchElementException e) {
        System.out.println("Can't find an element...");
        scrollDownToFooter();
      }
    }
  }

  public void finishGameOnPage(Game game) throws InterruptedException, IOException {
    initMonitor();
    String firstTeamName = admin.parseTeamfromManifestByID(game.getTeams().get(0)).getTitle();
    String secondTeamName = admin.parseTeamfromManifestByID(game.getTeams().get(1)).getTitle();
    selectGameOnPage(game.getDate(), game.getFullTime(), firstTeamName, secondTeamName);
    Thread.sleep(2000);

    pressEndGameSeries();
  }

  private void pressConfirmAll() {
    browser.findElement(By.xpath("//button[text()='Confirm all']")).click();
  }

  private void pressEndGameSeries() throws InterruptedException {
    while (true) {
      try {
        browser.findElement(By.xpath("//button[contains(text(),'End game series')]")).click();
        Thread.sleep(2000);
        browser.switchTo().alert().accept();
        Thread.sleep(2000);
        System.out.println("end");
        break;
      } catch (WebDriverException | AssertionError e) {
        Thread.sleep(2000);
        scrollDownToFooter();
      }
    }
  }

  public void substitutePlayer(Game game, Team team, String playerRole, String newPlayerName) throws InterruptedException, IOException {
    initMonitor();
    String firstTeamName = admin.parseTeamfromManifestByID(game.getTeams().get(0)).getTitle();
    String secondTeamName = admin.parseTeamfromManifestByID(game.getTeams().get(1)).getTitle();
    selectGameOnPage(game.getDate(), game.getFullTime(), firstTeamName, secondTeamName);
    Thread.sleep(2000);
    //Press Substitution button
    browser.findElement(By.xpath("//button[text()='Substitutions']")).click();
    //Find player block and change player name on it
    String playerId = team.getPlayers().stream().filter(player -> player.getFullPosition().equals(playerRole))
            .findFirst()
            .map(player -> player.getId())
            .get();
    WebElement playerBlock = browser.findElement(By.xpath(String.format("//option[@value='%s']//parent::select[1]",playerId)));
    //playerBlock.click(); Maybe this one is not needed
    playerBlock.sendKeys(newPlayerName);
    browser.findElement(By.xpath("//button[text()='Confirm']")).click();
  }

  public void startGameOnPage(Game game) throws InterruptedException {
    //Move down for a bit to make button visible
    Actions act = new Actions(browser);
    act.sendKeys(Keys.DOWN).perform();
    Thread.sleep(1000);
    browser.findElement(By.className("game-start")).click();
  }

  public void selectGameOnPage(String date, String time, String firstTeam, String secondTeam) {
    List<WebElement> games = browser.findElements(By.xpath("//tr"));
    System.out.println(String.format("There is %d games on the page.", games.size()));

    for(WebElement g: games){
      try {
        browser.findElement(By.xpath(String.format("//td[text()='%s']", date)));
        browser.findElement(By.xpath(String.format("//td[text()='%s']//following-sibling::td[text()='%s']", date, time)));
        browser.findElement(By.xpath(String.format("//td[text()='%s']//following-sibling::td[text()='%s']//following::b[text()='%s']", date, time, firstTeam)));
        browser.findElement(By.xpath(String.format("//td[text()='%s']//following-sibling::td[text()='%s']//following::b[text()='%s']//following::b[text()='%s']", date, time, firstTeam, secondTeam)))
                .click();
        break;
      } catch (NoSuchElementException e){
        System.out.println("Game cell is wrong... Moving to the next game cell...");
      }
    }
  }

  public void selectGameOnPage(Game game) throws IOException {
    String firstTeamName = admin.parseTeamfromManifestByID(game.getTeams().get(0)).getTitle();
    String secondTeamName = admin.parseTeamfromManifestByID(game.getTeams().get(1)).getTitle();

    selectGameOnPage(game.getDate(), game.getFullTime(), firstTeamName, secondTeamName);
  }

  public boolean doesGameExistOnPage(String date, String time, String firstTeam, String secondTeam){
    boolean lastTry = false;
    System.out.println(String.format("Game to find should start on %s at %s between %s and %s", date, time, firstTeam, secondTeam));
    do {
      try { //when we are in the end of the list, "Upcoming" button shouldn't be found
        browser.findElement(By.xpath("//button[text()='Upcoming']"));
      } catch (NoSuchElementException e){
        lastTry = true;
      }
      try {
        browser.findElement(By.xpath(String.format("//td[text()='%s']", date)));
        browser.findElement(By.xpath(String.format("//td[text()='%s']//following-sibling::td[text()='%s']", date, time)));
        browser.findElement(By.xpath(String.format("//td[text()='%s']//following-sibling::td[text()='%s']//following::b[text()='%s']", date, time, firstTeam)));
        browser.findElement(By.xpath(String.format("//td[text()='%s']//following-sibling::td[text()='%s']//following::b[text()='%s']//following::b[text()='%s']", date, time, firstTeam, secondTeam)));
        return true;
      } catch (NoSuchElementException e){
        System.out.println("Can't find the game... Scrolling...");
        scrollDownToFooter();
      }
    } while (!lastTry);
    System.out.println("Game is not found in the list.");
    return false;
  }

  public boolean doesGameExistOnPage(Game game) throws IOException {
    String firstTeamName = admin.parseTeamfromManifestByID(game.getTeams().get(0)).getTitle();
    String secondTeamName = admin.parseTeamfromManifestByID(game.getTeams().get(1)).getTitle();

    return doesGameExistOnPage(game.getDate(),game.getFullTime(),firstTeamName, secondTeamName);
  }

  public void closeDraftForUsers() throws InterruptedException {
    Thread.sleep(2000);
    browser.findElement(By.xpath("//button[text()='Close draft for users']"))
            .click();
  }

  public Map<String, String> convertDraftListToBackendFormat(Map<String, String> oldMap) throws IOException {
    Map<String, String> newMap = new HashMap<>();
    oldMap.keySet().forEach(key -> {
      try {
        newMap.put(admin.getHeroIdByTitle(key), admin.getPlayerIdByTitle(oldMap.get(key)));
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
    return newMap;
  }

}
