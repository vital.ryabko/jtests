package appmanager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.util.concurrent.TimeUnit;

public class AppManager {

  private AdminHelper webadmin;
  private MonitorHelper monitor;
  private FormHelper form;

  public WebDriver browser;

  public void init() {
      ChromeOptions options = new ChromeOptions();
      options.addArguments("disable-infobars");
      options.addArguments("--start-maximized");
      options.addArguments("--disable-extensions");
      //options.addArguments("headless");
      //options.addArguments("window-size=1200x600");
      browser = new ChromeDriver(options);
      browser.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
      browser.get("https://admin-staging.wardapp.xyz/auth");

    form = new FormHelper(browser);
    monitor = new MonitorHelper(browser);
    webadmin = new AdminHelper(browser);
  }

  /*
    private void login() {
      WebElement auth_field = browser.findElement(By.xpath("//input[@name='token']"));
      auth_field.click();
      auth_field.sendKeys("secret");
      browser.findElement(By.xpath("//button[@type='submit']")).click();
    }
  */
  public void stop() {
    browser.quit();
  }

  public FormHelper form() {
    return form;
  }

  public MonitorHelper monitor() {
    return monitor;
  }

  public AdminHelper webadmin() {
    return webadmin;
  }

}
