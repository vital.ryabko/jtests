package appmanager;

import org.openqa.selenium.*;

import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class BaseHelpers {
  protected WebDriver browser;

  public BaseHelpers(WebDriver browser) {
    this.browser = browser;
  }

  public void initMonitor() {
    try {
      browser.findElement(By.className("game-start"));
    } catch (org.openqa.selenium.NoSuchElementException ex){
      browser.get("https://monitor-staging.wardapp.xyz/");
    }
    try{
      login();
    } catch (NoSuchElementException ex){
      System.out.println("Login is not needed.");
    }
  }

  protected void initAdmin() {
    try {
      browser.findElement(By.xpath("//button[text()='Create new']"));
    } catch (org.openqa.selenium.NoSuchElementException ex){
      browser.get("https://admin-staging.wardapp.xyz/");
    }
    try{
      login();
    } catch (NoSuchElementException ex){
      System.out.println("Login is not needed.");
    }
  }

  protected void login() {
    WebElement auth_field = browser.findElement(By.xpath("//input[@name='token']"));
    auth_field.click();
    auth_field.sendKeys("secret");
    browser.findElement(By.xpath("//button[@type='submit']")).click();
  }

  protected void scrollDownToFooter() {
    JavascriptExecutor js = (JavascriptExecutor)browser;
    js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
  }

  protected String toUpperCase(String string) {
    char[] str = string.toCharArray();
    for (int i=0;i<str.length;i++){
      str[i] = Character.toUpperCase(str[i]);
    }
    return String.valueOf(str);
  }
  protected String currentLocalDateUpper() {
    Calendar cal = Calendar.getInstance();
    String day_of_week = toUpperCase(cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US));
    String month = toUpperCase(cal.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US));
    String date = toUpperCase(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
    return day_of_week+", "+month+" "+date;
  }
  protected String currentLocalDate() {
    Calendar cal = Calendar.getInstance();
    String day_of_week = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
    String month = cal.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US);
    String date = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
    return day_of_week + ", " + month + " " + date;
  }

  protected String cleaned(String text){
    return text.replaceAll("\\n"," ");
  }

  public String millisToTime(long millis) { //format "00:03:20"
    return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
            TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
            TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
  }
 /* protected String timeForGame(String time) {
    Calendar cal = Calendar.getInstance();
    //String day_of_week = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);
    String month = String.valueOf(cal.get(Calendar.MONTH));
    String year = String.valueOf(cal.get(Calendar.YEAR));
    String date = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));

    return  date+month+year+time;
  }*/

  }
