package tests;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.junit.Test;
import org.openqa.selenium.net.UrlChecker;
import org.openqa.selenium.remote.DesiredCapabilities;
import restclient.AdminClient;

import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class SimpleTest {
  private String s = null;

  @Test
  public void test1(){
    try {
      assertThat(s,is(notNullValue()));
    } catch (AssertionError e){
      s = " ";
      assertThat(s,is(notNullValue()));
      System.out.println("String is not null!");
    }
  }

  @Test
  public void createGameTest() throws ParseException {
    AdminClient admin = new AdminClient();

    List<String> teams = new ArrayList<>();
    teams.add("Counter Logic Gaming");
    teams.add("Team Liquid");

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    Date parsedDate = dateFormat.parse("2018-10-23 21:53:00");
    Timestamp timestamp = new Timestamp(parsedDate.getTime());

    Response response = admin.createNewGame("auto-a1a1a1", timestamp, teams);
    System.out.println(response.getStatus());
  }

}
