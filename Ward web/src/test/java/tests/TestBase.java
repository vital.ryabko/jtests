package tests;

import appmanager.AppManager;
import org.junit.After;
import org.junit.Before;

public class TestBase {

  //public WebDriver browser;
  static AppManager app;

  @Before
  public void startDriver() throws Exception {
    app.init();

/*    ChromeOptions options = new ChromeOptions();
    options.addArguments("disable-infobars");
    options.addArguments("--start-maximized");
    options.addArguments("--disable-extensions");
    //options.addPreference("security.sandbox.content.level",5);
    browser = new ChromeDriver(options);
    browser.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    browser.get("https://admin-staging.wardapp.xyz/");*/
  }

  @After
  public void stopDriver() {
    app.stop();
   // browser.quit();
  }

}
