package tests.sanityTests;

import appmanager.AppManager;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import restclient.model.Game;
import restclient.model.Payload;

public class SanityTestBase {
  public static String FIRST_TEAM_NAME;
  public static String SECOND_TEAM_NAME;
  public static Game CURRENT_GAME;
  public static String CURRENT_GAME_ID;
  public static Payload CURRENT_PAYLOAD;
  public enum PRIZE_TYPE {
    headphones1, marshal_speaker, rp_card_10, tripleprize
  };
  static AppManager app;

  @BeforeClass
  public static void startDriver() {
    app = new AppManager();
    app.init();

    /*FIRST_TEAM_NAME = "Gambit Esports";
    SECOND_TEAM_NAME = "Splyce";
    CURRENT_GAME = app.webadmin().createGameTodayInXMinutes(FIRST_TEAM_NAME, SECOND_TEAM_NAME, 5);*/
  }

  @AfterClass
  public static void stopDriver() {
    //app.webadmin().endGameSeries(CURRENT_GAME);
    app.stop();
  }

}
