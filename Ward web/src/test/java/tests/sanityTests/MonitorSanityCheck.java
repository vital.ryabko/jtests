package tests.sanityTests;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import restclient.model.Team;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MonitorSanityCheck extends SanityTestBase  {

  @Test
  @DisplayName("Service method")
  public void test_1() throws IOException, ParseException {
    //Define parameters of the game
    FIRST_TEAM_NAME = "Echo Fox";
    SECOND_TEAM_NAME = "Elite Wolves";
    Team FIRST_TEAM = app.webadmin().getTeamByName(FIRST_TEAM_NAME);
    Team SECOND_TEAM = app.webadmin().getTeamByName(SECOND_TEAM_NAME);

    //Open Admin and create the game without prizes
    CURRENT_GAME = app.webadmin().createGame("lol", FIRST_TEAM, SECOND_TEAM, 10);
    //Get GameID
    CURRENT_GAME_ID = app.webadmin().getGameId(CURRENT_GAME);
    System.out.println(CURRENT_GAME_ID);
    //Check that the game with selected parameters is created in the list of games
    assertThat(app.webadmin().doesGameIdExistOnAdminPage(CURRENT_GAME_ID), is(true));
  }

  @Test
  @DisplayName("Find the game on monitor page.")
  public void test_4() throws IOException {
    app.monitor().initMonitor();
    //Check that game is on the page
    assertThat(app.monitor().doesGameExistOnPage(CURRENT_GAME), is(true));
    //If so, open the game on the page
    app.monitor().selectGameOnPage(CURRENT_GAME);
  }

  @Ignore
  @Test
  @DisplayName("Close draft for users on monitor page.")
  public void test_5() throws InterruptedException {
    app.monitor().closeDraftForUsers();
    //Check if draft is closed in API
    CURRENT_GAME_ID = app.webadmin().getGameId(CURRENT_GAME);
    assertThat(app.monitor().isDraftClosedInAPI(CURRENT_GAME_ID), is(true));
  }

  @Test
  @DisplayName("Set draft champs for players on monitor page.")
  public void test_6() throws InterruptedException, IOException {
    //Initialize teams-champs list
    Map<String, String> draft_list = new HashMap<>();
    draft_list.put("Huni", "Jax");
    draft_list.put("Dardoch", "Sona");
    draft_list.put("Fenix", "Tristana");
    draft_list.put("Altec", "Varus");
    draft_list.put("Adrian", "Fiora");
    draft_list.put("Force", "Singed");
    draft_list.put("Meliodas", "Tahm Kench");
    draft_list.put("rhOm", "LeBlanc");
    draft_list.put("Iruga", "Thresh");
    draft_list.put("Khael", "Karma");
    app.monitor().setChampForPlayer(draft_list);
    //Confirm draft on page
    app.monitor().confirmDraftOnPage();
    //Make map useful to compare
    Map<String, String> converted_draft = app.monitor().convertDraftListToBackendFormat(draft_list);
    //Verify draft on backend side
    assertThat(app.monitor().isDraftOnBackendEqualsToThis(CURRENT_GAME_ID, converted_draft), is(true));
  }

  @Test
  @DisplayName("Substitute draft players on monitor page.")
  public void test_7() throws InterruptedException, IOException {
    //Make substitution
    app.monitor().subPlayerToPlayerOnPage("Huni", "Lost");
    //Confirm substitution
    app.monitor().confirmPlayerSubOnPage();
    //Initialize teams-champs list
    Map<String, String> draft_sub_list = new HashMap<>();
    draft_sub_list.put("Lost", "Jax");
    //Set hero for new player
    app.monitor().setChampForPlayer(draft_sub_list);
    //Re-confirm draft
    app.monitor().reConfirmDraftOnPage();
    //Make map useful to compare
    Map<String, String> converted_draft_sub_list = app.monitor().convertDraftListToBackendFormat(draft_sub_list);
    //Verify that draft on backend side contains changes
    assertThat(app.monitor().isSubPlayerInDraftOnBack(CURRENT_GAME_ID, converted_draft_sub_list), is(true));
    //TODO Also we can check "substitutions" key on backend as an additional assertion
  }

  @Test
  @DisplayName("Start game.")
  public void test_8() throws InterruptedException {
    //Click on start game icon
    app.monitor().startGameOnPage(CURRENT_GAME);
    //Assert that wards become selective
    try {
      app.browser.findElement(By.className("game-ward-disabled"));
      assertThat("Wards are blocked", equalTo("Wards are enabled"));
    } catch (NoSuchElementException e) {
      System.out.println("Wards are enabled");
    }
  }

  @Test
  @DisplayName("Set First Blood player ward on monitor page.")
  public void test_9() {
    //Set checkbox of FB player ward
    app.monitor().setFBmainTo("Fenix");
    //Read FB time on the page
    String fb_time = app.monitor().getTimeOfFBwardOnPage();
    //Read FB time in API
    String fb_time_api = app.monitor().getTimeOfFBinAPI(CURRENT_GAME_ID);
    //Compare these times
    assertThat(fb_time, equalTo(fb_time_api));
    //Assert FB player value in API
    assertThat(app.monitor().getFBplayerValueInAPI(CURRENT_GAME_ID), equalTo("Fenix"));
  }
}
