package tests.sanityTests;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import restclient.model.Team;

import java.io.IOException;
import java.text.ParseException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class AdminSanityCheck extends SanityTestBase {

  @Test
  @DisplayName("Create new basic game with one match")
  public void test_1() throws IOException, ParseException {
    //Define parameters of the game
    FIRST_TEAM_NAME = "Echo Fox";
    SECOND_TEAM_NAME = "Elite Wolves";
    Team FIRST_TEAM = app.webadmin().getTeamByName(FIRST_TEAM_NAME);
    Team SECOND_TEAM = app.webadmin().getTeamByName(SECOND_TEAM_NAME);

    //Open Admin and create the game without prizes
    CURRENT_GAME = app.webadmin().createGame("lol", FIRST_TEAM, SECOND_TEAM, 10);
    //Get GameID
    CURRENT_GAME_ID = app.webadmin().getGameId(CURRENT_GAME);
    //Check that the game with selected parameters is created in the list of games
    assertThat(app.webadmin().doesGameIdExistOnAdminPage(CURRENT_GAME_ID), is(true));
  }

  @Test
  @DisplayName("Set prize for match of basic game.")
  public void test_2() throws InterruptedException {
    //Go to the game with current ID
    app.webadmin().goToGameWithIdOnAdminPage(CURRENT_GAME_ID);

    //Set Prize for Match
    int MATCH_ID = 1;
    int PLACE = 1;
    app.webadmin().setMatchPrizeOnPage(MATCH_ID,PLACE,PRIZE_TYPE.tripleprize.name());
    app.webadmin().saveUpdatedGame();
    Thread.sleep(2000);
    //Verify Match Prize on backend
    assertThat(app.webadmin().isPrizeExistForGame(CURRENT_GAME_ID, MATCH_ID, PLACE, PRIZE_TYPE.tripleprize.name()), is(true));
  }

  @Test
  @DisplayName("Set prize for game series.")
  public void test_3() throws InterruptedException {
    int PLACE = 1;
    //Go to the game with current ID
    app.webadmin().goToGameWithIdOnAdminPage(CURRENT_GAME_ID);
    //Set Prize for Game Series
    app.webadmin().setSeriesPrizeOnPage(PLACE,PRIZE_TYPE.headphones1.name());
    app.webadmin().saveUpdatedGame();
    Thread.sleep(2000);
    //Verify Match Prize on backend
    assertThat(app.webadmin().isSeriesPrizeExistForGame(CURRENT_GAME_ID, PLACE, PRIZE_TYPE.headphones1.name()), is(true));
  }

  }
