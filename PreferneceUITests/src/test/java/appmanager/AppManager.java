package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AppManager {

  private OnboardingHelper onboardingHelper;
  private ContentHelper contentHelper;
  public AndroidDriver driver;
  private InputHelper inputHelper;
  private NavigationHelper navHelper;

  public void init() throws MalformedURLException {
    DesiredCapabilities capabilities = new DesiredCapabilities();
    capabilities.setCapability("platformName", "Android");
    capabilities.setCapability("deviceName", "041604a1e6563902");
    capabilities.setCapability("platformVersion", "7.0");
    capabilities.setCapability("app", "C:\\Testing\\apk\\AureumPreference-dev-release-0.5.4.apk");
    driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    navHelper = new NavigationHelper(driver);
    inputHelper = new InputHelper(driver);
    contentHelper = new ContentHelper(driver);
    onboardingHelper = new OnboardingHelper(driver);
  }


  public void stop() {
    driver.quit();
  }

  public NavigationHelper getNavHelper() {
    return navHelper;
  }

  public InputHelper getInputHelper() {
    return inputHelper;
  }

  public ContentHelper getContentHelper() {
    return contentHelper;
  }

  public OnboardingHelper getOnboardingHelper() {
    return onboardingHelper;
  }
}
