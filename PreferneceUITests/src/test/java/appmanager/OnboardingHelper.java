package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

public class OnboardingHelper {

  private AppiumDriver driver;
  private InputHelper input;
  private ContentHelper content;
  private NavigationHelper nav;

  public OnboardingHelper(AndroidDriver driver) {
    this.driver = driver;
    this.input = new InputHelper(driver);
    this.content = new ContentHelper(driver);
    this.nav = new NavigationHelper(driver);
  }

  public void onboardingTillPhoto(String number, String code, String second_code, String username, String name_type) throws InterruptedException {
    nav.goToPhoneScreen();
    enterPhoneAndContinue(number);
    Thread.sleep(2000);
    enterCode(code, "Code 1");
    Thread.sleep(2000);
    enterCode(second_code, "Code 2");

    if (code != second_code){
      content.checkText(By.id("de.aureum.preference.dev:id/errorLabel"),"Passwords mismatch");
    } else {
      goToNameAndPhoto(username, name_type);
      goToEnableCameraEnable();
    }
  }

  private void enterCode(String code, String screen_name) {
    content.verifyScreen(screen_name);
    input.codeInput(code);
  }

  public void enterPhoneAndContinue(String number) {
    //Phone screen
    content.verifyScreen("Phone input");
    input.enterPhone(By.id("de.aureum.preference.dev:id/phone"), number);
    nav.verifyAndPushTheButton("de.aureum.preference.dev:id/confirmBtn", "Continue");
  }

  public void goToNameAndPhoto(String username, String type) throws InterruptedException {
    //Name screen
    content.verifyScreen("Onboarding Name");
    input.changeNameInput(username);
    if (type == "long name")
      content.checkText(By.id("dummy"),"dummy");
     else if (type != "correct")
       content.checkText(By.id("dummy"),"dummy");

  }

  public void goToEnableCameraEnable() {
    //Enable Camera screen
    content.verifyScreen("Enable camera");
    nav.verifyAndPushTheButton("de.aureum.preference.dev:id/continueBtn", "Enable Camera");
    nav.acceptCameraAlert();
    //Photo screen
    content.verifyScreen("Photo screen");
  }

  public void goToEnableCameraDiable() {
    //Enable Camera screen
    content.verifyScreen("Enable camera");
    nav.verifyAndPushTheButton("de.aureum.preference.dev:id/continueBtn", "Enable Camera");
    nav.denyCameraAlert();
    //Enable Camera screen
    content.verifyScreen("Enable camera");
  }
}
