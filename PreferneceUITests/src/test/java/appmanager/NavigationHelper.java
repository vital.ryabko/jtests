package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class NavigationHelper {
  private AppiumDriver driver;
  private InputHelper input;
  private ContentHelper content;

  public NavigationHelper(AndroidDriver driver) {
    this.driver = driver;
    this.input = new InputHelper(driver);
    this.content = new ContentHelper(driver);
  }

  public void verifyAndPushTheButton(String id, String text) {
    WebElement button = driver.findElement(By.id(id));
    Assert.assertEquals(button.getText(), text);
    button.click();
  }

  public void pushBackArrow(){
    WebElement arrow = driver.findElement(By.id("de.aureum.preference.dev:id/backBtn"));
    arrow.click();
  }

  public void pushBackArrowOnboarding(){
    WebElement arrow = driver.findElement(By.id("de.aureum.preference.dev:id/back"));
    arrow.click();
  }

  public void tapArea(By id) {
    WebElement button = driver.findElement(id);
    button.click();
  }

  public void login(String number, String code) {
    goToPhoneScreen();
    proceedPhone(number);
    input.codeInput(code);
  }

  public void selectLanguage(final String lang) {
    tapArea(By.id("de.aureum.preference.dev:id/row_language"));
    tapArea(By.xpath("//android.widget.TextView[@text='" + lang + "']"));
    content.checkText(By.id("de.aureum.preference.dev:id/title_language"), lang);
  }

  public void selectComplement(final String complement) {
    tapArea(By.id("de.aureum.preference.dev:id/card_complement"));
    tapArea(By.xpath("//android.widget.TextView[@text='" + complement + "']"));
    content.checkText(By.id("de.aureum.preference.dev:id/title_complement"), complement);
  }

  public void selectDrink(final String drink) {
    tapArea(By.id("de.aureum.preference.dev:id/card_drink"));
    tapArea(By.xpath("//android.widget.TextView[@text='" + drink + "']"));
    content.checkText(By.id("de.aureum.preference.dev:id/title_drink"), drink);
  }

  public void changeNameMain(String old_name, String new_name){
    goToChangeNameScreen();
    content.checkOldName(old_name);
    input.changeNameInput(new_name);
    verifyAndPushTheButton("de.aureum.preference.dev:id/confirmBtn", "Continue");
  }

  public void proceedPhone(String number){
    input.enterPhone(By.id("de.aureum.preference.dev:id/phone"), number);
    verifyAndPushTheButton("de.aureum.preference.dev:id/confirmBtn", "Continue");
  }

  public void returnToDefault(final String drink, final String dessert, final String language, String new_name) {
    selectDrink(drink);
    selectComplement(dessert);
    selectLanguage(language);
    changeNameMain("Cucumber", new_name);
  }

  public void goToPhoneScreen(){
    verifyAndPushTheButton("de.aureum.preference.dev:id/continueBtn", "Login or Register");
  }

  public void goToChangeNameScreen(){
    tapArea(By.id("de.aureum.preference.dev:id/row_name"));
  }

  public void goToManageYourFace() {
    tapArea(By.id("de.aureum.preference.dev:id/row_photos"));
  }

  public void goToAddPhoto() {
    tapArea(By.id("de.aureum.preference.dev:id/action_add"));
  }

  public void enableCamera() {
    tapArea(By.id("de.aureum.preference.dev:id/continueBtn"));
  }

  public void acceptCameraAlert(){
    MobileElement accept_btn = (MobileElement) driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button"));
    accept_btn.click();
  }

  public void denyCameraAlert(){
    MobileElement deny_btn = (MobileElement) driver.findElement(By.id("com.android.packageinstaller:id/permission_deny_button"));
    deny_btn.click();
  }

  public void swipeUp() {
    MobileElement element1 = (MobileElement) driver.findElement(By.id("de.aureum.preference.dev:id/row_name"));
    MobileElement element2 = (MobileElement) driver.findElement(By.id("de.aureum.preference.dev:id/card_drink"));
    new TouchAction(driver).press(element1).moveTo(element2).release().perform();
  }

  public void logout(String decision) {
    verifyAndPushTheButton("de.aureum.preference.dev:id/logoutBtn","Logout");
    if (decision == "Logout") driver.findElement(By.id("android:id/button1")).click();
    else driver.findElement(By.id("android:id/button2")).click();

  }
}
