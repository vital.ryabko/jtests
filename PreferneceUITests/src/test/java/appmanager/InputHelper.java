package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class InputHelper {
  private AppiumDriver driver;
  //private AndroidDriver droid;

  public void codeInput(String digit) {
    WebElement phone_number = driver.findElement(By.id("de.aureum.preference.dev:id/number_4"));
    phone_number.sendKeys(digit);
  }

  public void enterPhone(By locator, String number) {
    MobileElement phone = (MobileElement) driver.findElement(locator);
    phone.clear();
    phone.sendKeys(number);
  }

  public void changeNameInput(String new_name){
    MobileElement name = (MobileElement) driver.findElement(By.id("de.aureum.preference.dev:id/name"));
    name.clear();
    name.sendKeys(new_name);
  }


  public InputHelper(AppiumDriver driver) {
    this.driver = driver;
  }
}
