package appmanager;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.io.IOException;

public class ContentHelper {
  private AndroidDriver driver;

  public ContentHelper(AndroidDriver driver) {
    this.driver = driver;
  }

  public void checkText(By locator, String text) {
    WebElement title = driver.findElement(locator);
    String titleText = title.getText();
    Assert.assertEquals(titleText, text);
  }

  public void checkUsernameMain(String name) {
    String title = "Hi, "+name+"! Here are your preferences:";
    checkText(By.id("de.aureum.preference.dev:id/greeting"),title);
  }

  public void checkDrinkMain(String drink) {
    checkText(By.id("de.aureum.preference.dev:id/title_drink"),drink);
  }
  public void checkComplementMain(String complement) {
    checkText(By.id("de.aureum.preference.dev:id/title_complement"),complement);
  }
  public void checkLanguageMain(String language) {
    checkText(By.id("de.aureum.preference.dev:id/title_language"),language);
  }

  public void checkOldName(String old_name){
    MobileElement name = (MobileElement) driver.findElement(By.id("de.aureum.preference.dev:id/name"));
    Assert.assertEquals(name.getText(), old_name);
  }

  public void makeScreenshot(String pathname) throws IOException, InterruptedException {
    Thread.sleep(2000);
    File srcFile = driver.getScreenshotAs(OutputType.FILE);
    FileUtils.copyFile(srcFile, new File(pathname));
  }

  public void keyboardIsOpen(){
    Assert.assertEquals(driver.isKeyboardShown(),true);
  }
  public void keyboardIsClosed(){
    Assert.assertEquals(driver.isKeyboardShown(),false);
  }

  public void verifyScreen(String screen_name) {
    int a = 0; // validation parameter
    if (screen_name == "Onboarding Name") {
      checkText(By.id("de.aureum.preference.dev:id/description"),"Please enter your name");
      a = 1;
    }
     else if (screen_name == "Phone input") {
      checkText(By.className("android.widget.TextView"), "Use your phone number to login or create a new account");
      a = 1;
    }
      else if (screen_name == "Code 1") {
      checkText(By.id("de.aureum.preference.dev:id/title"), "Setup your password");
      a = 1;
    }
      else if (screen_name == "Code 2") {
      checkText(By.id("de.aureum.preference.dev:id/title"), "Confirm your password");
      a = 1;
    }
    else if (screen_name == "Code login") {
      checkText(By.id("de.aureum.preference.dev:id/title"), "Enter your password");
      a = 1;
    }
    else if (screen_name == "Enable camera") {
      checkText(By.id("de.aureum.preference.dev:id/description"), "First we need to know how you look, so we can recognise you anywhere and anytime you want");
      a = 1;
    }
    else if (screen_name == "Photo screen") {
      checkText(By.className("android.widget.TextView"), "First we need to know how you look, so we can recognise you anywhere and anytime you want");
      a = 1;
    }
    Assert.assertEquals(a,1);
  }
}
