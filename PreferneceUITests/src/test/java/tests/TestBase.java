package tests;

import appmanager.AppManager;
import org.junit.After;
import org.junit.Before;

import java.net.MalformedURLException;

public class TestBase {

  protected final AppManager app = new AppManager();

  @Before
  public void startDriver() throws MalformedURLException {
    app.init();
  }
  @After
  public void stopDriver() {
    app.stop();
  }
}
