package tests;

import org.junit.Test;
import org.openqa.selenium.By;

import java.io.IOException;

public class WalkthroughScreen extends TestBase {

  @Test
  public void testRegisterSuccessNoPhoto () throws IOException, InterruptedException {
    app.getOnboardingHelper().onboardingTillPhoto("+490011","1111","1111","Mary-Claire D'artaniyan Arnold", "correct");
    //app.getContentHelper().makeScreenshot("Photo_scr.png");
    app.getNavHelper().pushBackArrow();
    app.getContentHelper().verifyScreen("Onboarding Name");
  }

  @Test
  public void testRegisterFailCode () throws IOException, InterruptedException {
    app.getOnboardingHelper().onboardingTillPhoto("+490013","1111","2222","Mary-Claire D'artaniyan Arnold", "correct");
  }

  @Test
  public void testRegisterFailName() throws InterruptedException {
    app.getOnboardingHelper().onboardingTillPhoto("+490012","1111","1111","Mary-Claire D'artaniyan Arnold Shwarcenegger", "long name");
    app.getOnboardingHelper().goToNameAndPhoto(" ", "incorrect symbols");
    app.getOnboardingHelper().goToNameAndPhoto("`-=][';/.,<>?\\\"|}{+_)(*&^%$#@!~", "incorrect symbols");
    app.getOnboardingHelper().goToNameAndPhoto("User1", "numbers");
  }


  /**Manual Login API
   @Test(groups = {"Sanity"})
   public void ManualLogin() {
   Keywords object = new Keywords(GlobalProperties.POST_URL + ver, new Login_InputGson(ver).LoginRequest());
   String LoginJSONOutput = object.apiMethod_POST();

   Login_Response login_Response = new Login_Response();
   login_Response.response(Keywords.GSON(LoginJSONOutput, Login.class));
   }*/
}
