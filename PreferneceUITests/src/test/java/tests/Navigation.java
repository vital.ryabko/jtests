package tests;

import org.junit.Test;

public class Navigation extends TestBase {

  @Test
  public void testKeyboardOnboarding() throws InterruptedException {
    app.getContentHelper().keyboardIsClosed();
    app.getNavHelper().goToPhoneScreen();
    app.getContentHelper().keyboardIsOpen();
    app.getOnboardingHelper().enterPhoneAndContinue("+490021");
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsOpen();
    app.getInputHelper().codeInput("1111");
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsOpen();
    app.getInputHelper().codeInput("1111");
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsOpen();
    app.getInputHelper().changeNameInput("Navigation");
    app.getNavHelper().verifyAndPushTheButton("de.aureum.preference.dev:id/confirmBtn", "Continue");

    Thread.sleep(2000);
    app.getContentHelper().keyboardIsClosed();
    //Returning back
    app.getNavHelper().enableCamera();
    app.getNavHelper().acceptCameraAlert();
    Thread.sleep(2000);
    app.getNavHelper().pushBackArrow();
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsOpen();
    app.getNavHelper().pushBackArrow();
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsOpen();
    app.getNavHelper().pushBackArrow();
    Thread.sleep(1000);
    app.getContentHelper().keyboardIsClosed();
  }

  @Test
  public void testKeyboardOnboardingPhoneCodeBack() throws InterruptedException {
    app.getNavHelper().goToPhoneScreen();
    app.getOnboardingHelper().enterPhoneAndContinue("+490022");
    app.getInputHelper().codeInput("1111");
    Thread.sleep(2000);

    app.getNavHelper().pushBackArrowOnboarding();
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsOpen();

    app.getNavHelper().pushBackArrowOnboarding();
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsOpen();
  }

  @Test
  public void testKeyboardOnboardingPhoneCodeMismatch() throws InterruptedException {
    app.getNavHelper().goToPhoneScreen();
    app.getOnboardingHelper().enterPhoneAndContinue("+490023");
    app.getInputHelper().codeInput("1111");
    Thread.sleep(2000);
    app.getInputHelper().codeInput("2222");
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsOpen();
  }

  @Test
  public void testKeyboardLogin() throws InterruptedException {
    app.getNavHelper().goToPhoneScreen();
    app.getOnboardingHelper().enterPhoneAndContinue("+490049");
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsOpen();
    app.getInputHelper().codeInput("1234");
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsOpen();
    app.getInputHelper().codeInput("1111");
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsClosed();
  }

  @Test
  public void testKeyboardMainName() throws InterruptedException {
    app.getNavHelper().login("+490049", "1111");
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsClosed();
    app.getNavHelper().goToChangeNameScreen();
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsOpen();
    app.getNavHelper().pushBackArrow();
    Thread.sleep(2000);
    app.getContentHelper().keyboardIsClosed();

  }
  }
