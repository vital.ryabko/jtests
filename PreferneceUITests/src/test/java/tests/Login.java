package tests;

import org.junit.Test;

import java.io.IOException;

public class Login extends TestBase {

  @Test
  public void testLogin () throws IOException, InterruptedException {
    app.getNavHelper().login("+490049", "1111");
    //app.getNavHelper().swipeUp();
    Thread.sleep(2000);
    app.getContentHelper().makeScreenshot("Main.png");
  }

  @Test
  public void testSelectors () {
    app.getNavHelper().login("+490049", "1111");
    app.getNavHelper().selectDrink("Coca-Cola");
    app.getNavHelper().selectComplement("Dessert");
    app.getNavHelper().selectLanguage("Русский");
    app.getNavHelper().changeNameMain("Auto", "Cucumber");
    app.getContentHelper().checkUsernameMain("Cucumber");
    app.getNavHelper().returnToDefault("Tea", "Fruit", "English", "Auto");
  }

  @Test
  public void testLogout() throws InterruptedException {
    app.getNavHelper().login("+490049", "1111");
    Thread.sleep(2000);
    app.getNavHelper().swipeUp();
    app.getNavHelper().logout("Cancel");
    app.getNavHelper().logout("Logout");
    app.getContentHelper().verifyScreen("Phone input");
    //de.aureum.preference.dev:id/removeEnrollmentBtn
  }

  //Add tests to check remove enrollment
 /** @Test
  public void testDefaultState () throws IOException, InterruptedException {
    app.getNavHelper().login("+490049", "1111");
    app.getContentHelper().checkUsernameMain("Auto");
    app.getContentHelper().checkDrinkMain("Tea");
    app.getContentHelper().checkComplementMain("Fruit");
    app.getContentHelper().checkLanguageMain("English");
    app.getContentHelper().makeScreenshot("Main_default.png");
  }*/
}
