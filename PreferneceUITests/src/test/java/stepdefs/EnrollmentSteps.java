package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class EnrollmentSteps {

  @Given("^I am on Welcome screen$")
  public void iAmOnWelcomeScreen() throws Throwable {
    System.out.println("I'm on welcome screen");
    //throw new PendingException();
  }

  @Then("^I should see “Login or Register” button$")
  public void iShouldSeeLoginOrRegisterButton() throws Throwable {
    System.out.println("I see login");
    //throw new PendingException();
  }

  @And("^I take a screenshot$")
  public void iTakeAScreenshot() throws Throwable {
    System.out.println("I take screenshot");
    //throw new PendingException();
  }
}
