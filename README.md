## Running Ward tests from CLI

1. Open Terminal
2. Go to Ward project directory
3. Edit `build.gradle` file. Set properties ("executablePath","device","sdk") according to your local ones.
- executablePath - the full path to the application file
- device - the name of the device that can be taken from `adb devices` command or from Xcode on macOS.
- sdk - the version of mobile OS
4. Run `gradle test --tests tests.sanityTests.LoginSanityCheck -Dplatform=android` where "tests.sanityTests.LoginSanityCheck"- is the full name of Javaclass with tests.
5. If you need to run tests on iOS, just change "platform" property: `gradle test --tests tests.sanityTests.LoginSanityCheck -Dplatform=ios`