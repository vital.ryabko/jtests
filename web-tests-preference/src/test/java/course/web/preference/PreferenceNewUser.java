package course.web.preference;

import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class PreferenceNewUser {
  private WebDriver driver;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    ChromeOptions options = new ChromeOptions();
    //options.addArguments("start-maximized");
    options.addArguments("disable-infobars");
    options.addArguments("--start-maximized");
    options.addArguments("--disable-extensions");
    driver = new ChromeDriver(options);
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    login("https://preference-crm-dev.aureum.xyz/");
  }
/**
  @Test
  public void testEditUser() throws Exception {
    driver.get("https://preference-crm-dev.aureum.xyz/");
    driver.findElement(By.xpath("(//button[@type='button'])[8]")).click();
    driver.findElement(By.id("username")).click();
    driver.findElement(By.id("username")).clear();
    driver.findElement(By.id("username")).sendKeys("Autotest");
    driver.findElement(By.xpath("//form/div[2]/div[2]/div/div/div/div[@class='ant-select-selection__rendered']")).click();
    driver.findElement(By.xpath("//div[3]/div/div/div/ul/li[@class='ant-select-dropdown-menu-item']")).click();
    //driver.findElement(By.xpath("//div[3]/div/div/div/ul/li[@class='ant-select-dropdown-menu-item ant-select-dropdown-menu-item-selected']")).click();
    driver.findElement(By.xpath("//button[@type='submit']")).click();
  }
*/
  @Test
  public void testNewUser() throws Exception {
    driver.get("https://preference-crm-dev.aureum.xyz/");
    driver.manage().window().maximize();
    Thread.sleep(1000);
    driver.findElement(By.xpath("//button[@type='button']")).click();
    Thread.sleep(1000);
    driver.findElement(By.id("phoneNumber")).click();
    driver.findElement(By.id("phoneNumber")).clear();
    driver.findElement(By.id("phoneNumber")).sendKeys("+490040");
    driver.findElement(By.id("username")).click();
    driver.findElement(By.id("username")).clear();
    driver.findElement(By.id("username")).sendKeys("Autotest");
    driver.findElement(By.xpath("//div[3]/div[2]/div/div/div/div[@class='ant-select-selection__rendered']")).click();
    Thread.sleep(1000);
    driver.findElement(By.xpath("//div[3]/div/div/div/ul/li[@class='ant-select-dropdown-menu-item']")).click();
    Thread.sleep(1000);
    //((JavascriptExecutor)driver).executeScript("window.scrollBy(0,500)");
    Thread.sleep(1000);
    //driver.executeScript("document.getElementById('text-8').scrollIntoView(true);");
    driver.findElement(By.xpath("//div[5]/div[2]/div/div/div/div[@class='ant-select-selection__rendered']")).click();
    Thread.sleep(1000);
    driver.findElement(By.xpath("//div[4]/div/div/div/ul/li[@class='ant-select-dropdown-menu-item']")).click();
    Thread.sleep(1000);
    driver.findElement(By.xpath("//div[6]/div[2]/div/div/div/div[@class='ant-select-selection__rendered']")).click();
    Thread.sleep(1000);
    driver.findElement(By.xpath("//div[5]/div/div/div/ul/li[@class='ant-select-dropdown-menu-item']")).click();
    driver.findElement(By.xpath("//button[@type='submit']")).click();
  }
  private void login(String url) {
    driver.get(url);
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
