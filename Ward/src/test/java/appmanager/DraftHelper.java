package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import model.DraftData;
import model.TeamData;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import restclient.model.Team;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class DraftHelper extends BaseHelpers{
  public DraftHelper(AppiumDriver driver) {
    super(driver);
  }

  public void firstDraftTip() {
    new WebDriverWait(driver, 30, 1000)
            .until(ExpectedConditions.visibilityOf(firstDraftTip));
    okButton.click();
  }

  public void secondDraftTip() {
    new WebDriverWait(driver, 30, 1000)
            .until(ExpectedConditions.visibilityOf(secondDraftTip));
    okButton.click();
  }

  public void finalDraftTip(int players_amount) {
    //Wait for first tip appearing
    //wait_timer.until(ExpectedConditions.visibilityOfElementLocated(By.id("Time is over")));
    new WebDriverWait(driver, 240)
            .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@"+textAttribute+"='Time is over']")));
    //Check tip content
    String message;
    if (players_amount!=0){
      message = "You selected "+players_amount+" of 10 champions and can get up to "+players_amount*10+" 000 wp";
    } else {
      message = "You selected 0 of 10 champions and can get up to 0 wp";
    }
    Assert.assertNotNull(driver.findElementByAccessibilityId(message));
    //Check visibility of OK button
    //System.out.println(driver.getPageSource());
    List<WebElement> ok_button = driver.findElementsByAccessibilityId("Ward it!");
    Assert.assertTrue(ok_button.get(1).isDisplayed());
    //Click on OK button
    ok_button.get(1).click();
    //new TouchAction(driver).tap(button).perform();
  }

  public void acceptFinalDraftTip() {
    new WebDriverWait(driver, 240)
            .until(ExpectedConditions.visibilityOf(wardItTip));
    okDoneButton.click();
  }

  public void keepPredicting() {
    new WebDriverWait(driver, 240)
            .until(ExpectedConditions.visibilityOf(wardItTip));
    keepPredictingButton.click();
  }

  public void checkTeamInDraft(TeamData team, DraftData heroes) {
    driver.findElement(By.xpath("//*[@" + textAttribute + "='" + team.getName() + "']"));
    WebElement team_collection = getTeamCollection(team,heroes,"Draft");
    verifyTeamList(team, team_collection, heroes, "Draft");
    System.out.println(team.getName() + " players are OK.");
  }

  public void checkTeamInResults(TeamData team, DraftData heroes) {
    Assert.assertNotNull(driver.findElementByXPath("//*[@" + textAttribute + "='" + toUpperCase(team.getName()) + "']"));
    WebElement team_collection = getResultsTeamCollection(team);
    verifyTeamList(team, team_collection, heroes, "Results");
  }

  private WebElement getTeamCollection(TeamData team,DraftData heroes, String screen) {
    //heroes != null when there is Draft results screen
    WebElement team_collection;
    if (team.getIndex()==1) {
        if ((heroes == null || screen.equals("Draft"))
                && !platform.equals("ANDROID"))
        team_collection = collections.get(1);
      else
        team_collection = collections.get(0);
    }
      else {
        if ((heroes == null || screen.equals("Draft"))
                && !platform.equals("ANDROID"))
        team_collection = collections.get(2);
      else
        team_collection = collections.get(1);
    }
    return team_collection;
  }

  private WebElement getResultsTeamCollection(TeamData team) {
    WebElement team_collection;
    //TODO Refactor for Android
    if (team.getIndex()==1)
        team_collection = collections.get(0);
    else
        team_collection = collections.get(1);
    return team_collection;
  }

  private void verifyTeamList(TeamData team, WebElement team_collection, DraftData heroes, String screen) {
      List<WebElement> players; //Find the list of cells for players
      if (platform.equals("ANDROID"))
          players = team_collection.findElements(By.xpath("//*[contains(@id, '_player')]"));
      else
          players = team_collection.findElements(By.xpath("//XCUIElementTypeCell"));

    //Check names and roles of team members
    for (int i = 0; i < players.size(); i++) {
      String player_name = toUpperCase(team.getTeam().get(i).getName());
      String player_role = toUpperCase(team.getTeam().get(i).getRole());
        if (player_role.equals("SUPPORT"))
        player_role = "SUP";

        List<WebElement> player_attributes; //Find the list of one player attributes (Name and Role)
        if (platform.equals("ANDROID"))
            player_attributes = players.get(i).findElements(By.className("TextView"));
        else
            player_attributes = players.get(i).findElements(By.xpath(".//XCUIElementTypeStaticText"));
        assertThat(player_name, equalTo(player_attributes.get(0).getText()));

      String current_hero = null;
        if (heroes != null)
        current_hero = heroes.getDraft().get(i);
        if (current_hero != null)
            assertThat(current_hero, equalTo(player_attributes.get(1).getText()));
        else
            assertThat(player_role, equalTo(player_attributes.get(1).getText()));
    }
  }

  public void findHeroAndSelect(String hero) {
    if (platform.equals("ANDROID")) {
      //Tap search icon
      searchButton.click();
      //Enter hero name into search field
      searchField.sendKeys(hero);
      heroesCollection.get(0).click();
    } else {
      //Get current list of heroes
      WebElement heroes_collection = heroesCollection.get(0);
      //Find required hero
      char[] capital_letter = hero.toCharArray();
      driver.findElement(MobileBy.ByAccessibilityId.AccessibilityId(String.valueOf(capital_letter[0]))).click();
      while (true) {
        List<WebElement> heroes = heroes_collection.findElements(By.xpath(".//XCUIElementTypeCell"));
        try {
          String up_hero = toUpperCase(hero);
          driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"" + up_hero + "\"][2]")).click();
          System.out.println("Hero is found and ready to be set!");
          break;
        } catch (NoSuchElementException e) {
          System.out.println("I'm going to swipe " + heroes.get(2).findElement(By.xpath(".//XCUIElementTypeStaticText")).getText());
          swipe("Left", heroes.get(2).findElements(By.xpath(".//XCUIElementTypeStaticText")).get(1));
        }
      }
    }
  }

  public void setHeroToPlayer(Team team, int team_index, int player_num, String hero_name) {
    findHeroAndSelect(hero_name);

    WebElement team_collection = null;
    if (platform.equals("ANDROID")) {
      team_collection = driver.findElement(By.id("team1_container"));
      System.out.println("Team container is found!");

      if (team_index == 1 && team_collection != null)
        swipeAndroid("Right", team_collection);
      else if (team_index == 2 && team_collection != null)
        swipeAndroid("Left", team_collection);
      else
        System.out.println("Failed to swipe team container for some reasons.");

      WebElement player = driver.findElement(By.id("team" + team_index + "_player" + player_num));
      player.click();
      //Check hero name in player's slot
      player.findElement(By.xpath(".//*[@text='" + hero_name + "']"));
    } else {
      String player_name = team.getPlayers().get(player_num - 1).getTitle().toUpperCase();
      WebElement player = driver.findElement(By.xpath("//XCUIElementTypeCell[.//*[@name='" + player_name + "']]"));
      player.click();
      assertThat(driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + player_name + "']/following-sibling::*")).getText(), equalTo(hero_name));
    }
  }

  public void backToMatchfeedFromDraft() {
    formButtons.get(1).click();
    WebElement back_button = driver.findElement(By.xpath("//XCUIElementTypeButton[@name='Back to matchfeed']"));
    back_button.click();
  }

  public void backToMatchfeedFromDraftResults() {
    formButtons.get(0).click();
    WebElement back_button = driver.findElement(By.xpath("//XCUIElementTypeButton[@name='Back to matchfeed']"));
    back_button.click();
  }

  public void setHeroesToTeam(TeamData team1, DraftData heroes1) {
    int limit = heroes1.getDraft().size();
    for (int i = 0; i < limit; i++) {
      if (heroes1.getDraft().get(i)!=null){
        oldsetHeroToPlayer(team1, i + 1, heroes1.getDraft().get(i));
      }
    }
  }

  public void removeHeroFromPlayer(Team team, int team_index, int player_num) throws InterruptedException {
    WebElement team_collection;
    if (platform.equals("ANDROID")) {
      team_collection = driver.findElement(By.id("team1_container"));
      System.out.println("Team container is found!");

      if (team_index == 1 && team_collection != null)
        swipeAndroid("Right", team_collection);
      else if (team_index == 2 && team_collection != null)
        swipeAndroid("Left", team_collection);
      else
        System.out.println("Failed to swipe team container for some reasons.");
      driver.findElement(By.id("team" + team_index + "_player" + player_num)).click();
      WebElement hero_removal_text = driver.findElement(By.id("heroes_delete_desc"));

      PointOption tap_point = new PointOption(); //Text "Tap here to remove champion" isn't clickable. Creating workaround.
      tap_point.withCoordinates(hero_removal_text.getLocation().getX(), hero_removal_text.getLocation().getY());
      new TouchAction(driver).tap(tap_point).perform();
      //new TouchAction(driver).tap(TapOptions.tapOptions().withElement(ElementOption.element(hero_removal_text))).perform();
      //driver.findElement(By.xpath("//*[contains(@text,'Tap here to remove')]")).click();
    } else {
      String player_name = team.getPlayers().get(player_num - 1).getTitle().toUpperCase();
      //{"using":"xpath","value":"//XCUIElementTypeStaticText[@name='FLAME']"}
      ///XCUIElementTypeStaticText[@name="Tap here to remove champion"]
      driver.findElement(By.xpath("(//XCUIElementTypeStaticText[@name='" + player_name + "'])[2]")).click();
      driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Tap here to remove champion\"]")).click();
      assertThat(driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + player_name + "']/following-sibling::*")).getText(),
              equalTo(team.getPlayers().get(player_num - 1).getPosition().toUpperCase()));
    }
  }

  public void moveHeroFromTo(TeamData team_from, int player_from, TeamData team_to, int player_to) {
    WebElement team_from_collection = getTeamCollection(team_from,null,"Draft");
    WebElement team_to_collection = getTeamCollection(team_to,null,"Draft");
    List<WebElement> players_from = team_from_collection.findElements(By.xpath("//XCUIElementTypeCell"));
    List<WebElement> players_to = team_to_collection.findElements(By.xpath("//XCUIElementTypeCell"));
    String hero = players_from.get(player_from-1).findElements(By.xpath(".//XCUIElementTypeStaticText")).get(1).getText();
    WebElement from_slot = players_from.get(player_from-1);
    WebElement to_slot = players_to.get(player_to-1);
    from_slot.click();
    to_slot.click();
    //Check that FROM slot is empty
    String expected_role = team_from.getTeam().get(player_from-1).getRole();
    if (expected_role.equals("Support")){
      expected_role = "Sup";
    }
    Assert.assertEquals(from_slot.findElements(By.xpath(".//XCUIElementTypeStaticText")).get(1).getText(),toUpperCase(expected_role));
    //Check that TO slot contains this hero
    Assert.assertEquals(to_slot.findElements(By.xpath(".//XCUIElementTypeStaticText")).get(1).getText(),hero);
  }

  public void tapWardIt() {
    wardItButton.click();
    //driver.findElement(By.xpath("//*[@"+textAttribute+"='Ward it!']")).click();
  }

  public void waitTimeIsOverTip() {
    //Check that timer shows 00:00 (28)
    //wait_timer.until(ExpectedConditions.attributeContains(formStaticTexts.get(28),"value","00:05 left"));
    new WebDriverWait(driver, 240)
            .until(ExpectedConditions.attributeContains(formStaticTexts.get(28),"value","00:05 left"));
    System.out.println("Time is over");
  }

  private void openLeaderboardInResults() {
    //WebElement arrow = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeImage[@name='arrow-top']")));
    //arrow.click();
    driver.findElement(By.xpath("//XCUIElementTypeImage[@name='arrow-top']/..")).click();
  }

  private void closeLeaderboard() {
    //WebElement arrow = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeImage[@name='arrow-top']")));
    //arrow.click();
    driver.findElement(By.name("Close")).click();
  }

  public void checkScoreOfDraft(int sum) {
    openLeaderboardInResults();
    closeLeaderboard();
  }

  public void checkFinalDraftCopyrights() {
    assertThat(finalDraftTitle.isDisplayed(), is(true));
    assertThat(finalDraftStandbyCopyright.isDisplayed(), is(true));
  }

  public void checkEndDraftPushResults(int winner_amount) {
    if (winner_amount==0){
      if (platform.equals("ANDROID"))
        new WebDriverWait(driver, 180, 500)
                .until(ExpectedConditions.and(
                        ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Oooops!']")),
                        ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='" + winner_amount + "/10']"))));
      else
        new WebDriverWait(driver, 180, 500)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@name='" + winner_amount + "/10']")));
      /*new WebDriverWait(driver, 180)
              .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@" + textAttribute + ",'You did not guess')]")));*/
    } else{
      if (platform.equals("ANDROID"))
        new WebDriverWait(driver, 180)
                .until(ExpectedConditions.and(
                        ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='You got " + winner_amount + "0 000 wp']")),
                        ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='" + winner_amount + "/10']"))));
      else
        new WebDriverWait(driver, 180)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='You got " + winner_amount + "0 000 wp']")));
    }
  }

  public void waitAndProceedEmptyDraft() {
    firstDraftTip();
    //Wait when time is over and we can start the game
    finalDraftTip(0);
  }

  public void tapGoToSummonersRift() {
    new WebDriverWait(driver, 240)
            .until(ExpectedConditions.and(
                    ExpectedConditions.elementToBeClickable(goToSummonerRift),
                    ExpectedConditions.visibilityOf(finalDraftText)
            ));
    goToSummonerRift.click();
  }

  public void oldremoveHeroFromPlayer(TeamData team, int player_num) {
    WebElement team_collection;
    if (platform.equals("ANDROID")) {
      team_collection = driver.findElement(By.id("team1_container"));
      System.out.println("Team container is found!");

      if (team.getIndex() == 1 && team_collection != null)
        swipeAndroid("Right", team_collection);
      else if (team.getIndex() == 2 && team_collection != null)
        swipeAndroid("Left", team_collection);
      else
        System.out.println("Failed to swipe team container for some reasons.");
      driver.findElement(By.id("team" + team.getIndex() + "_player" + player_num)).click();
      driver.findElement(By.id("heroes_delete_desc")).click();
      //driver.findElement(By.xpath("//*[contains(@text,'Tap here to remove')]")).click();
    } else {
      String player_name = team.getTeam().get(player_num - 1).getName().toUpperCase();
      //{"using":"xpath","value":"//XCUIElementTypeStaticText[@name='FLAME']"}
      ///XCUIElementTypeStaticText[@name="Tap here to remove champion"]
      driver.findElement(By.xpath("(//XCUIElementTypeStaticText[@name='" + player_name + "'])[2]")).click();
      driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Tap here to remove champion\"]")).click();
      assertThat(driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + player_name + "']/following-sibling::*")).getText(),
              equalTo(team.getTeam().get(0).getRole().toUpperCase()));
    }
  }

  public void oldsetHeroToPlayer(TeamData team, int player_num, String hero) {
    findHeroAndSelect(hero);

    WebElement team_collection = null;
    if (platform.equals("ANDROID")) {
      team_collection = driver.findElement(By.id("team1_container"));
      System.out.println("Team container is found!");

      if (team.getIndex() == 1 && team_collection != null)
        swipeAndroid("Right", team_collection);
      else if (team.getIndex() == 2 && team_collection != null)
        swipeAndroid("Left", team_collection);
      else
        System.out.println("Failed to swipe team container for some reasons.");

      WebElement player = driver.findElement(By.id("team" + team.getIndex() + "_player" + player_num));
      player.click();
      //Check hero name in player's slot
      player.findElement(By.xpath(".//*[@text='" + hero + "']"));
    } else {
      String player_name = team.getTeam().get(player_num - 1).getName().toUpperCase();
      WebElement player = driver.findElement(By.xpath("//XCUIElementTypeCell[.//*[@name='" + player_name + "']]"));
      player.click();
      assertThat(driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + player_name + "']/following-sibling::*")).getText(), equalTo(hero));
    }
  }

}
