package appmanager;

import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class AndroidAppManager extends AppManager {

  public AndroidAppManager(String executablePath, String device, String sdk) {
    super(MobilePlatform.ANDROID, executablePath, device, sdk);

    //getCapabilities().setCapability(MobileCapabilityType.NO_RESET, true);
    //getCapabilities().setCapability(MobileCapabilityType.PLATFORM_VERSION, sdk);
    getCapabilities().setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
  }
}
