package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import model.GameData;
import model.PlayerData;
import model.TeamData;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import restclient.AdminClient;
import restclient.model.Game;
import restclient.model.Player;
import restclient.model.Team;

import java.io.IOException;
import java.util.List;

import static java.util.Objects.isNull;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class MatchHelper extends BaseHelpers {
  PointOption tap_point = new PointOption()
          .withCoordinates(100, 100);
  WebDriverWait wait30 = new WebDriverWait(driver, 30);

  private AdminClient admin = new AdminClient();

  public MatchHelper(AppiumDriver driver) {
    super(driver);
  }

  public void verifyMatchfeedTitle() {
    new WebDriverWait(driver, 30)
            .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@" + textAttribute + "='" + "Matchfeed" + "']")));
    //assertThat(driver.findElement(By.xpath("//*[@" + textAttribute + "='" + "Matchfeed" + "']")), is(notNullValue()));
  }

  public boolean verifyLocalDate() {
    String current = currentLocalDateUpper();
    for (MobileElement t : formStaticTexts) {
      try {
        if (t.getText().equals(current)) {
          System.out.println("Date is found");
          return true;
        }
      } catch (Exception e) {
        System.out.println("Date isn't found yet.");
      }
    }
    return false;
  }

  public void selectFirstBloodChampion(Team team, String player_position) {
    String player_title;
    WebElement player;
    for (Player p : team.getPlayers()) {
      if (p.getPosition().equals(player_position)) {
        player_title = p.getTitle();
        System.out.println(String.format("Player %s has %s position", player_title.toUpperCase(), player_position.toUpperCase()));
        if (platform.equals("ANDROID")) {
          player = driver.findElement(By.xpath("//*[@text='" + player_title.toUpperCase() + "']"));
          assertThat(driver.findElement(By.xpath("//*[@text='" + player_title.toUpperCase() + "']/preceding-sibling::*[1]")).getText(),
                  equalTo(p.getPosition().toUpperCase()));
        } else {
          player = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + p.getTitle().toUpperCase() + "']"));
          try {
            assertThat(driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + p.getTitle().toUpperCase() + "']/preceding-sibling::*[1]")).getText(), //it can be [2] - need to check in future
                    equalTo(p.getPosition().toUpperCase()));
          } catch (AssertionError a) {
            assertThat(driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + p.getTitle().toUpperCase() + "']/preceding-sibling::*[2]")).getText(), //it can be [2] - need to check in future
                    equalTo(p.getPosition().toUpperCase()));
          }
        }
        player.click();
      }
    }
    System.out.println("Player ward is selected.");
  }

  public void selectTowerTeam(Team team) {
    WebElement steam;
    if (platform.equals("ANDROID"))
      steam = driver.findElement(By.xpath("//*[@text='" + team.getTitle().toUpperCase() + "']"));
    else
      steam = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + team.getTitle().toUpperCase() + "']"));
    steam.click();
    System.out.println(String.format("%s team is selected for Tower ward.", team.getTitle().toUpperCase()));
  }

  @Deprecated
  public void matchfeedContains(GameData game) {
    MobileElement gameCell = findCellForGame(game);
    assertThat(gameCell, is(notNullValue()));
  }

  @Deprecated
  private MobileElement findCellForGame(GameData game) {
    String team1Name = game.getTeam1().getShortName();
    String team2Name = game.getTeam2().getShortName();
    String gameTime = game.getTime();
    MobileElement gameCell = null;
    String bottomText1 = "More games";
    String bottomText2 = "Stay tuned";
    boolean bottomLine = false;

    swipe("Down"); // We need to update game list

    //Take every game cell
    while (!bottomLine) {
      for (MobileElement g : games) {
        //If there is a game for Team1 and Team2, we will find it by Team1 and Team2 names and Game time.
        try {
          new WebDriverWait(driver, 4)
                  .until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@" + textAttribute + "='" + team1Name + "']"))));
          //assertThat(g.findElement(By.xpath("//*[@" + textAttribute + "='" + team1Name + "']")), is(notNullValue()));
          assertThat(g.findElement(By.xpath("//*[@" + textAttribute + "='" + team2Name + "']")), is(notNullValue()));
          assertThat(g.findElement(By.xpath("//*[@" + textAttribute + "='" + gameTime + "']")), is(notNullValue()));
          gameCell = g;
          System.out.println("Found game cell");
        } catch (AssertionError | NoSuchElementException e) {
          //Otherwise we get an exception and move to the next game cell
        }
        try { //If there is end text, then this set of cells is final
          g.findElement(By.xpath("//*[contains(@" + textAttribute + ", '" + bottomText1 + "') and contains(@" + textAttribute + ", '" + bottomText2 + "')]"));
          bottomLine = true;
        } catch (NoSuchElementException e) {
        }
      }
      if (isNull(gameCell)) { // If no game found, we can try to swipe Up
        swipe("Up");
      }
    }

    return gameCell;
  }

  @Deprecated
  public void tapGame(GameData game) {
    swipe("Down"); //To update the list of games

    String team1Name = game.getTeam1().getName();
    String team2Name = game.getTeam2().getName();
    String gameTime = game.getTime();
    WebElement targetGame = null;
    while (true) {
      try {
        new WebDriverWait(driver, 4)
                .until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@" + textAttribute + "='" + team1Name + "']"))));
        List<WebElement> parentGameCells;
        if (platform.equals("ANDROID"))
          parentGameCells = driver.findElements(By.xpath("//*[@" + textAttribute + "='" + team1Name + "']/.."));
        else
          parentGameCells = driver.findElements(By.xpath("//*[@" + textAttribute + "='" + team1Name + "']/../.."));
        for (WebElement g : parentGameCells) {
          try {
            g.findElement(By.xpath(".//*[@" + textAttribute + "='" + team2Name + "']"));
            g.findElement(By.xpath(".//*[contains(@" + textAttribute + ",'" + gameTime + "')]"));
            System.out.println("Found the game " + team1Name + " vs " + team2Name + " at " + gameTime + "in Matchfeed. Tapping it...");
            targetGame = g;
          } catch (NoSuchElementException n) {
            System.out.println("Continue searching the game...");
          }
        }
        if (targetGame == null)
          throw new NoSuchElementException("Swipe required.");
        else {
          targetGame.click();
          break;
        }
      } catch (NoSuchElementException e) {
        try {
          driver.findElement(By.xpath("//*[contains(@" + textAttribute + ", 'More games')]"));
          System.out.println("Can't find the game " + team1Name + " vs " + team2Name + " at " + gameTime);
          System.out.println(e);
          break;
        } catch (NoSuchElementException m) {
          System.out.println("Swipe Up.");
          swipe("Up");
        }
      }
    }
  }

  public void tapGame(Game game) throws IOException {
    swipe("Down"); //To update the list of games

    List<String> teamIDs = game.getTeams();
    Team team1 = admin.parseTeamfromManifestByID(teamIDs.get(0));
    Team team2 = admin.parseTeamfromManifestByID(teamIDs.get(1));

    String team1Name = team1.getAcronim();
    String team2Name = team2.getAcronim();
    String gameTime = game.getTime();

    System.out.println("Game ID: " + game.getId());
    System.out.println(String.format("Teams of this game are %s and %s. Game time is: %s", team1Name, team2Name, gameTime));

    WebElement targetGame = null;
    while (true) {
      try {
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@" + textAttribute + "='" + team1Name + "']"))));
        List<WebElement> parentGameCells;
        if (platform.equals("ANDROID"))
          parentGameCells = driver.findElements(By.xpath("//*[@" + textAttribute + "='" + team1Name + "']/.."));
        else
          parentGameCells = driver.findElements(By.xpath("//*[@" + textAttribute + "='" + team1Name + "']/../.."));
        for (WebElement g : parentGameCells) {
          try {
            g.findElement(By.xpath(".//*[@" + textAttribute + "='" + team2Name + "']"));
            g.findElement(By.xpath(".//*[contains(@" + textAttribute + ",'" + gameTime + "')]"));
            System.out.println("Found the game " + team1Name + " vs " + team2Name + " at " + gameTime + "in Matchfeed. Tapping it...");
            targetGame = g;
          } catch (NoSuchElementException n) {
            System.out.println("Continue searching the game...");
          }
        }
        if (targetGame == null)
          throw new NoSuchElementException("Swipe required.");
        else {
          targetGame.click();
          break;
        }
      } catch (NoSuchElementException e) {
        try {
          driver.findElement(By.xpath("//*[contains(@" + textAttribute + ", 'More games')]"));
          System.out.println("Can't find the game " + team1Name + " vs " + team2Name + " at " + gameTime);
          System.out.println(e);
          break;
        } catch (NoSuchElementException m) {
          System.out.println("Swipe Up.");
          swipe("Up");
        }
      }
    }
  }

  @Deprecated
  public void startMatchCopyrights(GameData game) {
    String team1Name = game.getTeam1().getShortName();
    String team2Name = game.getTeam2().getShortName();
    String team1FullName = game.getTeam1().getName();
    String team2FullName = game.getTeam2().getName();
    String gameTime = game.getTime();

    try {
      //Check shortnames of Teams
      driver.findElement(By.xpath("//*[@" + textAttribute + "='" + team1Name + "']"));
      driver.findElement(By.xpath("//*[@" + textAttribute + "='" + team2Name + "']"));
      //Check fullnames of Teams
      driver.findElement(By.xpath("//*[@" + textAttribute + "='" + team1FullName + " vs " + team2FullName + "']"));
      //Check game time
      driver.findElement(By.xpath("//*[@" + textAttribute + "='" + gameTime + "']"));
      //Check game LocalDate (at title)
      driver.findElement(By.xpath("//*[@" + textAttribute + "='" + currentLocalDate() + "']"));
      //Check draft is available
      //driver.findElement(By.xpath("//*[contains(@" + textAttribute + ",'" + "is available" + "')]"));
      System.out.println("Start match screen copyrights are OK.");
    } catch (NoSuchElementException e) {
      System.out.println("Can't evaluate start match screen :(");
      System.out.println(e);
    }
  }

  public void startMatchCopyrights(Game game) throws IOException {
    List<String> teamIDs = game.getTeams();
    Team team1 = admin.parseTeamfromManifestByID(teamIDs.get(0));
    Team team2 = admin.parseTeamfromManifestByID(teamIDs.get(1));

    String team1Name = team1.getAcronim();
    String team2Name = team2.getAcronim();
    ;
    String gameTime = game.getTime();
    String team1FullName = team1.getTitle();
    String team2FullName = team2.getTitle();

    try {
      //Check shortnames of Teams
      driver.findElement(By.xpath("//*[@" + textAttribute + "='" + team1Name + "']"));
      driver.findElement(By.xpath("//*[@" + textAttribute + "='" + team2Name + "']"));
      //Check fullnames of Teams
      driver.findElement(By.xpath("//*[@" + textAttribute + "='" + team1FullName + " vs " + team2FullName + "']"));
      //Check game time
      driver.findElement(By.xpath("//*[@" + textAttribute + "='" + gameTime + "']"));
      //Check game LocalDate (at title)
      driver.findElement(By.xpath("//*[@" + textAttribute + "='" + currentLocalDate() + "']"));
      //Check draft is available
      //driver.findElement(By.xpath("//*[contains(@" + textAttribute + ",'" + "is available" + "')]"));
      System.out.println("Start match screen copyrights are OK.");
    } catch (NoSuchElementException e) {
      System.out.println("Can't evaluate start match screen :(");
      System.out.println(e);
    }
  }

  public void tapToJoin() {
    new WebDriverWait(driver, 10)
            .until(ExpectedConditions.elementToBeClickable(joinButton))
            .click();
  }

  public void login(String country, String phone) throws InterruptedException {
    tapSignInButton();
    proceedPhone(phone, country);
    enterCode("198412");
    NextBtn.click();
    allowNotifications();
  }

  @Deprecated
  public void selectGame(GameData game) {
    tapGame(game);
    tapToJoin();
  }

  @Deprecated
  public void commonMatchCopyrights(GameData game) {
    checkTeamsInGame(game);
    checkGameIsLive();
    checkCommonWardsCopyrights();
  }

  private void checkGameIsLive() {
    Assert.assertNotNull(driver.findElementByXPath("//XCUIElementTypeStaticText[@value='LIVE']"));
  }

  @Deprecated
  private void checkTeamsInGame(GameData game) {
    Assert.assertNotNull(driver.findElementByAccessibilityId(game.getTeam1().getShortName()));
    Assert.assertNotNull(driver.findElementByAccessibilityId(game.getTeam2().getShortName()));
  }

  private void checkCommonWardsCopyrights() {
    //TODO refactor for Android
    Assert.assertNotNull(driver.findElementByAccessibilityId("Ward now and get"));
    Assert.assertNotNull(driver.findElementByAccessibilityId("Ward it!"));
  }

  public void firstBloodCopyrights() throws InterruptedException {
    Thread.sleep(2000);
    assertThat(driver.findElement(By.xpath("//*[@" + textAttribute + "='Who will spill first blood?']")).isDisplayed(), is(true));
    //Arrows left and right
    assertThat(arrowLeft.isEnabled(), is(false));
    assertThat(arrowRight.isEnabled(), is(true));
  }

  public void towerCopyrights() {
    assertThat(towerQuestionTitle.getText(), equalTo("Which team will destroy the first tower?"));
  }

  public void tapWardIt() {
    wardItButton.click();
  }

  public void fBloodPlayersSelectionCopyrights() throws InterruptedException {
    Thread.sleep(2000);
    //Title "FIRST BLOOD WARD"
    assertThat(fBloodSelectPlayerTitle.getText(), equalTo("FIRST BLOOD WARD"));
    //Title "Who will spill first blood?"
    assertThat(fBloodSelectPlayerSubTitle.getText(), equalTo("Who will spill first blood?"));
    //Ward It button is not enabled
    assertThat(wardItButton.isEnabled(), is(false));
  }

  public void towerWardTeamsCopyrights() {
    assertThat(towerSelectTeamTitle.getText(), equalTo("FIRST TOWER WARD"));
    assertThat(towerSelectTeamSubtitle.getText(), equalTo("Which team will destroy the first tower?"));
    assertThat(wardItButton.isEnabled(), is(false));
  }

  public void closeFirstBlood() {
    //TODO Refactor for Android
    //Click close button
    //wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByAccessibilityId("Close"))).click();
    new WebDriverWait(driver, 180)
            .until(ExpectedConditions.elementToBeClickable(driver.findElementByAccessibilityId("Close"))).click();
  }

  public void checkFirstBloodPlayersFor(Team team) {
    for (Player p : team.getPlayers()) {
      if (p.isRegular()) {
        if (platform.equals("ANDROID")) {
          new WebDriverWait(driver, 30, 3000)
                  .until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@text='" + team.getAcronim() + "']"))));
          assertThat(driver.findElement(By.xpath("//*[@text='" + p.getTitle().toUpperCase() + "']/preceding-sibling::*[1]")).getText(),
                  equalTo(p.getPosition().toUpperCase()));
        } else {
          driver.findElement(By.xpath("//*[@name='" + team.getAcronim() + "']"));
          //new WebDriverWait(driver, 30, 3000).until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@name='" + team.getAcronim() + "']"))));
          try {
            assertThat(driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + p.getTitle().toUpperCase() + "']/preceding-sibling::*[1]")).getText(), //it can be [2] - need to check in future
                    equalTo(p.getFullPosition().toUpperCase()));
          } catch (AssertionError a) {
            assertThat(driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + p.getTitle().toUpperCase() + "']/preceding-sibling::*[2]")).getText(), //it can be [2] - need to check in future
                    equalTo(p.getFullPosition().toUpperCase()));
          }
          //assertThat(driver.findElement(By.xpath("//*[@name='" + p.getTitle().toUpperCase() + "']/preceding-sibling::*[@name='" + p.getPosition().toUpperCase() + "']")).getText(), is(notNullValue()));
        }
        System.out.println("Player " + p.getTitle().toUpperCase() + " is found in the team.");
      }
    }
  }

  public void checkTowerTeam(Team team) {
    if (platform.equals("ANDROID"))
      driver.findElement(By.xpath("//*[@text='" + team.getTitle().toUpperCase() + "']"));
    else
      driver.findElement(By.xpath("//*[@name='" + team.getTitle().toUpperCase() + "']"));
  }

  public void checkWardItDisabled() {
    assertThat(wardItButton.isEnabled(), equalTo(false));
  }

  public void checkWardItEnabled() {
    assertThat(wardItButton.isEnabled(), equalTo(true));
  }

  @Deprecated
  public void checkFirstBloodPlayersIn(TeamData team) {
    //Find team names
    driver.findElement(By.xpath("//*[@" + textAttribute + "='" + team.getShortName().toUpperCase() + "']"));
    System.out.println("Team " + team.getShortName() + " is found on the screen.");
    for (PlayerData p : team.getTeam()) {
      if (platform.equals("ANDROID"))
        assertThat(driver.findElement(By.xpath("//TextView[@text='" + p.getName().toUpperCase() + "']/preceding-sibling::*[1]")).getText(),
                equalTo(p.getRole().toUpperCase()));
      else
        assertThat(driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + p.getName().toUpperCase() + "']/preceding-sibling::XCUIElementTypeStaticText[2]")).getText(),
                equalTo(p.getRole().toUpperCase()));
      System.out.println("Player " + p.getName().toUpperCase() + " is found in the team.");
    }
  }

  @Deprecated
  public void checkTeamsInFirstBloodWard(TeamData team1, TeamData team2) {
    // DEPRECATED!
    //Parent collection
    WebElement parent = collections.get(2);
    //Assert teams in collection
    Assert.assertNotNull(parent.findElement(By.id(toUpperCase(team1.getShortName()))));
    Assert.assertNotNull(parent.findElement(MobileBy.ByAccessibilityId.AccessibilityId(toUpperCase(team2.getShortName()))));
    List<WebElement> players = parent.findElements(By.className("XCUIElementTypeCell"));
    String player_name;
    String player_role;
    int j = -1;
    for (int i = 2; i < players.size(); i++) {
      if (i % 2 == 0) {
        j++;
        player_name = toUpperCase(team1.getTeam().get(j).getName());
        player_role = toUpperCase(team1.getTeam().get(j).getRole());
      } else {
        player_name = toUpperCase(team2.getTeam().get(j).getName());
        player_role = toUpperCase(team2.getTeam().get(j).getRole());
      }
      System.out.println(player_name + " " + player_role);
      List<WebElement> player_attributes = players.get(i).findElements(By.className("XCUIElementTypeStaticText"));
      Assert.assertEquals(player_role, player_attributes.get(0).getText());
      Assert.assertEquals(player_name, player_attributes.get(1).getText());
    }
  }

  @Deprecated
  public void selectFirstBloodChampion(TeamData team, int player_num) {
    //Parent collection
    WebElement parent = collections.get(2);
    //Assert teams in collection
    List<WebElement> players = parent.findElements(By.className("XCUIElementTypeCell"));
    //Calculate index of player
    int index = 2 * player_num;
    //Select player with correct index
    if (team.getIndex() == 1)
      players.get(index).click();
    else
      players.get(index + 1).click();
  }

  public void wardIt() {
    if (platform.equals("ANDROID")) {
      WebElement ward_it = driver.findElementById("wardAvailableWardit");
      tap_point.withCoordinates(ward_it.getLocation().getX(), ward_it.getLocation().getY());
      new TouchAction(driver).tap(tap_point).perform();
      System.out.println("Ward it! button is pressed!");
    } else
      new WebDriverWait(driver, 80)
              .until(ExpectedConditions.elementToBeClickable(wardItButton)).click();
  }

  public void tapPickLine(String line) {
    tapLine(line, commonPickLineFieldImg);
  }

  public void tapFBPickLine(String line) {
    tapLine(line, fbPickLineFieldImg);
  }

  private void tapLine(String line, WebElement img) {
    int baseY = img.getLocation().getY();
    switch (line) {
      case "bottom":
        tap_point.withCoordinates((int) Math.round(img.getSize().width * 0.84), baseY + img.getSize().height / 2);
        new TouchAction(driver).tap(tap_point).perform();
        break;
      case "jungle":
        tap_point.withCoordinates(Math.round(img.getSize().width / 3), baseY + Math.round(img.getSize().height / 3));
        new TouchAction(driver).tap(tap_point).perform();
        break;
      case "middle":
        tap_point.withCoordinates(img.getSize().width / 2, baseY + img.getSize().height / 2);
        new TouchAction(driver).tap(tap_point).perform();
        break;
      case "top":
        tap_point.withCoordinates((int) Math.round(img.getSize().width - img.getSize().width * 0.84), baseY + img.getSize().height / 2);
        new TouchAction(driver).tap(tap_point).perform();
        break;
    }
  }

  public void verifyPicksOnScreen() {
    //TODO Refactor for Android
    //Assert image of main pick
    Assert.assertNotNull(driver.findElementByAccessibilityId("lol-stand-warded"));
    //Assert image of additional pick (MID line)
    Assert.assertNotNull(driver.findElementByAccessibilityId("line_pick_thumb_mid"));
  }

  public void tapArrow(String direction) {
    //TODO Refactor for Android
    driver.findElementByAccessibilityId("arrow " + direction).click();
  }

  public void wardIsBlocked() {
    //TODO Refactor for Android
    //Assert lock splash
    Assert.assertNotNull(driver.findElementByAccessibilityId("lock-splash"));
    //Assert lock timer
    Assert.assertNotNull(driver.findElementByAccessibilityId("UNLOCKS IN"));
  }

  public void copywrites(String reason) {
    //TODO Refactor for Android
    String copywrites = null;
    switch (reason) {
      case "Tower":
        copywrites = "Which team will destroy the first tower?";
        break;
      case "Dragon":
        copywrites = "Which team will kill the first dragon?";
        break;
      case "Herald":
        copywrites = "Will the Rift Herald be killed?";
        break;
      case "Baron":
        copywrites = "Which team will get the Baron first?";
        break;
      case "Pentakill":
        copywrites = "Will there be a Pentakill?";
        break;
      case "ACE":
        copywrites = "Will there be a Ace?";
        break;
      case "Inhibitor":
        copywrites = "Which team will destroy the Inhibitor first??";
        break;
      case "WinGame":
        copywrites = "Who will win this game?";
        break;
    }
    if (copywrites != null) {
      new WebDriverWait(driver, 180)
              .until(ExpectedConditions.visibilityOfElementLocated(By.id(copywrites)));
    } else {
      System.out.println("Copywrite parameter is invalid.");
    }
  }

  public void waitPush(String reason) {
    String pushText = null;
    switch (reason) {
      case "tower":
        pushText = "Which team will destroy the first tower?";
        break;
      case "dragon":
        pushText = "Which team will kill the first dragon?";
        break;
      case "herald":
        pushText = "Will the Rift Herald be killed?";
        break;
      case "baron":
        pushText = "Which team will get the Baron first?";
        break;
      case "pentakill":
        pushText = "Will there be a Pentakill?";
        break;
      case "ace":
        pushText = "Will there be a Ace?";
        break;
      case "inhibitor":
        pushText = "Which team will destroy the Inhibitor first??";
        break;
      case "nexus":
        pushText = "Who will win this game?";
        break;
    }
    if (pushText != null) {
      new WebDriverWait(driver, 90)
              .until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[contains(@text, 'the first tower?')]"))));
      new WebDriverWait(driver, 90)
              .until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[contains(@text, 'unlocked')]"))));
      System.out.println(reason + " push");
    } else {
      System.out.println("Incorrect push names.");
    }
  }

  public void commonWardLineCopyrights() throws InterruptedException {
    Thread.sleep(2000);
    assertThat(commonSelectLineTitle.getText(), equalTo("BONUS WARD"));
    //assertThat(commonSelectLineSubtitle.getText(), equalTo("Which line?")); //temporary hidden
    assertThat(wardItButton.isEnabled(), is(false));
  }

  public void firstBloodWrongCopyrights(String player_name) {
    //Subtitle
    new WebDriverWait(driver, 30)
            .until(ExpectedConditions.and(
                    ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@" + textAttribute + "='Oooops!']")),
                    ExpectedConditions.visibilityOf(commonFailSubTitle)));
    if (platform.equals("ANDROID"))
      assertThat(driver.findElement(By.xpath("//*[contains(@text, 'by " + player_name + "')]")).isDisplayed(), is(true));
    else
      assertThat(driver.findElement(By.xpath("//*[contains(@name, 'by " + player_name + "')]")).isDisplayed(), is(true));
  }

  private WebElement subtitle; //TODO Refactor WebDriverWaits
  public void towerWrongTeamCopyrights(Team team) {
    new WebDriverWait(driver, 30)
            .until(ExpectedConditions.and(
                    ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@" + textAttribute + "='Oooops!']")),
                    ExpectedConditions.visibilityOf(commonFailSubTitle)));
    if (platform.equals("ANDROID"))
      assertThat(driver.findElement(By.xpath("//*[contains(@text, 'by " + team.getTitle() + "')]")).isDisplayed(), is(true));
    else
      subtitle = wait30.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[contains(@name, 'by " + team.getTitle() + "')]"))));
    assertThat(subtitle.getText(), equals("Tower destoyed by " + team.getTitle()));
  }

  public String getPlayerName(Team team, String player_position) {
    return team.getPlayers().stream().filter(p -> p.getPosition().equals(player_position)).findFirst().orElse(null).getTitle();
  }

  public String getWardScore() {
    if (platform.equals("ANDROID"))
      return wardScore.getAttribute("contentDescription"); // attribute could be "name" too
    else {
      System.out.println(driver.getPageSource());
    }
      return wardScore.getAttribute("content-desc");
  }

  public void sandbox() {
    Runnable wait_title =
            () -> {
              new WebDriverWait(driver, 180, 500)
                      .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@text,'Welcome')]")));
              System.out.println("1st Screen.");
            };
    Runnable wait_text =
            () -> {
              new WebDriverWait(driver, 180)
                      .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@text,'eSport game')]")));
              System.out.println("2nd Screen.");
            };
    Thread pushTitle = new Thread(wait_title);
    Thread pushText = new Thread(wait_text);

    pushText.start();
    pushTitle.start();


  }


}
