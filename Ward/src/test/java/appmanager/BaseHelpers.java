package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.util.*;

public class BaseHelpers extends LocatorsBase{
  protected AppiumDriver driver;

  public BaseHelpers(AppiumDriver driver) {
    super(driver);
    this.driver = driver;
  }

  protected String toUpperCase(String string) {
    char[] str = string.toCharArray();
    for (int i=0;i<str.length;i++){
      str[i] = Character.toUpperCase(str[i]);
    }
    return String.valueOf(str);
  }
  protected String currentLocalDateUpper() {
    Calendar cal = Calendar.getInstance();
    String day_of_week = toUpperCase(cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US));
    String month = toUpperCase(cal.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US));
    String date = toUpperCase(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
    return day_of_week+", "+month+" "+date;
  }
  protected String currentLocalDate() {
    Calendar cal = Calendar.getInstance();
    String day_of_week = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
    String month = cal.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US);
    String date = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
    return day_of_week + ", " + month + " " + date;
  }

  protected String cleaned(String text){
    return text.replaceAll("\\n"," ");
  }

  public void writeNewPhone() throws IOException {
    int phone = new Random().nextInt(Integer.MAX_VALUE);
    List<String> list1 = new ArrayList<>();
    list1.add(""+phone);
    if (Files.isWritable(Paths.get("accounts.txt"))) {
      Files.write(Paths.get("accounts.txt"), list1, StandardOpenOption.APPEND);
    }
  }

  public String readLastPhoneFromFile() throws IOException {
    List<String> phones = null;
    if (Files.isReadable(Paths.get("accounts.txt"))) {
      phones = Files.readAllLines(Paths.get("accounts.txt"));
      if (phones.size()<1)
        System.out.println("File doesn't contain correct phone!");
    }
    return phones.get(phones.size()-1);
  }
 /* protected String timeForGame(String time) {
    Calendar cal = Calendar.getInstance();
    //String day_of_week = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);
    String month = String.valueOf(cal.get(Calendar.MONTH));
    String year = String.valueOf(cal.get(Calendar.YEAR));
    String date = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));

    return  date+month+year+time;
  }*/

  public void swipe (String direction){
    Dimension size = driver.manage().window().getSize();
    // PointOption point = new PointOption();
    WaitOptions wait = new WaitOptions().withDuration(Duration.ofMillis(1000));
    // JavascriptExecutor js = (JavascriptExecutor) driver;
    HashMap<String, String> swipeElement = new HashMap<String, String>();

    int startX = 0;
    int startY = 0;
    int endX = 0;
    int endY = 0;

    switch (direction) {
      case "Up":
        swipeElement.put("duration",String.valueOf(1.0));
        swipeElement.put("fromX", String.valueOf(size.width / 2));
        swipeElement.put("fromY", String.valueOf(size.height / 2 + size.height / 4));
        swipeElement.put("toX", String.valueOf(size.width / 2));
        swipeElement.put("toY", String.valueOf(size.height / 2 - size.height / 4));
        startX = size.width / 2;
        startY = size.height / 2;
        endX = startX;
        endY = 10;
        break;
      case "Down":
        swipeElement.put("duration",String.valueOf(1.0));
        swipeElement.put("fromX", String.valueOf(size.width / 2));
        swipeElement.put("fromY", String.valueOf(size.height / 2));
        swipeElement.put("toX", String.valueOf(size.width / 2));
        swipeElement.put("toY", String.valueOf(size.height));
        startX = size.width / 2;
        startY = size.height / 2;
        endX = startX;
        endY = size.height-10;
        break;
      case "Left":
        swipeElement.put("duration",String.valueOf(1.0));
        swipeElement.put("fromX", String.valueOf(size.width / 2 + size.width / 3));
        swipeElement.put("fromY", String.valueOf(size.height / 2));
        swipeElement.put("toX", String.valueOf(size.width / 10));
        swipeElement.put("toY", String.valueOf(size.height / 2));
        break;
      case "Right":
        swipeElement.put("duration",String.valueOf(1.0));
        swipeElement.put("fromX", String.valueOf(size.width / 2 - size.width / 3));
        swipeElement.put("fromY", String.valueOf(size.height / 2));
        swipeElement.put("toX", String.valueOf(size.width));
        swipeElement.put("toY", String.valueOf(size.height / 2));
        break;
    }

    if (driver.getCapabilities().getPlatform().toString().equals("MAC")){
      //driver.getPlatformName().toString().equals("android")
      //swipeElement.put("direction", move);
      //swipeElement.put("element", element);
      //js.executeScript("mobile: swipe", swipeElement);
      driver.executeScript("mobile: dragFromToForDuration", swipeElement);
    } else {
      new TouchAction(driver)
              .press(new PointOption().withCoordinates(startX, startY))
              .waitAction(wait)
              .moveTo(new PointOption().withCoordinates(endX, endY))
              .release()
              .perform();
    }
  }

  public void swipe (String direction, WebElement element) {
    // JavascriptExecutor js = (JavascriptExecutor) driver;
    HashMap<String, String> swipeElement = new HashMap<String, String>();
    String move = null;
    swipeElement.put("duration", String.valueOf(1.0));
    swipeElement.put("element", element.getAttribute(textAttribute));

    switch (direction) {
      case "Up":
        move = "up";
        break;
      case "Down":
        move = "down";
        break;
      case "Left":
        swipeElement.put("fromX", String.valueOf(element.getLocation().x));
        swipeElement.put("fromY", String.valueOf(element.getLocation().y));
        swipeElement.put("toX", String.valueOf(10));
        swipeElement.put("toY", String.valueOf(element.getLocation().y));
        break;
      case "Right":
        swipeElement.put("fromX", String.valueOf(10));
        swipeElement.put("fromY", String.valueOf(element.getLocation().y));
        swipeElement.put("toX", String.valueOf(element.getLocation().x));
        swipeElement.put("toY", String.valueOf(element.getLocation().y));
        break;
    }

    //swipeElement.put("direction", move);
    //js.executeScript("mobile: swipe", swipeElement);
    //driver.executeScript("mobile: swipe", swipeElement);
    driver.executeScript("mobile: dragFromToForDuration", swipeElement);
  }

  public void swipeAndroid(String direction, WebElement element) {
    WaitOptions wait = new WaitOptions().withDuration(Duration.ofMillis(1000));
    Dimension size = driver.manage().window().getSize();

    int startX = element.getLocation().getX();
    int startY = element.getLocation().getY();
    int endX = 0;
    int endY = 0;

    switch (direction) {
      case "Up":
        break;
      case "Down":
        break;
      case "Left":
        endX = 10;
        endY = startY;
        break;
      case "Right":
        endX = size.width - 10;
        endY = startY;
        break;
    }
    new TouchAction(driver)
            .press(new PointOption().withCoordinates(startX, startY))
            .waitAction(wait)
            .moveTo(new PointOption().withCoordinates(endX, endY))
            .release()
            .perform();

  }

  public void tapSignInButton(){
             new WebDriverWait(driver, 10)
            .until(ExpectedConditions.elementToBeClickable(loginButton))
            .click();
    //loginButton.click();
  }
  public void proceedPhone(String phone, String country) throws InterruptedException {
    Thread.sleep(1000);
    clickCountryField();
    selectCountry(country);
    enterPhone(phone);
    NextBtn.click();
  }
  public void clickCountryField() {
    //formStaticTexts.get(1).click();
    phoneCountryCode.click();
  }
  public void selectCountry(String country) {
    phoneCountrySearchIcon.click();
    phoneCountrySearchField.sendKeys(country);
    phoneCountryGermany.get(1).click();
  }
  public void enterPhone(String number) {
    textFields.get(0).clear();
    textFields.get(0).sendKeys(number);
  }
  public void enterCode(String code) throws InterruptedException {
    Thread.sleep(1000);
    new WebDriverWait(driver, 10)
            .until(ExpectedConditions.visibilityOf(codeField))
            .sendKeys(code);
    //textFields.get(0).sendKeys(code);
  }

  public void allowNotifications() {
    //Wait for first tip appearing !IOS ONLY!
    new WebDriverWait(driver, 30)
            .until(ExpectedConditions.visibilityOfElementLocated(MobileBy.ByAccessibilityId.AccessibilityId("Allow"))).click();
  }

  public void makeScreenshot(String pathname) throws IOException, InterruptedException {
    Thread.sleep(1000);
    File srcFile = driver.getScreenshotAs(OutputType.FILE);
    FileUtils.copyFile(srcFile, new File(pathname));
  }

  public void backToMatchfeedFromMatch() throws InterruptedException {
    threeDotMenu.click();
    if (platform.equals("ANDROID"))
      new WebDriverWait(driver, 10)
            .until(ExpectedConditions.visibilityOf(backToMatchfeed))
            .click();
    else
      new WebDriverWait(driver, 10)
              .until(ExpectedConditions.visibilityOf(backToMatchfeed))
              .click();
  }
  }
