package appmanager;

import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class IosAppManager extends AppManager {

    public IosAppManager(String executablePath, String device, String sdk) {
      super(MobilePlatform.IOS, executablePath, device, sdk);

      //getCapabilities().setCapability(MobileCapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, true);
      //getCapabilities().setCapability(MobileCapabilityType.UNHANDLED_PROMPT_BEHAVIOUR, true);
      //getCapabilities().setCapability("autoAcceptAlerts", true);
      //getCapabilities().setCapability(MobileCapabilityType.NO_RESET, true);
      //getCapabilities().setCapability(MobileCapabilityType.PLATFORM_VERSION, sdk);
      getCapabilities().setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
    }
  }
