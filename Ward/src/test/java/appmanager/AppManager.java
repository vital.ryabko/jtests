package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.net.UrlChecker;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AppManager {

  private DraftHelper draft;
  private MatchHelper match;
  private GameHelper gameHelper;
  private LocatorsBase locators;
  private NavHelper navigation;
  private FormHelper form;
  public AppiumDriver driver;
  private String agent;
  private AppiumDriverLocalService service;

  //public WebDriver browser;

  private final DesiredCapabilities capabilities;

  AppManager(String agent, String executablePath, String device, String sdk) {
    this.agent = agent;

    capabilities = new DesiredCapabilities();
    capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, agent);
    capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, device);
    capabilities.setCapability(MobileCapabilityType.APP, executablePath);
    capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, sdk);
  }

  public void init() throws Exception {
    //DesiredCapabilities capabilities = new DesiredCapabilities();
    System.out.printf("Appium server is running = %b. ", isAppiumRunning(4723));
    if (isAppiumRunning(4723))
      killAppium(getPID(4723));

      // Build the appium service
      AppiumServiceBuilder builder = new AppiumServiceBuilder();
      builder.withIPAddress("127.0.0.1");
      builder.usingPort(4723);
      //builder.withCapabilities(capabilities);
      builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
      builder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");

      //Start the server with the builder
      service = AppiumDriverLocalService.buildService(builder);
      service.start();

      if(!service.isRunning())
        throw new IllegalStateException("Appium server not started");

    if (agent.equals(MobilePlatform.ANDROID)) {
      //capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
      //capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "041604a1e6563902");
      //capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.0");
      //capabilities.setCapability(MobileCapabilityType.APP, "C:\\Testing\\apk\\Wardit-staging-release-1.1.67.apk");
      //capabilities.setCapability("autoGrantPermissions", true);
      //capabilities.setCapability("noReset", true);
      driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    } else if (agent.equals(MobilePlatform.IOS)) {
      //capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
      //capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone X");
      //capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "11.4");
      //capabilities.setCapability(MobileCapabilityType.APP, "/Users/valery/Desktop/Ward.app");
      //capabilities.setCapability("autoGrantPermissions", true);
      //capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
      capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,180);
      driver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
    }
      ChromeOptions options = new ChromeOptions();
      options.addArguments("disable-infobars");
      options.addArguments("--start-maximized");
      options.addArguments("--disable-extensions");
      //options.addArguments("headless");
      //options.addArguments("window-size=1200x600");
      /*browser = new ChromeDriver(options);
      browser.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
      browser.get("https://admin-staging.wardapp.xyz/auth");
      login();*/

    driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    form = new FormHelper(driver);
    navigation = new NavHelper(driver);
    locators = new LocatorsBase(driver);
    gameHelper = new GameHelper();
    match = new MatchHelper(driver);
    draft = new DraftHelper(driver);
  }

  /*
    private void login() {
      WebElement auth_field = browser.findElement(By.xpath("//input[@name='token']"));
      auth_field.click();
      auth_field.sendKeys("secret");
      browser.findElement(By.xpath("//button[@type='submit']")).click();
    }
  */
  public void stop() {
    driver.quit();
    //browser.quit();
  }

  DesiredCapabilities getCapabilities() {
    return capabilities;
  }

  public FormHelper form() {
    return form;
  }

  public NavHelper navigation() {
    return navigation;
  }

  public LocatorsBase locators() {
    return locators;
  }

  public GameHelper webadmin() {
    return gameHelper;
  }

  public MatchHelper match() {
    return match;
  }

  public DraftHelper draft() {
    return draft;
  }

  private boolean isAppiumRunning(int port) throws Exception {
    String SERVER_URL = String.format("http://127.0.0.1:%d/wd/hub", port);
    final URL status = new URL(SERVER_URL + "/sessions");
    try {
      new UrlChecker().waitUntilAvailable(2000, TimeUnit.MILLISECONDS, status);
      return true;
    } catch (UrlChecker.TimeoutException e) {
      return false;
    }
  }

  private String getPID(int port){
    try {
      String line;
      String command = String.format("lsof -nP -i4TCP:%d",port);
      Process p = Runtime.getRuntime().exec(command);
      BufferedReader input =
              new BufferedReader
                      (new InputStreamReader(p.getInputStream()));
      while ((line = input.readLine()) != null) {
        String result = line.replaceAll("( )+", " ").split(" ")[1];
        if (!result.equals("PID"))
          return result;
      }
      input.close();
    }
    catch (Exception err) {
      err.printStackTrace();
    }
    return null;
  }

  private void killAppium(String pid) throws IOException {
    String command = String.format("kill -9 %s",pid);
    Runtime.getRuntime().exec(command);
  }
}
