package appmanager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class LocatorsBase {
  public String textAttribute;
  String platform;

  public LocatorsBase(AppiumDriver driver) {
    Duration timeout = Duration.ofSeconds(20);
    PageFactory.initElements(new AppiumFieldDecorator(driver, timeout), this);

    //This parameter makes the difference between iOS and Android text attribute name. Now it's the same for both.
    if (driver.getCapabilities().getPlatform().toString().equals("LINUX")){
      textAttribute = "text";
      platform = "ANDROID";
    } else {
        textAttribute = "value";
        platform = "IOS";
    }
  }

  @AndroidFindBy(id = "button_sign_in_phone")
  @iOSFindBy(accessibility = "Sign in via Phone")
  public MobileElement loginButton;

  @AndroidFindBy(xpath= "//*[@text='Welcome to WARD, the real-time fantasy e-sport game']")
  @iOSFindBy(xpath = "//XCUIElementTypePageIndicator[@name=\"page 1 of 7\"]")
  public MobileElement walkPage1;

  @AndroidFindBy(xpath= "//*[@text='Ward is a fantasy eSport game']")
  @iOSFindBy(xpath = "//XCUIElementTypePageIndicator[@name=\"page 2 of 7\"]")
  public MobileElement walkPage2;

  @AndroidFindBy(xpath= "//*[@text='WHO WILL GET THE FIRST KILL?']")
  @iOSFindBy(xpath = "//XCUIElementTypePageIndicator[@name=\"page 3 of 7\"]")
  public MobileElement walkPage3;

  @AndroidFindBy(xpath= "//*[@text='During the match you will have 6 wards to predict']")
  @iOSFindBy(xpath = "//XCUIElementTypePageIndicator[@name=\"page 4 of 7\"]")
  public MobileElement walkPage4;

  @AndroidFindBy(id= "wardAvailableWardit")
  @iOSFindBy(xpath = "//XCUIElementTypePageIndicator[@name=\"page 5 of 7\"]")
  public MobileElement walkPage5;

  @AndroidFindBy(xpath= "//*[@text='We will notify you, when new Ward is available']")
  @iOSFindBy(accessibility = "Ward it!")
  public MobileElement walkPage5_wardIt;

  @AndroidFindBy(xpath= "//*[@text='We will notify you, when new Ward is available']")
  @iOSFindBy(xpath = "//XCUIElementTypePageIndicator[@name=\"page 6 of 7\"]")
  public MobileElement walkPage6;

  @AndroidFindBy(xpath= "//*[@text='Get the highest score and win awesome prizes!']")
  @iOSFindBy(xpath = "//XCUIElementTypePageIndicator[@name=\"page 7 of 7\"]")
  public MobileElement walkPage7;

  //Agreement elements

  @AndroidFindBy(id= "image")
  @iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"￼ ￼ \"]")
  public MobileElement closeAgreement;

  @AndroidFindBy(id= "agree")
  @iOSFindBy(accessibility = "Yes, I Agree")
  public MobileElement agree;

  @AndroidFindBy(id= "disagree")
  @iOSFindBy(accessibility = "No, I don't")
  public MobileElement disagree;

  // Common elements

  @AndroidFindBy(className = "android.widget.TextView")
  @iOSFindBy(className = "XCUIElementTypeStaticText")
  public List<MobileElement> formStaticTexts;

  @AndroidFindBy(className = "android.widget.EditText")
  @iOSFindBy(className = "XCUIElementTypeTextField")
  public List<MobileElement> textFields;

  //@AndroidFindBy(className = "android.widget.ImageButton")
  @iOSFindBy(className = "XCUIElementTypeButton")
  public List<MobileElement> formButtons;

  @AndroidFindBy(xpath = "//*[@text='" + "OK" + "']")
  @iOSFindBy(accessibility = "OK")
  public WebElement okButton;

  // Login Phone screen

  @AndroidFindBy(id = "close")
  @iOSFindBy(accessibility = "Cancel")
  public MobileElement phoneCancelBtn;

  @iOSFindBy(accessibility = "Terms & Conditions")
  public MobileElement phoneTerms;

  @AndroidFindBy(id = "phone_code")
  @iOSFindBy(accessibility = "+1")
  public MobileElement phoneCountryCode;

  @AndroidFindBy(id = "searchToggle")
  @iOSFindBy(accessibility = "search icon")
  public MobileElement phoneCountrySearchIcon;

  @iOSFindBy(accessibility = "Done")
  public MobileElement phoneCloseCountryList;

  @AndroidFindBy(id = "searchField")
  @iOSFindBy(xpath = "//XCUIElementTypeTextField[@value=\"Search\"]")
  public MobileElement phoneCountrySearchField;

  @AndroidFindBy(className = "android.widget.TextView")
  @iOSFindBy(accessibility = "Germany")
  public List<MobileElement> phoneCountryGermany;

  @AndroidFindBy(id = "next")
  @iOSFindBy(accessibility = "Next")
  public MobileElement NextBtn;

  //Login Code screen

  @AndroidFindBy(id = "confirm_code")
  @iOSFindBy(xpath = "//XCUIElementTypeTextField[@value=\"Code from sms\"]")
  public MobileElement codeField;

  @AndroidFindBy(id = "resend")
  @iOSFindBy(accessibility = "Haven't received the code?")
  public MobileElement codeHaventReceived;

  @AndroidFindBy(id = "error_text")
  @iOSFindBy(accessibility = "New SMS could be queried in 24 seconds")
  public MobileElement codeTimerMessage;

  @AndroidFindBy(id = "resend")
  @iOSFindBy(accessibility = "Resend the code")
  public MobileElement codeResend;

  @AndroidFindBy(id = "error_text")
  @iOSFindBy(accessibility = "Wrong code")
  public MobileElement wrongCodeError;

  @AndroidFindBy(id = "back")
  @iOSFindBy(accessibility = "Wrong code")
  public MobileElement backArrow;

  //Register Name screen

  //1. Back button is formButtons.get(0)
  //2. Title is formStaticTexts.get(0) value="Ok, Let’s know each other. What is your full name?"
  //3. Name is textFields.get(0) value="Name"
  @AndroidFindBy (id = "name")
  @iOSFindBy (xpath = "//XCUIElementTypeTextField[@value=\"Name\"]")
  public MobileElement nameField;

  //4. Second Name is textFields.get(1) value="Second Name"
  @AndroidFindBy (id = "secondName")
  @iOSFindBy (xpath = "//XCUIElementTypeTextField[@value=\"Second Name\"]")
  public MobileElement secondNameField;
  //5. Next is formButtons.get(1) value="Next"

  //Register Avatar screen

  //1. Back button is formButtons.get(0)
  //2. Title is formStaticTexts.get(1) value="Now choose your avatar"
  //3. Next is formButtons.get(1) value="Next"
  @AndroidFindBy(id = "de.aureum.card.dev:id/login")
  @iOSFindBy(xpath = "//XCUIElementTypeCell")
  public List<MobileElement> avatars;

  //Matchfeed screen

  //1. Settings button is formButton.get(0) name="settings icon"
  //2. Title is formStaticTexts.get(0) value="Matchfeed"
  //3. Date "FRIDAY, MAR 16" is one of formStaticTexts, maybe formStaticTexts.get(1)
  //4. Parent for one match record is XCUIElementTypeCell. First record is XCUIElementTypeCell.get(1)
  @AndroidFindBy(className = "android.view.ViewGroup")
  @iOSFindBy(xpath = "//XCUIElementTypeCell")
  public List<MobileElement> games;

  @AndroidFindBy(id = "settings")
  public MobileElement settingsIcon;
  //5. Child inside Parent is XCUIElementTypeStaticText
  //6. First team short record is child 0 value="ABC"
  //7. Second team short record is child 1 value="CBA"
  //8. First team full record is child 2 value="UNICORN"
  //9. Second team full record is child 3 value="FAITH"
  //10. Time of match is child 4 value="2:35 PM"

  //Settings screen
  @AndroidFindBy(xpath = "//*[@text='Logout']")
  @iOSFindBy(xpath = "//*[@value='Logout']")
  public MobileElement logoutButton;

  //Enter Match screen

  //1. Back button is formButtons.get(0)
  //2. "Join the Game" button is formButtons.get(1) name="Join the Game"
  @AndroidFindBy(id = "join_button")
  @iOSFindBy(accessibility = "Join the Game")
  public MobileElement joinButton;

  @AndroidFindBy(id = "searchButton")
  public MobileElement searchButton;

  @AndroidFindBy(id = "searchField")
  public MobileElement searchField;

  @AndroidFindBy(id = "container")
  @iOSFindBy(xpath = "//XCUIElementTypeCollectionView")
  public List<WebElement> heroesCollection;

  //3. First team short record is formStaticTexts.get(0) value="TSM"
  //4. Second team short record is formStaticTexts.get(2) value="FNC"
  //5. First+Second teams full records is formStaticTexts.get(3) value="TSM vs FNATIC"
  //6. Time of match is formStaticTexts.get(1) value="2:35 PM"
  //7. Draft notification is formStaticTexts.get(7) value="Draft is available"

  //Draft screen
  @AndroidFindBy(xpath = "//*[contains(@id, '_container')]")
  @iOSFindBy(xpath = "//XCUIElementTypeCollectionView")
  public List<WebElement> collections;

  @AndroidFindBy(xpath = "//*[@text='Predict the Picks']")
  @iOSFindBy(accessibility = "Predict the Picks")
  public WebElement firstDraftTip;

  @AndroidFindBy(xpath = "//*[contains(@text, 'Predict all the')]")
  @iOSFindBy(xpath = "//*[contains(@value, 'Predict all the')]")
  public WebElement secondDraftTip;

  @AndroidFindBy(xpath = "//*[contains(@text, 'you are done')]")
  @iOSFindBy(xpath = "//*[contains(@name, 'done with Draft')]")
  public WebElement wardItTip;

  @AndroidFindBy(id = "ok")
  @iOSFindBy(accessibility = "OK, I’m done")
  public WebElement okDoneButton;

  @AndroidFindBy(id = "keepPredicting")
  @iOSFindBy(accessibility = "Keep predicting")
  public WebElement keepPredictingButton;

  @AndroidFindBy(xpath = "//*[@text='Draft Ward']")
  @iOSFindBy(accessibility = "Draft Ward")
  public WebElement finalDraftTitle;

  @AndroidFindBy(xpath = "//*[contains(@text,'Please standby')]")
  @iOSFindBy(xpath = "//*[contains(@value,'Please standby')]")
  public WebElement finalDraftStandbyCopyright;

  @AndroidFindBy(xpath = "//*[contains(@text, 'is starting')]")
  @iOSFindBy(xpath = "//*[@value='The game is starting']")
  public WebElement finalDraftText;

  @AndroidFindBy(xpath = "//*[@text=\"Go to Summoner's Rift\"]")
  @iOSFindBy(accessibility = "Go to Summoner's Rift")
  public WebElement goToSummonerRift;

  //FirstBlood screen

  @AndroidFindBy(id = "arrowLeft")
  @iOSFindBy(accessibility = "arrow left")
  public WebElement arrowLeft;

  @AndroidFindBy(id = "arrowRight")
  @iOSFindBy(accessibility = "arrow right")
  public WebElement arrowRight;

  @AndroidFindBy(id = "ward_it")
  @iOSFindBy(accessibility = "Ward it!")
  public WebElement wardItButton;

  @AndroidFindBy(id = "quit")
  @iOSFindBy(accessibility = "Back to matchfeed")
  public WebElement backToMatchfeed;

  @AndroidFindBy(id = "moreButton")
  @iOSFindBy(accessibility = "more icon")
  public WebElement threeDotMenu;

  //First Blood first selection screen

  @AndroidFindBy(xpath = "//*[contains(@resource-id, 'id/title')]/*[1]")
  @iOSFindBy(accessibility = "FIRST BLOOD WARD")
  public WebElement fBloodSelectPlayerTitle;

  @AndroidFindBy(xpath = "//*[contains(@resource-id, 'id/subtitle')]/*[1]")
  @iOSFindBy(accessibility = "Who will spill first blood?")
  public WebElement fBloodSelectPlayerSubTitle;

  @AndroidFindBy(xpath = "//*[contains(@resource-id, 'id/title')]/*[1]")
  @iOSFindBy(accessibility = "BONUS WARD")
  public WebElement commonSelectLineTitle;

  @AndroidFindBy(xpath = "//*[contains(@resource-id, 'id/subtitle')]/*[1]")
  @iOSFindBy(accessibility = "Which line?")
  public WebElement commonSelectLineSubtitle;

  @AndroidFindBy(id = "picker_lines_img")
  @iOSFindBy(accessibility = "picker-lines-none")
  public WebElement commonPickLineFieldImg;

  @AndroidFindBy(id = "picker_lines_img")
  @iOSFindBy(accessibility = "picker-lines-jungle-none")
  public WebElement fbPickLineFieldImg;

  @AndroidFindBy(xpath = "//*[@text='You were wrong']")
  @iOSFindBy(accessibility = "You were wrong")
  public WebElement commonFailSubTitle;

  @AndroidFindBy(id = "score")
  public WebElement wardScore;

  //Tower Ward section

  @WithTimeout(time = 90, chronoUnit = ChronoUnit.SECONDS)
  @AndroidFindBy(xpath = "//*[contains(@text, 'unlocked')]")
  public WebElement pushUnlocked;

  @AndroidFindBy(id = "wardDescLabel")
  @iOSFindBy(accessibility = "Which team will destroy the first tower?")
//@iOSFindBy(xpath = "//*[contains(@value,'the first tower?')]")
  public WebElement towerQuestionTitle;

  @AndroidFindBy(xpath = "//*[contains(@resource-id, 'id/title')]/*[1]")
  @iOSFindBy(accessibility = "FIRST TOWER WARD")
  public WebElement towerSelectTeamTitle;

  @AndroidFindBy(xpath = "//*[contains(@resource-id, 'id/subtitle')]/*[1]")
  @iOSFindBy(accessibility = "Which team will destroy the first tower?")
//@iOSFindBy(xpath = "//*[contains(@value,'the first tower?')]")@iOSFindBy(accessibility = "FIRST BLOOD WARD")
  public WebElement towerSelectTeamSubtitle;


}
