package tests;

import appmanager.AndroidAppManager;
import appmanager.AppManager;
import appmanager.IosAppManager;
import io.appium.java_client.remote.MobilePlatform;
import org.junit.After;
import org.junit.Before;

import java.net.MalformedURLException;
import java.security.InvalidParameterException;


public class TestBase {

  //protected static final AppManager app = new AppManager(MobilePlatform.IOS);
  //protected static final AppManager app = new AppManager(BrowserType.FIREFOX);
  //protected static final AppManager app = new AppManager(MobilePlatform.ANDROID);
 // public WebDriver browser;
  static AppManager app;

  @Before
  public void startDriver() throws Exception {
    //Uncomment this when running tests from CLI
    /*String platform = System.getProperty("platform");
    String executablePath = System.getProperty("executablePath");
    String device = System.getProperty("device");
    String sdk = System.getProperty("sdk");*/

    //To run tests from IDE uncomment these parameters
    String platform = "android";
    String executablePath = "C:\\Testing\\apk\\Wardit-staging-release-1.1.99.apk";
    String device = "041604a1e6563902";
    String sdk = "7.0";
    /*String platform = "ios";
    String executablePath = "/Users/vital/Testing/apk/Ward.app";
    String device = "iPhone X";
    String sdk = "12.0";*/

    if (platform.toUpperCase().equals(MobilePlatform.IOS.toUpperCase())) {
      app = new IosAppManager(executablePath, device, sdk);
    } else if (platform.toUpperCase().equals(MobilePlatform.ANDROID.toUpperCase())) {
      app = new AndroidAppManager(executablePath, device, sdk);
    } else {
      throw new InvalidParameterException();
    }
    app.init();

/*    ChromeOptions options = new ChromeOptions();
    options.addArguments("disable-infobars");
    options.addArguments("--start-maximized");
    options.addArguments("--disable-extensions");
    //options.addPreference("security.sandbox.content.level",5);
    browser = new ChromeDriver(options);
    browser.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    browser.get("https://admin-staging.wardapp.xyz/");*/
  }

  @After
  public void stopDriver() {
    app.stop();
   // browser.quit();
  }

}
