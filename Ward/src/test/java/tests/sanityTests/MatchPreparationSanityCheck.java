package tests.sanityTests;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import restclient.AdminClient;
import restclient.model.Team;

import java.io.IOException;

public class MatchPreparationSanityCheck extends SanityTestBase {

  public MatchPreparationSanityCheck() throws IOException {
    super();
  }

  private AdminClient admin = new AdminClient();
  //Read the new phone (for this session) from the file
  private String newPhone = app.form().readLastPhoneFromFile();
  //Declare environment
  private Team FIRST_TEAM = admin.parseTeamfromManifest(FIRST_TEAM_NAME);
  private Team SECOND_TEAM = admin.parseTeamfromManifest(SECOND_TEAM_NAME);

  @Test
  @DisplayName("Matchfeed Sanity check. Create new game and check if it's in the Matchfeed.")
  public void sanityTestMatchfeed1() throws InterruptedException, IOException {

    //Login as registered user
    app.navigation().goToPhoneScreen();
    app.form().loginWithPhone(this.newPhone);

    //Land on Matchfeed screen
    app.match().verifyMatchfeedTitle();
    app.match().verifyLocalDate();

    //Select the game in Matchfeed
    app.match().tapGame(CURRENT_GAME);

    //Check game start screen and join the game
    app.match().startMatchCopyrights(CURRENT_GAME);
    app.match().tapToJoin();
  }

  @Test
  @DisplayName("Matchfeed Sanity check. Check Draft selections. Proceed with empty Draft.")
  public void sanityTestMatchfeed2() throws InterruptedException {
    /*THis is only for debug mode
    if (app.driver.getCapabilities().getPlatform().toString().equals("MAC"))
      app.match().tapToJoin();*/

    //Land on Draft screen
    try {
      //Wait first tip for 30sec and verify it if found
      app.draft().firstDraftTip();
    } catch (NoSuchElementException | TimeoutException e) {
      System.out.println(e);
      System.out.println("First draft is missed. Continuing...");
    }

    //Set hero to player
    app.draft().setHeroToPlayer(FIRST_TEAM, 1, 1, "Ahri");

    try {
      //Wait second tip and verify it
      app.draft().secondDraftTip();
    } catch (NoSuchElementException | TimeoutException e) {
      System.out.println(e);
      System.out.println("Second draft is missed. Continuing...");
    }

    //Remove hero from player
    //app.draft().removeHeroFromPlayer(FIRST_TEAM, 1, 1);

    if (app.driver.getCapabilities().getPlatform().toString().equals("LINUX"))
      app.navigation().searchButton.click();

    //Proceed with this draft
    app.draft().tapWardIt();
    app.draft().keepPredicting();
    app.draft().tapWardIt();
    app.draft().checkFinalDraftCopyrights();
  }

  @Test
  @DisplayName("Check Final Draft screen. Check scores after draft confirmation. Transition to Match.")
  public void sanityTestMatchfeed3() throws InterruptedException, IOException {
    //API: set random Prepick for teams
    CURRENT_PAYLOAD = app.webadmin().setDefaultPrepick(CURRENT_GAME, FIRST_TEAM_NAME, SECOND_TEAM_NAME);

    //Verify popup message in the app
    app.draft().checkEndDraftPushResults(0);

    //API: post start game payload
    CURRENT_PAYLOAD = app.webadmin().startGame(CURRENT_GAME, CURRENT_PAYLOAD);

    //Tap "Go to Summoner's Rift"
    app.draft().tapGoToSummonersRift();

    //Check FirstBlood screen copyrights
    app.match().firstBloodCopyrights();
  }

  @Test
  @DisplayName("Exit game - return back. Pick FirstBlood ward. Stay on FirstBlood screen, wait TowerDestroy push.")
  public void sanityTestMatchfeed4() throws IOException, InterruptedException {
    //Quit match screen
    app.match().backToMatchfeedFromMatch();
    //Join the game
    app.match().tapGame(CURRENT_GAME);
    app.match().tapToJoin();
    //Land on the First Blood screen
    app.match().firstBloodCopyrights();

    //Tap Ward it!
    app.match().wardIt();

    //Check First Blood Player selection screen
    app.match().fBloodPlayersSelectionCopyrights();
    app.match().checkFirstBloodPlayersFor(FIRST_TEAM);
    app.match().checkFirstBloodPlayersFor(SECOND_TEAM);
    app.match().checkWardItDisabled();

    //Select player
    app.match().selectFirstBloodChampion(FIRST_TEAM, "mid");
    app.match().checkWardItEnabled();

    //Get score and tap Ward it!
    //System.out.println(String.format("Fixed score is: %s", app.match().getWardScore()));
    //String score = app.match().getWardScore();
    app.match().tapWardIt();

    //Select line
    app.match().commonWardLineCopyrights();
    app.match().checkWardItDisabled();
    app.match().tapPickLine("jungle");
    app.match().checkWardItEnabled();
    app.match().tapWardIt();

    //Send FB results to backend
    String fb_winner = app.match().getPlayerName(FIRST_TEAM, "adc");
    System.out.println(String.format("I'm going to set %s player on backend.", fb_winner));
    CURRENT_PAYLOAD = app.webadmin().setFBWard(CURRENT_GAME, CURRENT_PAYLOAD, FIRST_TEAM, "adc", "jungle");

    //Check wrong choose
    app.match().firstBloodWrongCopyrights(fb_winner);

    //TODO Add push check for Tower ward
    //Go to the second ward
    app.match().arrowRight.click();
  }

  @Ignore
  @Test
  @DisplayName("FirstBlood ward. Check Score in Leader board.")
  public void sanityTestMatchFirstBlood2() {
    //TODO Implement Leader board check
  }

  @Test
  @DisplayName("Tower ward. Select wrong team. Select correct line though.")
  public void sanityTestMatchfeed5() throws IOException, InterruptedException {
    //app.match().waitPush("tower");

    //Land on the Tower screen
    app.match().towerCopyrights();

    //Tap Ward it!
    app.match().wardIt();

    //Check Tower teams selection screen
    app.match().towerWardTeamsCopyrights();
    app.match().checkTowerTeam(FIRST_TEAM);
    app.match().checkTowerTeam(SECOND_TEAM);
    app.match().checkWardItDisabled();

    //Select team
    app.match().selectTowerTeam(FIRST_TEAM);
    app.match().checkWardItEnabled();

    //Get score and tap Ward it!
    //System.out.println(String.format("Fixed score is: %s", app.match().getWardScore()));
    //String score = app.match().getWardScore();
    app.match().tapWardIt();

    //Select line
    app.match().commonWardLineCopyrights();
    app.match().checkWardItDisabled();
    app.match().tapPickLine("top");
    app.match().checkWardItEnabled();
    app.match().tapWardIt();

    //Send Tower results to backend
    System.out.println(String.format("I'm going to set %s team on backend.", SECOND_TEAM.getTitle()));
    CURRENT_PAYLOAD = app.webadmin().setTowerWardTeam(CURRENT_GAME, CURRENT_PAYLOAD, SECOND_TEAM);

    //Check wrong team choosing
    app.match().towerWrongTeamCopyrights(SECOND_TEAM);

    //Check re-calculations for correct line
    //TODO app.match().commonCorrectLineCopyrights("top");
  }
  @Ignore
  @Test
  public void SandboxTest() throws InterruptedException {
    Thread.sleep(30000);
    app.match().towerWrongTeamCopyrights(SECOND_TEAM);
    //app.match().checkFirstBloodPlayersFor(FIRST_TEAM);

    //Select player
    app.match().selectFirstBloodChampion(SECOND_TEAM, "top");
    app.match().checkWardItEnabled();

    //System.out.println(String.format("Fixed score is: %s", app.match().getWardScore()));
    //String score = app.match().getWardScore();
    app.match().tapWardIt();

    //Select line
    app.match().commonWardLineCopyrights();
    app.match().checkWardItDisabled();
    //app.match().tapPickLine("middle");
    app.match().tapPickLine("top");
    app.match().tapPickLine("bottom");
    app.match().tapPickLine("jungle");
    app.match().checkWardItEnabled();
    app.match().tapWardIt();


  }
  }
