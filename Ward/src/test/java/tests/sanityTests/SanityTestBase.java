package tests.sanityTests;

import appmanager.AndroidAppManager;
import appmanager.AppManager;
import appmanager.IosAppManager;
import io.appium.java_client.remote.MobilePlatform;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import restclient.model.Game;
import restclient.model.Payload;

import java.net.MalformedURLException;
import java.security.InvalidParameterException;
import java.text.ParseException;

public class SanityTestBase {
  public static String FIRST_TEAM_NAME;
  public static String SECOND_TEAM_NAME;
  public static Game CURRENT_GAME;
  public static Payload CURRENT_PAYLOAD;
  static AppManager app;

  @BeforeClass
  public static void startDriver() throws Exception {
    //Uncomment this when running tests from CLI
    /*String platform = System.getProperty("platform");
    String executablePath = System.getProperty("executablePath");
    String device = System.getProperty("device");
    String sdk = System.getProperty("sdk");*/

    //To run tests from IDE uncomment these parameters
    /*String platform = "android";
    String executablePath = "C:\\Testing\\apk\\Wardit-staging-release-1.1.99.apk";
    String device = "041604a1e6563902";
    String sdk = "7.0";*/
    String platform = "ios";
    String executablePath = "/Users/vitalryabko/Testing/apk/Ward.app";
    String device = "iPhone X";
    String sdk = "12.0";

    if (platform.toUpperCase().equals(MobilePlatform.IOS.toUpperCase())) {
      app = new IosAppManager(executablePath, device, sdk);
    } else if (platform.toUpperCase().equals(MobilePlatform.ANDROID.toUpperCase())) {
      app = new AndroidAppManager(executablePath, device, sdk);
    } else {
      throw new InvalidParameterException();
    }
    app.init();

    FIRST_TEAM_NAME = "Gambit Esports";
    SECOND_TEAM_NAME = "Splyce";
    CURRENT_GAME = app.webadmin().createGameTodayInXMinutes(FIRST_TEAM_NAME, SECOND_TEAM_NAME, 5);
  }

  @AfterClass
  public static void stopDriver() {
    app.webadmin().endGameSeries(CURRENT_GAME);
    app.stop();
  }

}
