package tests.sanityTests;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class RegisterSanityCheck extends SanityTestBase {

  private void writeNewAccountToFile () throws IOException {
    if(!Files.exists(Paths.get("accounts.txt")))
      Files.createFile(Paths.get("accounts.txt"));
    app.navigation().writeNewPhone();
  }

  @Test
  @DisplayName("Registration Sanity check. Go to Name screen")
  public void sanityTestRegister1() throws InterruptedException, IOException {

    //Proceed Walkthrough
    app.navigation().goToPhoneScreen();

    //Create a new record to the accounts file
    writeNewAccountToFile();
    // Read new phone from phone's file
    String newPhone = app.form().readLastPhoneFromFile();

    //Using new phone, create new user
    app.form().proceedPhone(newPhone, "Germany");
    app.navigation().enterCode("198412");
    app.form().NextBtn.click();
  }

  @Test
  @DisplayName("Registration Sanity check. Check Name screen")
  public void sanityTestRegister2() throws InterruptedException {
    //Check Name screen
    app.navigation().validateScreen("Name");
    app.form().isKeyboardOpen();
    app.form().isNextDisabled();

    //Back to Phone screen and return to Name screen
    app.form().goBack();
    app.form().isNumKeyboardOpen();
    app.form().NextBtn.click();
    app.navigation().validateScreen("Name");

    //Check logic of Name screen
    app.form().enterName("a");
    app.form().isNextDisabled();
    app.form().clearNameField();
    app.form().enterLastName("Tester");
    app.form().isNextDisabled();
    app.form().enterName("Auto");

    //Go to Avatar screen
    app.form().NextBtn.click();
  }

  @Test
  @DisplayName("Registration Sanity check. Check Avatar screen")
  public void sanityTestRegister3() throws InterruptedException {

    //Land on Avatar screen
    app.navigation().validateScreen("Avatar");
    app.form().isKeyboardClosed();

    //Go Back to Name screen and return to Avatar screen
    app.form().isNextDisabled();
    app.form().goBack();
    app.navigation().validateScreen("Name");
    app.form().NextBtn.click();

    //Select avatar and go to Favorite screen
    app.form().tapAvatar(0);
    app.form().isNextEnabled();
    app.navigation().swipe("Up");
    app.form().tapAvatar(1);
  }

  @Test
  @DisplayName("Registration Sanity check. Check Favorite screen")
  public void sanityTestRegister4() throws InterruptedException {

    //Go to Favorite team
    app.form().NextBtn.click();
    app.navigation().validateScreen("Favorite");

    //Select favorite team and go to Matchfeed
    app.navigation().selectTeam("100 Thieves");
  }

  @Test
  @DisplayName("Registration Sanity check. Go to Settings screen and logout")
  public void sanityTestRegister5() {

    //Validate Matchfeed screen
    app.navigation().validateScreen("Matchfeed");

    //Go to Settings screen
    app.navigation().tapSettingsIcon();
    app.navigation().validateScreen("Settings");

    //Make logout
    app.navigation().logout();
  }
}
