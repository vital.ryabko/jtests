package tests;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.IOException;

public class Registering extends TestBase {

  public Registering() throws IOException {
  }

  private static String newPhone;

  @Test
  @DisplayName("New user + Name screen + Back to Phone + Go to Avatar")
  public void testNameScreen () throws InterruptedException, IOException {
    //Read the new phone (for this session) from the file
    newPhone = app.form().readLastPhoneFromFile();

    app.navigation().goToPhoneScreen();
    app.form().proceedPhone(newPhone, "Germany");
    app.navigation().enterCode("198412");
    app.form().NextBtn.click();
    //app.form().waitForCodeAndGoNext(); We can use Secure code autofill function on iOS on real device

    //First time on Name screen
    app.navigation().validateScreen("Name");
    app.form().isKeyboardOpen();
    app.form().isNextDisabled();

    //Back to Phone screen and return to Name screen
    app.form().goBack();
    app.form().isNumKeyboardOpen();
    app.form().NextBtn.click();
    app.navigation().validateScreen("Name");

    //Check logic of Name screen
    app.form().enterName("a");
    app.form().isNextDisabled();
    app.form().clearNameField();
    app.form().enterLastName("Tester");
    app.form().isNextDisabled();
    app.form().enterName("Auto");

    //Go to Avatar screen
    app.form().NextBtn.click();
    app.navigation().validateScreen("Avatar");
    app.form().isKeyboardClosed();
  }

  @Test
  @DisplayName("New user + Avatar screen + Back to Name + Go to Avatar")
  public void testAvatarScreen () throws InterruptedException, IOException {

    //Go to Avatar screen
    app.navigation().tapSignInButton();
    app.form().proceedPhone(newPhone, "Germany");
    //app.form().waitForCodeAndGoNext();
    app.form().enterName("Auto");
    app.form().enterLastName("Tester");
    app.form().NextBtn.click();
    app.navigation().validateScreen("Avatar");

    //Go Back to Name screen and return to Avatar screen
    app.form().isNextDisabled();
    app.form().goBack();
    app.navigation().validateScreen("Name");
    app.form().NextBtn.click();

    //Select avatar and go to Matchfeed
    app.form().tapAvatar(0);
    app.form().isNextEnabled();
    app.form().tapAvatar(0);
    app.form().isNextDisabled();
    app.navigation().swipe("Up");
    app.form().tapAvatar(1);

    //Go to Favorite team
    app.form().NextBtn.click();
    app.navigation().validateScreen("Favorite");

    //Select favorite team
    app.navigation().selectTeam("100 Thieves");
    app.navigation().validateScreen("Matchfeed");
  }

  @Test
  @DisplayName("Go to Settings + Logout")
  public void testLogout () throws InterruptedException, IOException {

  }


  }
