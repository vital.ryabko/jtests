package tests;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.junit.Test;
import org.openqa.selenium.net.UrlChecker;
import org.openqa.selenium.remote.DesiredCapabilities;
import restclient.AdminClient;

import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class SimpleTest {
  private String s = null;

  @Test
  public void test1(){
    try {
      assertThat(s,is(notNullValue()));
    } catch (AssertionError e){
      s = " ";
      assertThat(s,is(notNullValue()));
      System.out.println("String is not null!");
    }
  }

  @Test
  public void createGameTest() throws ParseException {
    AdminClient admin = new AdminClient();

    List<String> teams = new ArrayList<>();
    teams.add("Counter Logic Gaming");
    teams.add("Team Liquid");

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    Date parsedDate = dateFormat.parse("2018-10-23 21:53:00");
    Timestamp timestamp = new Timestamp(parsedDate.getTime());

    Response response = admin.createNewGame("auto-a1a1a1", timestamp, teams);
    System.out.println(response.getStatus());
  }

  private AppiumDriverLocalService service;
  @Test
  public void checkAppium() throws Exception {

    DesiredCapabilities cap = new DesiredCapabilities();
    cap.setCapability("deviceName","iPhone X");
    cap.setCapability("app","/Users/Testing/apk/Ward.app");
    cap.setCapability("automationName","XCUITest");
    System.out.printf("Appium server is running = %b. ", isAppiumRunning(4723));

    if (!isAppiumRunning(4723)){
      killAppium(getPID(4723));
      // Build the appium service
      AppiumServiceBuilder builder = new AppiumServiceBuilder();
      builder.withIPAddress("127.0.0.1");
      builder.usingPort(4723);
      builder.withCapabilities(cap);
      //builder.withCapabilities(capabilities);
      builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
      builder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");

      //Start the server with the builder
      service = AppiumDriverLocalService.buildService(builder);
      service.start();

      if(!service.isRunning())
        throw new IllegalStateException("Appium server not started");
    } else
      System.out.println("Appium is already running.");
  }

  private boolean isAppiumRunning(int port) throws Exception {
    final URL status = new URL(String.format("http://127.0.0.1:%d/wd/hub/sessions",port));
    try {
      new UrlChecker().waitUntilAvailable(2000, TimeUnit.MILLISECONDS, status);
      return true;
    } catch (UrlChecker.TimeoutException e) {
      return false;
    }
  }

  private String getPID(int port){
    try {
      String line;
      String command = String.format("lsof -nP -i4TCP:%d",port);
      Process p = Runtime.getRuntime().exec(command);
      BufferedReader input =
              new BufferedReader
                      (new InputStreamReader(p.getInputStream()));
      while ((line = input.readLine()) != null) {
        String result = line.replaceAll("( )+", " ").split(" ")[1];
        if (!result.equals("PID"))
          return result;
      }
      input.close();
    }
    catch (Exception err) {
      err.printStackTrace();
    }
    return null;
  }

  private void killAppium(String pid) throws IOException {
    String command = String.format("kill -9 %s",pid);
    Runtime.getRuntime().exec(command);
  }
}
